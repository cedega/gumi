GUMI
=======
-------

To download the data files required for compiling, please visit: https://dl.dropboxusercontent.com/u/12649758/GUMI/gumidata.zip
  
Extract the files to the root of your GUMI repository.

This step is optional Desktop applications, but heavily recommended for iOS / Android.
  
---------------
For newcomers, please view the [Wiki](https://bitbucket.org/cedega/gumi/wiki/Home).

FAQ
=======
-------

Q: How do I access the documentation?

A: Either generate it using Doxygen in the 'doc' folder or visit: https://dl.dropboxusercontent.com/u/12649758/GUMI/html/index.html