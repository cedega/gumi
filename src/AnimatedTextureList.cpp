#include "GUMI/AnimatedTextureList.h"

namespace gu
{

AnimatedTextureList::AnimatedTextureList() :
    selected_index_(0)
{
}

AnimatedTextureList::AnimatedTextureList(const gu::AnimatedTexture &at) :
    selected_index_(0)
{
    animated_textures_.push_back(at);
    animated_texture_names_.push_back("");
}

void AnimatedTextureList::add(const std::string &animation_name, const gu::AnimatedTexture &at)
{
    animated_textures_.push_back(at);
    animated_texture_names_.push_back(animation_name);
}

void AnimatedTextureList::change(const std::string &animation_name, bool reset)
{
    int index = -1;

    for (unsigned int i = 0; i < animated_texture_names_.size(); i++)
        if (animation_name.compare(animated_texture_names_[i]) == 0)
        {
            index = i;
            break;
        }

    if (index == -1)
    {
        SDL_Log("[GUMI] (AnimatedTextureList) Error: cannot find \"%s\" in AnimatedTextureList", animation_name.c_str());
        return;
    }

    selected_index_ = index;

    if (reset)
        animated_textures_[selected_index_].reset();
}

void AnimatedTextureList::change(int index, bool reset)
{
    selected_index_ = index;

    if (reset)
        animated_textures_[selected_index_].reset();
}

// Getters

gu::AnimatedTexture* AnimatedTextureList::getCurrentAnimatedTexture()
{
    return &animated_textures_[selected_index_];
}

std::vector<gu::AnimatedTexture>* AnimatedTextureList::getAnimatedTextures()
{
    return &animated_textures_;
}

}
