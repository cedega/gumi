#include "GUMI/Collisions.h"

namespace gu
{

Collisions::Collisions()
{
}

void Collisions::draw()
{
    for (unsigned int i = 0; i < aabbs_.size(); i++)
        gu::DrawRect::draw(aabbs_[i]);
}

void Collisions::loadFileCollisions(const std::string &file_path)
{
    if (gu::Zip::getRedirectFileLoading())
    {
        loadZipCollisions(file_path);
        return;
    }

#if defined(TARGET_OS_IPHONE) || defined(__ANDROID__) 

    SDL_RWops *rw = SDL_RWFromFile(file_path.c_str(), "r");
    Sint64 size = rw->size(rw);
    char *buffer = new char[size];
    rw->read(rw, buffer, size, 1);
    std::string contents(buffer, size);
    delete[] buffer;
    SDL_FreeRW(rw);

    // Remove potential Windows file endings
    contents.erase(std::remove(contents.begin(), contents.end(), '\r'), contents.end()); 
    processAABBs(contents);

#else

    std::ifstream file(file_path.c_str());

    std::string line;
    std::string contents;

    while (std::getline(file, line))
    {
        contents += line + "\n";
    }

    file.close();
    
    // Remove potential Windows file endings
    contents.erase(std::remove(contents.begin(), contents.end(), '\r'), contents.end()); 
    processAABBs(contents);

#endif

}

void Collisions::loadZipCollisions(const std::string &file_path)
{

#if defined(TARGET_OS_IPHONE) || defined(__ANDROID__) 

    SDL_RWops *rw = SDL_RWFromFile(file_path.c_str(), "r");
    Sint64 size = rw->size(rw);
    char *buffer = new char[size];
    rw->read(rw, buffer, size, 1);
    std::string contents(buffer, size);
    delete[] buffer;
    SDL_FreeRW(rw);

    // Remove potential Windows file endings
    contents.erase(std::remove(contents.begin(), contents.end(), '\r'), contents.end()); 
    processAABBs(contents);

#else

    gu::Zip zip(file_path);
    std::string contents(zip.getBuffer(), zip.getSize());
    free(zip.getBuffer());

    // Remove potential Windows file endings
    contents.erase(std::remove(contents.begin(), contents.end(), '\r'), contents.end()); 
    processAABBs(contents);

#endif

}

bool Collisions::collide(gu::Entity *entity) const
{
    bool ret = false;
    
    gu::AABB entity_aabb = gu::toAABB(entity->getHitbox());
    glm::vec3 mtv;

    for (unsigned int i = 0; i < aabbs_.size(); i++)
    {
        if (entity->gu::EntityLogic::collide(entity->getHitbox(), gu::toYOBB(aabbs_[i]), &mtv, &entity_aabb, &aabbs_[i]))
        {
            entity->gu::EntityLogic::setPosition(entity->gu::EntityLogic::getPosition() - mtv);
            entity->updateHitbox();
            ret = true;
        }
    }

    return ret;
}

void Collisions::processAABBs(const std::string &contents)
{
    size_t start = contents.find('\n') + 1;
    std::string line;

    gu::AABB temp;
    int index = 0;
    
    while (true)
    {
        size_t endline = contents.find('\n', start);
        line = contents.substr(start, endline - start);
        
        if (line.compare("END") == 0)
        {
            index = 0;
            aabbs_.push_back(temp);
            break;
        }
        else if(line.at(0) == '@')
        {
            index = 0;
            aabbs_.push_back(temp);
        }
        else
        {
            switch (index)
            {
            case 0: temp.centre.x = atof(line.c_str()); index++; break;
            case 1: temp.centre.y = atof(line.c_str()); index++; break;
            case 2: temp.centre.z = atof(line.c_str()); index++; break;
            case 3: temp.radius.x = atof(line.c_str()); index++; break;
            case 4: temp.radius.y = atof(line.c_str()); index++; break;
            case 5: temp.radius.z = atof(line.c_str()); index++; break;
            default: break;
            }
        }

        start = endline + 1;
    }
}

void Collisions::add(const gu::AABB &aabb)
{
    aabbs_.push_back(aabb);
}

void Collisions::clear()
{
    aabbs_.clear();
}

// Getters

const std::vector<gu::AABB>& Collisions::getAABBs() const
{
    return aabbs_;
}

}
