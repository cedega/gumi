#include "GUMI/Entity.h"

namespace gu
{

Entity::Entity() :
    model_(NULL),
    scaling_(1.0f),
    colour_(1.0f),
    turn_speed_(8.0f),
    speed_(0.0f),
    jump_(0.0f),
    jumping_(false),
    apply_turn_speed_(true),
    loaded_armature_(false),
    rotate_hitbox_(false)
{
}

Entity::Entity(const Entity &obj) : 
    gu::EntityLogic(obj),
    gu::Drawable(obj),
    model_(obj.model_),
    armature_(obj.armature_),
    position_(obj.position_),
    last_position_(obj.last_position_),
    rotation_(obj.rotation_),
    scaling_(obj.scaling_),
    colour_(obj.colour_),
    input_(obj.input_),
    turn_speed_(obj.turn_speed_),
    speed_(obj.speed_),
    jump_(obj.jump_),
    jumping_(obj.jumping_),
    apply_turn_speed_(obj.apply_turn_speed_),
    loaded_armature_(obj.loaded_armature_),
    hitbox_(obj.hitbox_),
    hitbox_offset_(obj.hitbox_offset_),
    cullbox_(obj.cullbox_),
    cullbox_offset_(obj.cullbox_offset_),
    rotate_hitbox_(obj.rotate_hitbox_)
{
}

Entity::~Entity()
{
}

Entity& Entity::operator=(const Entity &obj)
{
    gu::EntityLogic::operator=(obj);
    gu::Drawable::operator=(obj);

    model_ = obj.model_;
    armature_ = obj.armature_;
    position_ = obj.position_;
    last_position_ = obj.last_position_;
    rotation_ = obj.rotation_;
    scaling_ = obj.scaling_;
    colour_ = obj.colour_;

    input_ = obj.input_;
    turn_speed_ = obj.turn_speed_;
    speed_ = obj.speed_;
    jump_ = obj.jump_;
    jumping_ = obj.jumping_;
    apply_turn_speed_ = obj.apply_turn_speed_;
    
    hitbox_ = obj.hitbox_;
    hitbox_offset_ = obj.hitbox_offset_;
    cullbox_ = obj.cullbox_;
    cullbox_offset_ = obj.cullbox_offset_;
    rotate_hitbox_ = obj.rotate_hitbox_;
    loaded_armature_ = obj.loaded_armature_;

    return *this;
}
    
void Entity::updateAnimations(const float frame_time)
{
    if (loaded_armature_)
    {
        model_->bindArmature(&armature_);
        model_->updateAnimations(frame_time);
    }
}

void Entity::draw(const float interpolation)
{
    model_->setPosition(position_);
    model_->setLastPosition(last_position_);
    model_->setRotation(rotation_);
    model_->setScaling(scaling_);
    model_->setColour(colour_);
    model_->draw(interpolation);
}

void Entity::bindShader(gu::Shader *shader)
{
    model_->bindShader(shader);
}

void Entity::bindModel(gu::Model *model, gu::Shader *shader)
{
    model_ = model;

    if (shader != NULL)
        model_->bindShader(shader);

    if (loaded_armature_)
        model_->bindArmature(&armature_);
}

void Entity::input(const glm::vec2 &move, bool jump)
{
    input_.move = move;
    input_.jump = jump;
}

bool Entity::move(const float dt)
{
    glm::vec3 velocity = glm::vec3(0.0f, getVelocity().y, 0.0f);
    bool moving = gu::absval(input_.move.x) > 0 || gu::absval(input_.move.y) > 0;
    
    if (apply_turn_speed_ && moving)
    {
        glm::vec2 percent = glm::normalize(input_.move);
        velocity.x = speed_ * percent.x;
        velocity.z = speed_ * percent.y;

        float rotation_y_target = -1*((std::atan2(input_.move.y, input_.move.x) * 180.0f / M_PI) - 90.0f);
        if (rotation_y_target < 0)
            rotation_y_target += 360.0f;
            
        float difference = rotation_y_target - rotation_.y;
        
        // If it's less work to go around the circle than cut through 0
        if (gu::absval(difference) <= 360.0f - gu::absval(difference))
        {
            rotation_.y += difference / turn_speed_;
        }
        // If it's less work to cut through 0
        else
        {
            // calculate proper distance and direction through the 0 cut
            if (rotation_y_target > rotation_.y)
                rotation_.y -= (360.0f - gu::absval(difference) + rotation_.y) / turn_speed_;
            else
                rotation_.y += (360.0f - gu::absval(difference) + rotation_y_target) / turn_speed_;
        }
        
        // Make rotation_.y all positive and between 0 - 360 degrees
        if (rotation_.y < 0)
            rotation_.y += 360.0f;
        else if (rotation_.y > 360)
            rotation_.y -= 360.0f;

        // Only rotate the hitbox if the option is enabled
        if (rotate_hitbox_)
        {
            hitbox_.rotation = rotation_.y;
        }
    }

    // Jump
    if (input_.jump && !jumping_)
    {
        velocity.y = jump_;
        jumping_ = true;
    }

    setVelocity(velocity);
    gu::EntityLogic::move(dt);
    updateHitbox();

    return moving;
}

void Entity::updateHitbox()
{
    hitbox_.centre = gu::EntityLogic::getPosition() + hitbox_offset_;
}

bool Entity::collide(const gu::AABB &object)
{
    glm::vec3 mtv;
    bool ret = gu::EntityLogic::collide(hitbox_, gu::toYOBB(object), &mtv);

    hitbox_.centre -= mtv;
    gu::EntityLogic::setPosition(gu::EntityLogic::getPosition() - mtv);

    return ret;
}

bool Entity::collide(const gu::YOBB &object)
{
    glm::vec3 mtv;
    bool ret = gu::EntityLogic::collide(hitbox_, object, &mtv);

    hitbox_.centre -= mtv;
    gu::EntityLogic::setPosition(gu::EntityLogic::getPosition() - mtv);

    return ret;
}

void Entity::update(const float dt)
{
    last_position_ = position_;
    position_ = gu::EntityLogic::getPosition();

    cullbox_.centre = gu::EntityLogic::getPosition() + cullbox_offset_;
}

void Entity::loadAnimation(gu::Animation *animation, float blend_time, gu::AnimationLayer index)
{
    armature_.loadAnimation(animation, blend_time, index);
}

void Entity::removeOverloadedAnimation(float blend_time)
{
    armature_.removeOverloadedAnimation(blend_time);
}

void Entity::applyTurnSpeed(bool b)
{
    apply_turn_speed_ = b;
}

// Virtual

void Entity::onLand()
{
    jumping_ = false;
}

// Setters

void Entity::setArmature(const gu::Armature &armature)
{
    armature_ = armature;
    loaded_armature_ = true;
}

void Entity::setPosition(const glm::vec3 &position, bool snap)
{
    gu::EntityLogic::setPosition(position);
    position_ = position;

    if (snap)
       last_position_ = position; 

    cullbox_.centre = gu::EntityLogic::getPosition() + cullbox_offset_;
}

void Entity::setRotation(const glm::vec3 &rotation)
{
    rotation_ = rotation;
}

void Entity::setScaling(const glm::vec3 &scaling)
{
    scaling_ = scaling;
}

void Entity::setColour(const glm::vec4 &colour)
{
    colour_ = colour;
}

void Entity::setHitboxRadius(const glm::vec3 &radius)
{
    hitbox_.radius = radius;
}

void Entity::setHitboxOffset(const glm::vec3 &offset)
{
    hitbox_offset_ = offset;
}

void Entity::setHitboxRotation(float r)
{
    hitbox_.rotation = r;
}

void Entity::setCullboxRadius(const glm::vec3 &radius)
{
    cullbox_.radius = radius;
}

void Entity::setCullboxOffset(const glm::vec3 &offset)
{
    cullbox_offset_ = offset;
}

void Entity::setRotateHitbox(bool b)
{
    rotate_hitbox_ = b;
}

void Entity::setSpeed(float speed)
{
    speed_ = speed;
}

void Entity::setJump(float jump)
{
    jump_ = jump;
}

void Entity::setTurnSpeed(float t)
{
    if (t < 1.0f)
        t = 1.0f;
        
    turn_speed_ = t;
}

// Getters

gu::Shader* Entity::getShader()
{
    return model_->getShader();
}

gu::Armature* Entity::getArmature()
{
    return &armature_;
}

const glm::vec3& Entity::getPosition() const
{
    return position_;
}

const glm::vec3& Entity::getRotation() const
{
    return rotation_;
}

const glm::vec3& Entity::getScaling() const
{
    return scaling_;
}

const glm::vec4& Entity::getColour() const
{
    return colour_;
}

const EntityInput& Entity::getEntityInput() const
{
    return input_;
}

const gu::YOBB& Entity::getHitbox() const
{
    return hitbox_;
}

const glm::vec3& Entity::getHitboxOffset() const
{
    return hitbox_offset_;
}

float Entity::getHitboxRotation() const
{
    return hitbox_.rotation;
}

const gu::AABB& Entity::getCullbox() const
{
    return cullbox_;
}

const glm::vec3& Entity::getCullboxOffset() const
{
    return cullbox_offset_;
}

bool Entity::getRotateHitbox() const
{
    return rotate_hitbox_;
}

float Entity::getSpeed() const
{
    return speed_;
}

float Entity::getJump() const
{
    return jump_;
}

gu::Model* Entity::getModel()
{
    return model_;
}

float Entity::getTurnSpeed() const
{
    return turn_speed_;
}

gu::Animation* Entity::getLoadedAnimation(gu::AnimationLayer index)
{
    return armature_.getLoadedAnimation(index);
}

bool Entity::isAnimating(const gu::Animation *anim, gu::AnimationLayer index) const
{
    return armature_.isAnimating(anim);
}

}

