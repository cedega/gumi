#include "GUMI/Engine.h"

namespace gu
{

int receiveMobileEvents(void *engine, SDL_Event *event);
    
Engine::Engine()
{
    init();
}

Engine::Engine(const std::string &zip_path, const std::string &zip_password)
{
    init();
	gu::Zip::setZipPath(zip_path);
    gu::Zip::setZipPassword(zip_password);
}

Engine::~Engine()
{
    for (unsigned int i = 0; i < gu::Input::getControllers()->size(); i++)
        SDL_GameControllerClose(gu::Input::getControllers()->at(i));

    gu::Shader::deleteDefaultShaders();
    gu::DrawRect::destroy();
    gu::TextureLoader::deleteAllTextures();
    SDL_GL_DeleteContext(context_);
	SDL_DestroyWindow(window_);
    Mix_CloseAudio();
    TTF_Quit();
}

void Engine::init()
{
    alive_ = true;
    reset_accumulator_ = false;
    fullscreen_ = false;

    context_ = NULL;
    window_ = NULL;
    window_title_ = "GUMI";

    accumulator_ = 0.0;
    frame_time_ = 0;
    dt_ = 1.0f / 60.0f;
    enabled_ = true;
    fps_limit_ = 0;
    vsync_ = false;
    frame_count_ = 0;

    aspect_ratio_ = 0.0f;
    perspective_2d_ = glm::mat4(1.0f);

    setSeed(time(NULL));

    SDL_SetEventFilter(receiveMobileEvents, this);

    if (SDL_Init(SDL_INIT_EVERYTHING) != 0)
        SDL_Log("[GUMI] (Engine) Error: SDL could not initialize: %s", SDL_GetError());

    if (Mix_OpenAudio(44100, MIX_DEFAULT_FORMAT, 2, 4096) != 0)
        SDL_Log("[GUMI] (Engine) Error: SDL_mixer could not initialize");

    if (TTF_Init() != 0)
        SDL_Log("[GUMI] (Engine) Error: SDL_ttf could not initialize");

    gu::attachEngine(this);
}

void Engine::initOpenGL()
{
    // --------------------
    // Create OpenGL Context
    // --------------------
    
    int opengl_version = 0;

#if defined(TARGET_OS_IPHONE) || defined(__ANDROID__)
   
    SDL_GL_DeleteContext(context_);
    SDL_GL_SetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, 2);
    SDL_GL_SetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, 0);
    context_ = SDL_GL_CreateContext(window_);
    opengl_version = 2;

    if (context_ == NULL)
        SDL_Log("[GUMI] (Engine) Error: OpenGL ES 2.0 Context Creation Failed");

#else

    int opengl_minor_version = 0;
    
    SDL_GL_SetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, 3);
    SDL_GL_SetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, 2);
    context_ = SDL_GL_CreateContext(window_);
    
    if (context_ == NULL)
    {
        SDL_Log("[GUMI] (Engine) OpenGL 3.2 Context Creation Failed. Trying lower version...");
        SDL_GL_DeleteContext(context_);
        SDL_GL_SetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, 3);
        SDL_GL_SetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, 1);
        context_ = SDL_GL_CreateContext(window_);

        if (context_ == NULL)
        {
            SDL_Log("[GUMI] (Engine) OpenGL 3.1 Context Creation Failed. Trying lower version...");
            SDL_GL_DeleteContext(context_);
            SDL_GL_SetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, 3);
            SDL_GL_SetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, 0);
            context_ = SDL_GL_CreateContext(window_);

            if (context_ == NULL)
            {
                SDL_Log("[GUMI] (Engine) OpenGL 3.0 Context Creation Failed. Trying lower version...");
                SDL_GL_DeleteContext(context_);
                SDL_GL_SetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, 2);
                SDL_GL_SetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, 1);
                context_ = SDL_GL_CreateContext(window_);

                if (context_ == NULL)
                {
                    SDL_Log("[GUMI] (Engine) Error: OpenGL 2.1 Context Creation Failed -- version is too low to continue execution");
                    kill();
                    return;
                }

                opengl_version = 2;
                opengl_minor_version = 1;
            }
            else
            {
                opengl_version = 3;
                opengl_minor_version = 0;
            }
        }
        else
        {
            opengl_version = 3;
            opengl_minor_version = 1;
        }
    }
    else
    {
        opengl_version = 3;
        opengl_minor_version = 2;
    }

#endif

    SDL_Log("[GUMI] (Engine) %s", (char*)glGetString(GL_VERSION));
    gu::Shader::setOpenGLVersion(opengl_version);

#if !defined(TARGET_OS_IPHONE) && !defined(__ANDROID__)

    glewExperimental = GL_TRUE;
    GLenum error = glewInit();
    if (error != GLEW_OK)
        SDL_Log("[GUMI] (Engine) Error: GLEW Initialization Error");
    glGetError();

#endif

    int x, y;
    SDL_GL_GetDrawableSize(window_, &x, &y);
    render_size_.x = x;
    render_size_.y = y;
    aspect_ratio_ = static_cast<float>(render_size_.x) / render_size_.y;
    perspective_2d_ = glm::ortho(0.0f, static_cast<float>(x), static_cast<float>(y), 0.0f, -1.0f, 1.0f);
   
    glm::ivec2 window_size;
    SDL_GetWindowSize(window_, &window_size.x, &window_size.y);
    
    glm::vec2 render_ratio;
    render_ratio.x = render_size_.x / static_cast<float>(window_size.x);
    render_ratio.y = render_size_.y / static_cast<float>(window_size.y);
    gu::Input::setRenderRatio(render_ratio);

    glViewport(0, 0, render_size_.x, render_size_.y);
    glEnable(GL_DEPTH_TEST);
    glEnable(GL_BLEND);
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
    glDepthFunc(GL_LEQUAL);
    glEnable(GL_CULL_FACE);

#if !defined(TARGET_OS_IPHONE) && !defined(__ANDROID__)

    if (opengl_version <= 3 && opengl_minor_version <= 1)
        glEnable(GL_POINT_SPRITE);

    glEnable(GL_VERTEX_PROGRAM_POINT_SIZE);

#endif

#if defined(__ANDROID__)

    gu::Drawable::initAndroid();

#endif

    gu::Shader::initDefaultShaders();
    gu::DrawRect::init();
    gu::Image::init(render_size_);

    gu::Shader::SHADER_UI->modify("proj", perspective_2d_);
}

void Engine::initWindow(const std::string &window_title, int render_width, int render_height, int msaa)
{
    window_title_ = window_title;
    render_size_.x = render_width;
    render_size_.y = render_height;
    aspect_ratio_ = static_cast<float>(render_size_.x) / render_size_.y;

#if !defined(TARGET_OS_IPHONE) && !defined(__ANDROID__)

    if (msaa > 0)
    {
        SDL_GL_SetAttribute(SDL_GL_MULTISAMPLEBUFFERS, 1);
        SDL_GL_SetAttribute(SDL_GL_MULTISAMPLESAMPLES, msaa);
    }

#endif

    SDL_SetHint("SDL_HINT_ORIENTATIONS", "LandscapeLeft LandscapeRight");

#if !defined(TARGET_OS_IPHONE) && !defined(__ANDROID__)

    SDL_GL_SetAttribute(SDL_GL_CONTEXT_PROFILE_MASK, SDL_GL_CONTEXT_PROFILE_CORE);

#else

    SDL_GL_SetAttribute(SDL_GL_CONTEXT_PROFILE_MASK, SDL_GL_CONTEXT_PROFILE_ES);

#endif

    SDL_GL_SetAttribute(SDL_GL_DEPTH_SIZE, 24);
    SDL_GL_SetAttribute(SDL_GL_DOUBLEBUFFER, 1);

#if defined(TARGET_OS_IPHONE) || defined(__ANDROID__)

    window_ = SDL_CreateWindow(window_title_.c_str(), SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, 0, 0, SDL_WINDOW_OPENGL | SDL_WINDOW_FULLSCREEN_DESKTOP | SDL_WINDOW_ALLOW_HIGHDPI);

#else

    window_ = SDL_CreateWindow(window_title_.c_str(), SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED, render_width, render_height, SDL_WINDOW_OPENGL | SDL_WINDOW_SHOWN | SDL_WINDOW_ALLOW_HIGHDPI);

#endif
    
    if (window_ == NULL)
        SDL_Log("[GUMI] (Engine) Error: Failed to create SDL_Window");
    
    initOpenGL();

#if !defined(TARGET_OS_IPHONE) && !defined(__ANDROID__)

    if (msaa > 0)
        glEnable(GL_MULTISAMPLE);

#endif

    gu::Input::initControllers();
}

int receiveMobileEvents(void *engine, SDL_Event *event)
{
    gu::Engine *e;
    
    switch (event->type)
    {
        case SDL_APP_TERMINATING:
            /* Terminate the app.
             Shut everything down before returning from this function.
             */
            return 0;
        case SDL_APP_LOWMEMORY:
            /* You will get this when your app is paused and iOS wants more memory.
             Release as much memory as possible.
             */
            return 0;
        case SDL_APP_WILLENTERBACKGROUND:
            /* Prepare your app to go into the background.  Stop loops, etc.
             This gets called when the user hits the home button, or gets a call.
             */
            e = reinterpret_cast<gu::Engine*>(engine);
            e->disable();
            glFinish();
            return 0;
        case SDL_APP_DIDENTERBACKGROUND:
            /* This will get called if the user accepted whatever sent your app to the background.
             If the user got a phone call and canceled it, you'll instead get an    SDL_APP_DIDENTERFOREGROUND event and restart your loops.
             When you get this, you have 5 seconds to save all your state or the app will be terminated.
             Your app is NOT active at this point.
             */
            return 0;
        case SDL_APP_WILLENTERFOREGROUND:
            /* This call happens when your app is coming back to the foreground.
             Restore all your state here.
             */
            return 0;
        case SDL_APP_DIDENTERFOREGROUND:
            /* Restart your loops here.
             Your app is interactive and getting CPU again.
             */
            e = reinterpret_cast<gu::Engine*>(engine);
            e->enable();
            return 0;
        default:
            return 1;
    }
}

void Engine::runGameLoop()
{
    Uint32 current_time = SDL_GetTicks();
    Uint32 new_time;
    float frame_timer = 0.0f;
    unsigned int frame_tick = 0;
    
    SDL_Event event;

    while (alive_)
    {
        new_time = SDL_GetTicks();
        frame_time_ = (new_time - current_time) / 1000.0f;
        current_time = new_time;

        // Prevent Spiral of Death
        if (frame_time_ > 0.25f)
        {
            frame_time_ = 0.25f;
        }

        accumulator_ += frame_time_;
        frame_timer += frame_time_;
        frame_tick++;
        
        if (frame_timer > 1.0f)
        {
            frame_count_ = frame_tick;
            frame_tick = 0;
            frame_timer = 0.0f;
        }

        if (reset_accumulator_)
        {
            accumulator_ = 0;
            reset_accumulator_ = false;
        }
        
        // Force only 1 update loop if the device is not suppose to be rendering
        if (!enabled_)
            accumulator_ = dt_;
        
        // Run Logic at dt frequency
        while (accumulator_ >= dt_)
        {
            gu::Input::touch_fingers_.clear();
            
            while (SDL_PollEvent(&event))
            {
                if (event.type == SDL_FINGERDOWN || event.type == SDL_FINGERUP || event.type == SDL_FINGERMOTION)
                {
                    gu::Finger temp;
                    temp.type = event.tfinger.type;
                    temp.index = event.tfinger.fingerId;
                    temp.x = event.tfinger.x * render_size_.x;
                    temp.y = event.tfinger.y * render_size_.y;
                    temp.dx = event.tfinger.dx * render_size_.x;
                    temp.dy = event.tfinger.dy * render_size_.y;
                    gu::Input::touch_fingers_.push_back(temp);
                }
                else if (event.type == SDL_CONTROLLERDEVICEADDED)
                {
                    gu::Input::addController(event.cdevice.which);
                }
                else if (event.type == SDL_CONTROLLERDEVICEREMOVED)
                {
                    gu::Input::removeController(event.cdevice.which);
                }
                else
                    events(event);
            }
            
            if (enabled_)
                logic(dt_);

            accumulator_ -= dt_;
        }

        if (enabled_)
        {
            glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
            render(accumulator_ / dt_, frame_time_);
            SDL_GL_SwapWindow(window_);
        }

        delay(new_time);
    }
}

void Engine::delay(Uint32 new_time)
{
    if (!enabled_)
    {
        SDL_Delay(100);
    }
    else if (fps_limit_ > 0)
    {
        int cap_diff = (1000.0f / fps_limit_) - (SDL_GetTicks() - new_time);
        if (cap_diff > 0)
            SDL_Delay(cap_diff);
    }
}

void Engine::kill()
{
    alive_ = false;
}

void Engine::resetAccumulator()
{
    reset_accumulator_ = true; 
}
    
void Engine::disable()
{
    enabled_ = false;
}

void Engine::enable()
{
    enabled_ = true;
}
    
// Setters

void Engine::setFullScreen(bool b)
{
    fullscreen_ = b;
    
    if (fullscreen_)
    {
        SDL_SetWindowFullscreen(window_, SDL_WINDOW_FULLSCREEN);

        int x,y;
        SDL_GetWindowSize(window_, &x, &y);
        glViewport(0, 0, x, y);
    }
    else
        SDL_SetWindowFullscreen(window_, 0);
}

void Engine::setLogicAccumulator(float f)
{
    accumulator_ = f;
}

void Engine::setSeed(unsigned int seed)
{
    seed_ = seed;
    std::srand(seed);
}
    
void Engine::setPhysicsDt(float dt)
{
    dt_ = dt;
}
    
void Engine::setFPSLimit(int fps)
{
    fps_limit_ = fps;
}

bool Engine::setVerticalSync(bool v)
{
    if (v)
    {
        if (SDL_GL_SetSwapInterval(-1) == -1)
        {
            if (SDL_GL_SetSwapInterval(1) == -1)
            {
                vsync_ = false;
                return vsync_;
            }
        }

        vsync_ = true;
        return vsync_;
    }
    else
    {
        vsync_ = !(SDL_GL_SetSwapInterval(0) == 0);
        return vsync_;
    }
}

void Engine::setWindowTitle(const std::string &title)
{
    SDL_SetWindowTitle(window_,title.c_str());
}

void Engine::setWindowIcon(const std::string &path_in_zip)
{
    SDL_Log("[GUMI] (Engine) Error: setWindowIcon is unimplemented");
}

void Engine::setClearColour(float r, float g, float b, float a)
{
    glClearColor(r, g, b, a);
}

// Getters

bool Engine::getFullScreen() const
{
    return fullscreen_;
}

float Engine::getFrameTime() const
{
    return frame_time_;
}

float Engine::getLogicAccumulator() const
{
    return accumulator_;
}

unsigned int Engine::getSeed() const
{
    return seed_;
}

float Engine::getPhysicsDt() const
{
    return dt_;
}
    
int Engine::getFPSLimit() const
{
    return fps_limit_;
}

bool Engine::getVerticalSync() const
{
    return vsync_;
}
    
const glm::ivec2& Engine::getRenderSize() const
{
    return render_size_;
}
    
glm::ivec2 Engine::getWindowSize() const
{
    glm::ivec2 size;
    SDL_GetWindowSize(window_, &size.x, &size.y);
    return size;
}
    
glm::vec2 Engine::getWindowRatio() const
{
    glm::vec2 ratio;
    glm::ivec2 window_size = getWindowSize();
    ratio.x = static_cast<float>(window_size.x)/render_size_.x;
    ratio.y = static_cast<float>(window_size.y)/render_size_.y;
    return ratio;
}

unsigned int Engine::getFPS() const
{
    return frame_count_;
}

SDL_Window* Engine::getWindow()
{
    return window_;
}

const glm::mat4& Engine::getPerspective2D() const
{
    return perspective_2d_;
}

float Engine::getAspectRatio() const
{
    return aspect_ratio_;
}

}
