#include "GUMI/IOSystem.h"
#include <iostream>

namespace gu
{

IOStream::IOStream(SDL_RWops *rw)
{
    rw_ = rw;
}

IOStream::~IOStream()
{
    SDL_FreeRW(rw_);
}

size_t IOStream::FileSize() const
{
    if (rw_ == NULL)
        return 0;

    return rw_->size(rw_);
}

void IOStream::Flush()
{
    // NONE
}

size_t IOStream::Read(void *pvBuffer, size_t pSize, size_t pCount)
{
    return rw_->read(rw_, pvBuffer, pSize, pCount);
}

aiReturn IOStream::Seek(size_t pOffset, aiOrigin pOrigin)
{
    return aiReturn_FAILURE;
}

size_t IOStream::Tell() const
{
    return 0;
}

size_t IOStream::Write(const void *pvBuffer, size_t pSize, size_t pCount)
{
    return 0;
}

IOSystem::IOSystem()
{

}

void IOSystem::Close(Assimp::IOStream *pFile)
{
    delete pFile;
}

bool IOSystem::Exists(const char *pFile) const
{
    SDL_RWops *rw = SDL_RWFromFile(pFile, "rb");

    if (rw == NULL)
        return false;
    else
    {
        SDL_FreeRW(rw);
        return true;
    }
}

char IOSystem::getOsSeparator() const
{

#if defined(_WIN32)

    return '\\';

#else

    return '/';

#endif

}

Assimp::IOStream* IOSystem::Open(const char *pFile, const char *pMode)
{
    SDL_RWops *rw = SDL_RWFromFile(pFile, pMode);

    if (rw == NULL)
        return NULL;

    return new gu::IOStream(rw);
}

}
