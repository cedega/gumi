#include "GUMI/TextureLoader.h"

namespace gu
{

std::vector<gu::TextureInfo> TextureLoader::textures_;
GLenum TextureLoader::filter_ = GL_NEAREST;

GLuint TextureLoader::loadFileTexture(const std::string &file_path, bool cache)
{
    if (gu::Zip::getRedirectFileLoading())
        return loadZipTexture(file_path, cache);

    gu::TextureInfo *ti = getTextureInfo(file_path);
    if (ti != NULL)
    {
        ti->count++;
        return ti->id;
    }

#if defined(TARGET_OS_IPHONE) || defined(__ANDROID__)

    SDL_RWops *rw = SDL_RWFromFile(file_path.c_str(), "rb");
    
    if (rw == NULL)
    {
        SDL_Log("[GUMI] (TextureLoader) Error: Could not load \"%s\" as a texture", file_path.c_str());
        return 0;
    }
    
    SDL_Surface *surface = IMG_Load_RW(rw, 0);
    SDL_RWclose(rw);

#else

    SDL_Surface *surface = IMG_Load(file_path.c_str());

#endif
    
    if (surface == NULL)
    {
        SDL_Log("[GUMI] (TextureLoader) Error: Could not load \"%s\" from file (IMG_Load error)", file_path.c_str());
        return 0;
    }

    if (surface->format->Amask == 0)
    {
        SDL_Log("[GUMI] (TextureLoader) Error: Could not load \"%s\" from file: image must have an alpha channel", file_path.c_str());
        return 0;
    }

    return cache ? loadSurface(surface, file_path) : loadSurface(surface, "");
}

GLuint TextureLoader::loadZipTexture(const std::string &zip_path, bool cache)
{
    gu::TextureInfo *ti = getTextureInfo(zip_path);
    if (ti != NULL)
    {
        ti->count++;
        return ti->id;
    }

#if defined(TARGET_OS_IPHONE) || defined(__ANDROID__)

    SDL_RWops *rw = SDL_RWFromFile(zip_path.c_str(), "rb");
    
    if (rw == NULL)
    {
        SDL_Log("[GUMI] (TextureLoader) Error: Could not load \"%s\" as a texture", zip_path.c_str());
        return 0;
    }
    
    SDL_Surface *surface = IMG_Load_RW(rw, 0);
    SDL_RWclose(rw);

#else

    gu::Zip zip(zip_path);
    SDL_RWops *rw = SDL_RWFromConstMem(zip.getBuffer(), zip.getSize());
    SDL_Surface *surface = IMG_Load_RW(rw, 0);
    SDL_RWclose(rw);
    free(zip.getBuffer());

#endif

    if (surface == NULL)
    {
        SDL_Log("[GUMI] (TextureLoader) Error: Could not load \"%s\" from file (IMG_Load error)", zip_path.c_str());
        return 0;
    }

    if (surface->format->Amask == 0)
    {
        SDL_Log("[GUMI] (TextureLoader) Error: Could not load \"%s\" from file: image must have an alpha channel", zip_path.c_str());
        return 0;
    }

    return cache ? loadSurface(surface, zip_path) : loadSurface(surface, "");
}

GLuint TextureLoader::loadSurface(SDL_Surface *surface, const std::string &file_path)
{
    if (!file_path.empty() && getTextureInfo(file_path) != NULL)
        return 0;

    GLuint texture;
    
    glGenTextures(1, &texture);
    glBindTexture(GL_TEXTURE_2D, texture);

#ifdef TARGET_OS_IPHONE

    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, surface->w, surface->h, 0, GL_BGRA, GL_UNSIGNED_BYTE, surface->pixels);

#else

    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, surface->w, surface->h, 0, GL_RGBA, GL_UNSIGNED_BYTE, surface->pixels);

#endif

    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
    
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, filter_);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, filter_);
    
    // Only cache if a file name is supplied
    if (file_path.length() > 0)
    {
        gu::TextureInfo ti;
        ti.id = texture;
        ti.name = file_path;
        ti.w = surface->w;
        ti.h = surface->h;
        ti.count = 1;

        textures_.push_back(ti);
    }

    SDL_FreeSurface(surface);
    return texture;
}
    
GLuint TextureLoader::loadPixels(void *pixels, int width, int height)
{
    GLuint texture;
    
    glGenTextures(1, &texture);
    glBindTexture(GL_TEXTURE_2D, texture);
    
#ifdef TARGET_OS_IPHONE

    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, width, height, 0, GL_BGRA, GL_UNSIGNED_BYTE, pixels);

#else

    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, width, height, 0, GL_RGBA, GL_UNSIGNED_BYTE, pixels);

#endif
    
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, filter_);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, filter_);

    return texture;
}

void TextureLoader::deleteTexture(GLuint texture_id)
{
    for (unsigned int i = 0; i < textures_.size(); i++)
    {
        if (textures_[i].id == texture_id)
        {
            textures_[i].count -= 1;

            if (textures_[i].count <= 0)
            {
                glDeleteTextures(1, &textures_[i].id);
                textures_.erase(textures_.begin() + i);
            }

            break;
        }
    }
}

void TextureLoader::deleteTexture(const std::string &texture_name)
{
    for (unsigned int i = 0; i < textures_.size(); i++)
    {
        if (textures_[i].name.compare(texture_name) == 0)
        {
            textures_[i].count -= 1;

            if (textures_[i].count <= 0)
            {
                glDeleteTextures(1, &textures_[i].id);
                textures_.erase(textures_.begin() + i);
            }

            break;
        }
    }
}

void TextureLoader::deleteAllTextures()
{
    if (!textures_.empty())
    {
        for (unsigned int i = 0; i < textures_.size(); i++)
            glDeleteTextures(1, &textures_[i].id);
    }

    textures_.clear();
}

gu::TextureInfo* TextureLoader::getTextureInfo(GLuint texture_id)
{
    for (unsigned int i = 0; i < textures_.size(); i++)
        if (textures_[i].id == texture_id)
            return &textures_[i];

    return NULL;
}

gu::TextureInfo* TextureLoader::getTextureInfo(const std::string &texture_name)
{
    for (unsigned int i = 0; i < textures_.size(); i++)
        if (textures_[i].name.compare(texture_name) == 0)
            return &textures_[i];

    return NULL;
}

void TextureLoader::setTextureFilter(GLenum filter)
{
    filter_ = filter;
}

GLenum TextureLoader::getTextureFilter()
{
    return filter_;
}

unsigned int TextureLoader::getTextureCount()
{
    return textures_.size();
}

}
