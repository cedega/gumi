#include "GUMI/Octree.h"

namespace gu
{

int Octree::comparisons = 0;

Octree::Octree()
{
    parent_ = NULL;
    for (int i = 0; i < 8; i++)
        children_[i] = NULL;
    type_ = gu::QUADTREE_NONE;
}

Octree::~Octree()
{
    deconstruct();
}

void Octree::deconstruct()
{
    for (int i = 0; i < 8; i++)
        if (children_[i] != NULL)
            delete children_[i];
}

void Octree::draw()
{
    if (type_ != gu::QUADTREE_NONE)
    {
        glm::vec3 radius = size_.radius;

        if (size_.radius.x >= FLT_MAX)
            size_.radius.x = 0.0f;
        else if (size_.radius.y >= FLT_MAX)
            size_.radius.y = 0.0f;
        else if (size_.radius.z >= FLT_MAX)
            size_.radius.z = 0.0f;

        gu::DrawRect::draw(size_);
        size_.radius = radius;

        if (children_[0] != NULL)
        for (int i = 0; i < 4; i++)
            children_[i]->draw();
    }
    else
    {
        gu::DrawRect::draw(size_);

        if (children_[0] != NULL)
        for (int i = 0; i < 8; i++)
            children_[i]->draw();
    }
}

void Octree::init(const gu::AABB &size, const std::vector<gu::YOBB> &objects, int depth, gu::Octree *parent)
{
    std::vector<gu::AABB> aabbs;
    aabbs.resize(objects.size());
    
    for (unsigned int i = 0; i < objects.size(); i++)
    {
        aabbs[i] = gu::toAABB(objects[i]);
    }
    
    if (type_ == gu::QUADTREE_NONE)
        make8(size, aabbs, depth, parent);
    else
        make4(size, aabbs, depth, parent);
}

void Octree::make4(const gu::AABB &size, const std::vector<gu::AABB> &objects, int depth, gu::Octree *parent)
{
    size_ = size;
    parent_ = parent;

    // Break if there's less than 5 objects in the quad section
    if (objects.size() < 10 || depth > 3)
    {
        objects_ = objects;
        return;
    }

    gu::AABB octs[4];

    const glm::vec3 &centre = size_.centre;
    glm::vec3 radius = size_.radius / 2.0f;

    switch (type_)
    {
    case gu::QUADTREE_XY: 
        size_.radius.z = FLT_MAX;
        octs[0] = gu::AABB(centre + glm::vec3(-radius.x, -radius.y, 0.0f), glm::vec3(radius.x, radius.y, FLT_MAX));
        octs[1] = gu::AABB(centre + glm::vec3(radius.x, -radius.y, 0.0f), glm::vec3(radius.x, radius.y, FLT_MAX));
        octs[2] = gu::AABB(centre + glm::vec3(-radius.x, radius.y, 0.0f), glm::vec3(radius.x, radius.y, FLT_MAX));
        octs[3] = gu::AABB(centre + glm::vec3(radius.x, radius.y, 0.0f), glm::vec3(radius.x, radius.y, FLT_MAX));
        break;
    case gu::QUADTREE_XZ: 
        size_.radius.y = FLT_MAX;
        octs[0] = gu::AABB(centre + glm::vec3(-radius.x, 0.0f, -radius.z), glm::vec3(radius.x, FLT_MAX, radius.z));
        octs[1] = gu::AABB(centre + glm::vec3(radius.x, 0.0f, -radius.z), glm::vec3(radius.x, FLT_MAX, radius.z));
        octs[2] = gu::AABB(centre + glm::vec3(-radius.x, 0.0f, radius.z), glm::vec3(radius.x, FLT_MAX, radius.z));
        octs[3] = gu::AABB(centre + glm::vec3(radius.x, 0.0f, radius.z), glm::vec3(radius.x, FLT_MAX, radius.z));
        break;
    case gu::QUADTREE_YZ: 
        size_.radius.x = FLT_MAX;
        octs[0] = gu::AABB(centre + glm::vec3(0.0f, -radius.y, -radius.z), glm::vec3(FLT_MAX, radius.y, radius.z));
        octs[1] = gu::AABB(centre + glm::vec3(0.0f, radius.y, -radius.z), glm::vec3(FLT_MAX, radius.y, radius.z));
        octs[2] = gu::AABB(centre + glm::vec3(0.0f, -radius.y, radius.z), glm::vec3(FLT_MAX, radius.y, radius.z));
        octs[3] = gu::AABB(centre + glm::vec3(0.0f, radius.y, radius.z), glm::vec3(FLT_MAX, radius.y, radius.z));
        break;
    default: break;
    }

    std::vector<gu::AABB> buckets[4];

    // Process Objects
    bool added;
    for (unsigned int i = 0; i < objects.size(); i++)
    {
        added = false;
        
        for (int j = 0; j < 4; j++)
        {
            comparisons++;
            // If object intersects or is within a subtree
            if (gu::contains(objects[i], octs[j]))
            {
               buckets[j].push_back(objects[i]);
               added = true;
               break;
            }
        }
        
        if (!added)
            objects_.push_back(objects[i]);
    }

    for (int i = 0; i < 4; i++)
    {
        children_[i] = new Octree();
        children_[i]->convertToQuadTree(type_);
        children_[i]->make4(octs[i], buckets[i], depth+1, this);
    }
}

void Octree::make8(const gu::AABB &size, const std::vector<gu::AABB> &objects, int depth, gu::Octree *parent)
{
    size_ = size;
    parent_ = parent;

    // Break if there's less than 5 objects in the quad section
    if (objects.size() < 10 || depth > 3)
    {
        objects_ = objects;
        return;
    }

    gu::AABB octs[8];

    // Top-left-front
    octs[0] = gu::AABB(size_.centre + glm::vec3(-size_.radius.x / 2.0f, size_.radius.y / 2.0f, -size_.radius.z / 2.0f), size_.radius / 2.0f);
    // Top-left-back
    octs[1] = gu::AABB(size_.centre + glm::vec3(-size_.radius.x / 2.0f, -size_.radius.y / 2.0f, -size_.radius.z / 2.0f), size_.radius / 2.0f);

    // Top-right-front
    octs[2] = gu::AABB(size_.centre + glm::vec3(size_.radius.x / 2.0f, size_.radius.y / 2.0f, -size_.radius.z / 2.0f), size_.radius / 2.0f);
    // Top-right-back
    octs[3] = gu::AABB(size_.centre + glm::vec3(size_.radius.x / 2.0f, -size_.radius.y / 2.0f, -size_.radius.z / 2.0f), size_.radius / 2.0f);

    // Bot-left-front
    octs[4] = gu::AABB(size_.centre + glm::vec3(-size_.radius.x / 2.0f, size_.radius.y / 2.0f, size_.radius.z / 2.0f), size_.radius / 2.0f);
    // Bot-left-back
    octs[5] = gu::AABB(size_.centre + glm::vec3(-size_.radius.x / 2.0f, -size_.radius.y / 2.0f, size_.radius.z / 2.0f), size_.radius / 2.0f);

    // Bot-right-front
    octs[6] = gu::AABB(size_.centre + glm::vec3(size_.radius.x / 2.0f, size_.radius.y / 2.0f, size_.radius.z / 2.0f), size_.radius / 2.0f);
    // Bot-right-back
    octs[7] = gu::AABB(size_.centre + glm::vec3(size_.radius.x / 2.0f, -size_.radius.y / 2.0f, size_.radius.z / 2.0f), size_.radius / 2.0f);

    std::vector<gu::AABB> buckets[8];

    // Process Objects
    bool added;
    for (unsigned int i = 0; i < objects.size(); i++)
    {
        added = false;
        for (int j = 0; j < 8; j++)
        {
            comparisons++;
            // If object intersects or is within a subtree
            if (gu::contains(objects[i], octs[j]))
            {
                buckets[j].push_back(objects[i]);
                added = true;
                break;
            }
        }
        
        if (!added)
            objects_.push_back(objects[i]);
    }

    for (int i = 0; i < 8; i++)
    {
        children_[i] = new Octree();
        children_[i]->make8(octs[i], buckets[i], depth+1, this);
    }
}

void Octree::update(const std::vector<gu::YOBB> &objects)
{
    deconstruct();
    comparisons = 0;
    init(size_, objects, 0, NULL);
}

void Octree::convertToQuadTree(QUADTREE_TYPE t)
{
    type_ = t;
}

}
