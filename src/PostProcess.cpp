#include "GUMI/PostProcess.h"

namespace gu
{

PostProcess::PostProcess()
{
    initialized_ = false;
    frame_buffer_ = 0;
}

PostProcess::~PostProcess()
{
    if (initialized_)
    {
        glDeleteTextures(1, &texture_);
        glDeleteTextures(1, &depth_texture_);
        glDeleteFramebuffers(1, &frame_buffer_);
        glDeleteRenderbuffers(1, &depth_buffer_);
    }
}

void PostProcess::draw()
{
    image_.draw();
}

void PostProcess::drawDepth()
{
    depth_image_.draw();
}

void PostProcess::init(const glm::vec2 &resolution, gu::Shader *shader, gu::Shader *depth_shader, const glm::vec3 &blank_colour)
{

#if !defined(TARGET_OS_IPHONE) && !defined(__ANDROID__)

    // Init Shader
    shader->modify("proj", glm::ortho(-resolution.x/2.0f, resolution.x/2.0f, -resolution.y/2.0f, resolution.y/2.0f, -1.0f, 1.0f));
    shader->modify("resolution", resolution);

    // Create Frame Buffer & Texture
    glGenFramebuffers(1, &frame_buffer_);
    glBindFramebuffer(GL_FRAMEBUFFER, frame_buffer_);

    glGenTextures(1, &texture_);
    glBindTexture(GL_TEXTURE_2D, texture_);
     
    // Give an empty image to OpenGL ( the last "0" )
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, resolution.x, resolution.y, 0, GL_RGBA, GL_UNSIGNED_BYTE, 0);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
    
    // The depth buffer
    glGenRenderbuffers(1, &depth_buffer_);
    glBindRenderbuffer(GL_RENDERBUFFER, depth_buffer_);
    glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH_COMPONENT, resolution.x, resolution.y);
    glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_RENDERBUFFER, depth_buffer_);

    glGenTextures(1, &depth_texture_);
    glBindTexture(GL_TEXTURE_2D, depth_texture_);
    glTexImage2D(GL_TEXTURE_2D, 0, GL_DEPTH_COMPONENT24, resolution.x, resolution.y, 0, GL_DEPTH_COMPONENT, GL_FLOAT, 0);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);

    // Set "renderedTexture" as our colour attachement #0
    glFramebufferTexture(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, texture_, 0);
    glFramebufferTexture(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, depth_texture_, 0);
     
    // Set the list of draw buffers.
    GLenum draw_buffers[1] = {GL_COLOR_ATTACHMENT0};
    glDrawBuffers(1, draw_buffers); // "1" is the size of draw_buffers

    if (glCheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE)
        SDL_Log("[GUMI] (PostProcess) Error: Failed to initialize frame buffer");

    initialized_ = true;

    const char *v =
    "in vec2 vPosition;\n"
    "in vec4 vColor;\n"
    "in vec2 vTexCoord;\n"
    "uniform mat4 proj;\n"
    "out vec4 color;\n"
    "void main()\n"
    "{\n"
    "    color = vColor;\n"
    "    vec4 newPos = vec4(vPosition, 0.0, 1.0);\n"
    "    gl_Position = proj * newPos;\n"
    "}\n";

    const char *f =
    "in vec4 color;\n"
    "void main()\n"
    "{\n"
    "    gl_FragColor = color;\n"
    "}\n";

    blank_shader_.compile(v, f, "130");
    blank_shader_.modify("proj", glm::ortho(-resolution.x/2.0f, resolution.x/2.0f, -resolution.y/2.0f, resolution.y/2.0f, -1.0f, 1.0f));
    blank_.bindShader(&blank_shader_);
    blank_.loadColour(resolution.x, resolution.y, glm::vec4(blank_colour, 1.0f));

    if (shader != NULL)
    {
        image_.bindShader(shader);
        image_.setTexture(texture_, resolution.x, resolution.y);
    }

    if (depth_shader != NULL)
    {
        depth_image_.bindShader(depth_shader);
        depth_image_.setTexture(depth_texture_, resolution.x, resolution.y);
    }

    glBindFramebuffer(GL_FRAMEBUFFER, 0);
    
#endif

}

void PostProcess::begin()
{
    glBindFramebuffer(GL_FRAMEBUFFER, frame_buffer_);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    blank_.draw();
}

void PostProcess::end()
{
    glBindFramebuffer(GL_FRAMEBUFFER, 0);
}

// Getters

GLuint PostProcess::getTexture() const
{
    return texture_;
}

GLuint PostProcess::getDepthTexture() const
{
    return depth_texture_;
}

}
