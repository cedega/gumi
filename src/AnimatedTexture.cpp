#include "GUMI/AnimatedTexture.h"

namespace gu
{

AnimatedTexture::AnimatedTexture() :
    frame_(0),
    tick_(0.0f),
    looping_(true),
    stopped_(false)
{
}

AnimatedTexture::AnimatedTexture(GLuint texture, float time) :
    frame_(0),
    tick_(0.0f),
    looping_(true),
    stopped_(false)
{
    timings_.push_back(time);
    textures_.push_back(texture);
}

AnimatedTexture::AnimatedTexture(const gu::AnimatedTexture &obj) :
    textures_(obj.textures_),
    timings_(obj.timings_),
    frame_(obj.frame_),
    tick_(obj.tick_),
    looping_(obj.looping_),
    stopped_(obj.stopped_)
{
    for (unsigned int i = 0; i < textures_.size(); i++)
    {
        gu::TextureInfo *ti = gu::TextureLoader::getTextureInfo(textures_[i]);
        if (ti != NULL)
            ti->count++;
    }
}

AnimatedTexture::~AnimatedTexture()
{
    for (unsigned int i = 0; i < textures_.size(); i++)
        gu::TextureLoader::deleteTexture(textures_[i]);
}

AnimatedTexture& AnimatedTexture::operator=(const gu::AnimatedTexture &obj)
{
    for (unsigned int i = 0; i < textures_.size(); i++)
        gu::TextureLoader::deleteTexture(textures_[i]);

    textures_ = obj.textures_;
    timings_ = obj.timings_;
    frame_ = obj.frame_;
    tick_ = obj.tick_;
    looping_ = obj.looping_;
    stopped_ = obj.stopped_;

    for (unsigned int i = 0; i < textures_.size(); i++)
    {
        gu::TextureInfo *ti = gu::TextureLoader::getTextureInfo(textures_[i]);
        if (ti != NULL)
            ti->count++;
    }

    return *this;
}

void AnimatedTexture::addTexture(GLuint texture, float time)
{
    timings_.push_back(time);
    textures_.push_back(texture);
}

void AnimatedTexture::update(const float dt)
{
    if (stopped_)
        return;

    tick_ += dt; 

    if (frame_ < timings_.size())
    {
        while (tick_ >= timings_[frame_])
        {
            tick_ -= timings_[frame_];
            frame_++;    

            if (frame_ >= timings_.size())
            {
                if (looping_)
                    frame_ = 0;
                else
                {
                    frame_--;
                    stopped_ = true;
                    break;
                }
            }
        }
    }
}

void AnimatedTexture::reset()
{
    tick_ = 0.0f;
    frame_ = 0;
}

// Setters

void AnimatedTexture::setLooping(bool b)
{
    looping_ = b;
}

void AnimatedTexture::setFrame(unsigned int i)
{
    frame_ = i;
}

// Getters

bool AnimatedTexture::getLooping() const
{
    return looping_;
}

bool AnimatedTexture::getStopped() const
{
    return stopped_;
}

unsigned int AnimatedTexture::getFrame() const
{
    return frame_;
}

GLuint AnimatedTexture::getCurrentTexture() const
{
    return textures_[frame_];
}

const std::vector<GLuint>* AnimatedTexture::getTextures() const
{
    return &textures_;
}

const std::vector<float>* AnimatedTexture::getTimings() const
{
    return &timings_;
}

}
