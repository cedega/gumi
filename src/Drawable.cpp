#include "GUMI/Drawable.h"

namespace gu
{

#if defined(__ANDROID__)

PFNGLGENVERTEXARRAYSOESPROC Drawable::glGenVertexArraysOES;
PFNGLBINDVERTEXARRAYOESPROC Drawable::glBindVertexArrayOES;
PFNGLDELETEVERTEXARRAYSOESPROC Drawable::glDeleteVertexArraysOES;
PFNGLISVERTEXARRAYOESPROC Drawable::glIsVertexArrayOES;

#endif

#if defined(TARGET_OS_IPHONE) || defined(__ANDROID__)

bool Drawable::has_oes_support_ = true;

#endif

#if defined(__ANDROID__)

void Drawable::initAndroid()
{
    std::string extensions((char*)glGetString(GL_EXTENSIONS));
    if (extensions.find("GL_OES_vertex_array_object") != std::string::npos)
    {
        glBindVertexArrayOES = (PFNGLBINDVERTEXARRAYOESPROC)eglGetProcAddress("glBindVertexArrayOES");
        glDeleteVertexArraysOES = (PFNGLDELETEVERTEXARRAYSOESPROC)eglGetProcAddress("glDeleteVertexArraysOES");
        glGenVertexArraysOES = (PFNGLGENVERTEXARRAYSOESPROC)eglGetProcAddress("glGenVertexArraysOES");
        glIsVertexArrayOES = (PFNGLISVERTEXARRAYOESPROC)eglGetProcAddress("glIsVertexArrayOES");

        has_oes_support_ = true;
    }
    else 
        has_oes_support_ = false;
}

#endif

#if defined(TARGET_OS_IPHONE) || defined(__ANDROID__)

bool Drawable::hasOES()
{
    return has_oes_support_;
}

#endif

}
