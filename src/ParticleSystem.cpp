#include "GUMI/ParticleSystem.h"

namespace gu
{

// ------------------------
// Examples
// ------------------------

void particleSquare(gu::Particle *particle, int index, void *data)
{
    // Life Time
    particle->life_time = gu::random(0.3f, 4.0f);
    
    // Positions
    particle->position.x = gu::random(-5.0f, 5.0f);
    particle->position.y = gu::random(-5.0f, 5.0f);
    particle->position.z = gu::random(-5.0f, 5.0f);
    
    // Accelerations
    particle->acceleration = glm::vec3(0.0f);

    // Colours
    particle->colour.r = gu::random(0.0f, 1.0f);
    particle->colour.g = gu::random(0.0f, 1.0f);
    particle->colour.b = gu::random(0.0f, 1.0f);
    
    // Velocities
    particle->velocity.x = gu::random(-5.0f, 5.0f);
    particle->velocity.y = gu::random(-5.0f, 5.0f);
    particle->velocity.z = gu::random(-5.0f, 5.0f);
}

void particleSphere(gu::Particle* particle, int index, void *data)
{
    const float tick = 360.0f / 500.0f;
    const float u = index * tick;
    const float v = gu::random(0.0f, 180.0f);

    // Life Time
    particle->life_time = gu::random(0.1f, 0.5f);
    
    // Positions
    particle->position = glm::vec3(0.0f);
    
    // Colours
    particle->colour.r = 1.0f;
    particle->colour.g = gu::random(0.0f, 0.7f);
    
    // Velocities
    particle->velocity.x = 15*std::cos(glm::radians(u))*std::sin(glm::radians(v));
    particle->velocity.y = 15*std::cos(glm::radians(v));
    particle->velocity.z = 15*std::sin(glm::radians(u))*std::sin(glm::radians(v));
    
    // Accelerations
    particle->acceleration = -3.0f*particle->velocity;
}

// ------------------------
// Examples End
// ------------------------

ParticleSystem::ParticleSystem() :
    shader_(NULL),
    initialized_(false),
    loaded_vao_(false),
    point_size_(1.0f),
    use_texture_(false),
    num_particles_(0),
    active_(false),
    additive_blending_(false),
    continuous_(false),
    data_(NULL)
{
}

ParticleSystem::ParticleSystem(const ParticleSystem &obj) :
    gu::Drawable(obj),
    particles_(obj.particles_),
    particle_data_(obj.particle_data_),
    shader_(obj.shader_),
    point_size_(obj.point_size_),
    use_texture_(obj.use_texture_),
    num_particles_(obj.num_particles_),
    active_(obj.active_),
    additive_blending_(obj.additive_blending_),
    continuous_(obj.continuous_),
    emitter_position_(obj.emitter_position_),
    last_emitter_position_(obj.last_emitter_position_)
{
    if (obj.initialized_)
    {
        if (obj.use_texture_)
        {
            loadTexture(obj.texture_);

            gu::TextureInfo *info = gu::TextureLoader::getTextureInfo(obj.texture_);
            info->count++;
        }

        create(particles_.size(), obj.particleSet_, obj.data_, obj.shader_);
    }
    else
    {
        initialized_ = false;
        data_ = NULL;
        loaded_vao_ = false;
    }
}

ParticleSystem::~ParticleSystem()
{
    deconstruct();
}

void ParticleSystem::deconstruct()
{
    if (initialized_)
        glDeleteBuffers(1, &vbo_);
        
    if (loaded_vao_)
    {

#if !defined(TARGET_OS_IPHONE) && !defined(__ANDROID__)

        glDeleteVertexArrays(1, &vao_);

#else

        glDeleteVertexArraysOES(1, &vao_);

#endif

    }

    if (use_texture_)
        gu::TextureLoader::deleteTexture(texture_);
}

ParticleSystem& ParticleSystem::operator=(const ParticleSystem &obj)
{
    gu::Drawable::operator=(obj);

    deconstruct();

    particles_ = obj.particles_;
    particle_data_ = obj.particle_data_;
    shader_ = obj.shader_;
    point_size_ = obj.point_size_;
    use_texture_ = obj.use_texture_;
    num_particles_ = obj.num_particles_;
    active_ = obj.active_;
    additive_blending_ = obj.additive_blending_;
    continuous_ = obj.continuous_;

    emitter_position_ = obj.emitter_position_;
    last_emitter_position_ = obj.last_emitter_position_;

    if (obj.initialized_)
    {
        if (obj.use_texture_)
        {
            loadTexture(obj.texture_);

            gu::TextureInfo *info = gu::TextureLoader::getTextureInfo(obj.texture_);
            info->count++;
        }

        create(particles_.size(), obj.particleSet_, obj.data_, obj.shader_);
    }
    else
    {
        initialized_ = false;
        data_ = NULL;
        loaded_vao_ = false;
    }

    return *this;
}

void ParticleSystem::draw(const float interpolation)
{
    if (!active_ || num_particles_ == 0)
        return;

    if (!additive_blending_)
    {
        if (use_texture_)
        {
            glEnable(GL_BLEND);
            glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
            glDepthMask(GL_FALSE);
        }
        else
            glDisable(GL_BLEND);
    }
    else
    {
        glEnable(GL_BLEND);
        glBlendFunc(GL_ONE, GL_ONE);
        glDepthMask(GL_FALSE);
        glDisable(GL_CULL_FACE);
    }
    
    shader_->bind();
    glm::vec3 interpolated_position = (interpolation * emitter_position_) + ((1.0f - interpolation) * last_emitter_position_);
    shader_->modify("pos", glm::translate(glm::mat4(1.0f), interpolated_position));
    shader_->modify("point_size", point_size_);

    if (loaded_vao_)
    {

#if !defined(TARGET_OS_IPHONE) && !defined(__ANDROID__)

        glBindVertexArray(vao_);

#else

        glBindVertexArrayOES(vao_);

#endif

    }
    else
    {
        glBindBuffer(GL_ARRAY_BUFFER, vbo_);

        if (!use_texture_)
        {
            glEnableVertexAttribArray(vPosition_);
            glVertexAttribPointer(vPosition_, 3, GL_FLOAT, GL_FALSE, 6*sizeof(GLfloat), 0);

            glEnableVertexAttribArray(vColour_);
            glVertexAttribPointer(vColour_, 3, GL_FLOAT, GL_FALSE, 6*sizeof(GLfloat), (GLvoid*)(3*sizeof(GLfloat)));
        }
        else
        {
            glEnableVertexAttribArray(vPosition_);
            glVertexAttribPointer(vPosition_, 3, GL_FLOAT, GL_FALSE, 3*sizeof(GLfloat), 0);

            glEnableVertexAttribArray(vRotation_);
            glVertexAttribPointer(vRotation_, 1, GL_FLOAT, GL_FALSE, 4*sizeof(GLfloat), (GLvoid*)(3*sizeof(GLfloat)));
        }
    }

    if (use_texture_)
    {
        glActiveTexture(GL_TEXTURE0);
        glBindTexture(GL_TEXTURE_2D, texture_);
    }
    
    glDrawArrays(GL_POINTS, 0, num_particles_);

    if (loaded_vao_)
    {

#if !defined(TARGET_OS_IPHONE) && !defined(__ANDROID__)

        glBindVertexArray(0);

#else

        glBindVertexArrayOES(0);

#endif

    }
    else
    {
        glDisableVertexAttribArray(vPosition_);

        if (!use_texture_)
            glDisableVertexAttribArray(vColour_);
        else
            glDisableVertexAttribArray(vRotation_);

        glBindBuffer(GL_ARRAY_BUFFER, 0);
    }

    if (additive_blending_ || use_texture_)
    {
        glDisable(GL_BLEND);
        glDepthMask(GL_TRUE);
        glEnable(GL_CULL_FACE);
    }
}

void ParticleSystem::update(const float dt)
{
    if (!active_ || num_particles_ == 0)
        return;

    int stride = updateParticles(dt);

    if (num_particles_ == 0)
        return;

    glBindBuffer(GL_ARRAY_BUFFER, vbo_);
    glBufferData(GL_ARRAY_BUFFER, num_particles_*stride*sizeof(GLfloat), &particle_data_[0], GL_DYNAMIC_DRAW);
    glBindBuffer(GL_ARRAY_BUFFER, 0);
}

int ParticleSystem::updateParticles(const float dt)
{
    unsigned int j = 0;

    if (use_texture_)
    {
        for (unsigned int i = 0; i < particles_.size(); i++)
        {
            particles_[i].life_tick += dt;

            if (continuous_ && particles_[i].life_tick >= particles_[i].life_time)
            {
                particleSet_(&particles_[i], i, data_);
                particles_[i].life_tick = 0.0f;
            }
            
            if (particles_[i].life_tick < particles_[i].life_time)
            {
                particles_[i].velocity += particles_[i].acceleration * dt;
                particles_[i].position += particles_[i].velocity * dt;
                particles_[i].rotation += particles_[i].rvelocity * dt;
            
                particle_data_[j] = particles_[i].position.x;
                particle_data_[j + 1] = particles_[i].position.y;
                particle_data_[j + 2] = particles_[i].position.z;
                particle_data_[j + 3] = particles_[i].rotation;
                j += 4;
            }
        }

        num_particles_ = j / 4;
        return 4;
    }
    else
    {
        for (unsigned int i = 0; i < particles_.size(); i++)
        {
            particles_[i].life_tick += dt;

            if (continuous_ && particles_[i].life_tick >= particles_[i].life_time)
            {
                particleSet_(&particles_[i], i, data_);
                particles_[i].life_tick = 0.0f;
            }
            
            if (particles_[i].life_tick < particles_[i].life_time)
            {
                particles_[i].velocity += particles_[i].acceleration * dt;
                particles_[i].position += particles_[i].velocity * dt;
            
                particle_data_[j] = particles_[i].position.x;
                particle_data_[j + 1] = particles_[i].position.y;
                particle_data_[j + 2] = particles_[i].position.z;
                particle_data_[j + 3] = particles_[i].colour.r;
                particle_data_[j + 4] = particles_[i].colour.g;
                particle_data_[j + 5] = particles_[i].colour.b;
                j += 6;
            }
        }

        num_particles_ = j / 6;
        return 6;
    }
}

void ParticleSystem::bindShader(gu::Shader *shader)
{
    shader_ = shader;
    vPosition_ = shader->getAttribLocation("vPosition");

    if (!use_texture_)
        vColour_ = shader->getAttribLocation("vColour");
    else
        vRotation_ = shader->getAttribLocation("vRotation");
}

void ParticleSystem::loadTexture(GLuint texture)
{
    texture_ = texture;
    use_texture_ = true;
}

void ParticleSystem::create(unsigned int particles, void (*particleSet)(gu::Particle *particle, int index, void *data), void *data, gu::Shader *shader)
{
    particleSet_ = particleSet;
    data_ = data;

    particles_.resize(particles);

    if (shader != NULL)
        bindShader(shader);
    
    // Create VAO and bind it

#if !defined(TARGET_OS_IPHONE) && !defined(__ANDROID__)

    if (gu::Shader::getOpenGLVersion() > 2)
    {
        glGenVertexArrays(1, &vao_);
        glBindVertexArray(vao_);
        loaded_vao_ = true;
    }

#else

    if (gu::Drawable::hasOES())
    {
        glGenVertexArraysOES(1, &vao_);
        glBindVertexArrayOES(vao_);
        loaded_vao_ = true;
    }

#endif
    
    // Initialize Data
    unsigned int j = 0;
    int stride;

    if (use_texture_)
    {
        particle_data_.resize(4*particles);

        for (unsigned int i = 0; i < particles_.size(); i++)
        {
            particle_data_[j] = particles_[i].position.x;
            particle_data_[j + 1] = particles_[i].position.y;
            particle_data_[j + 2] = particles_[i].position.z;
            particle_data_[j + 3] = particles_[i].rotation;
            j += 4;
        }

        stride = 4;
    }
    else
    {
        particle_data_.resize(6*particles);

        for (unsigned int i = 0; i < particles_.size(); i++)
        {
            particle_data_[j] = particles_[i].position.x;
            particle_data_[j + 1] = particles_[i].position.y;
            particle_data_[j + 2] = particles_[i].position.z;
            particle_data_[j + 3] = particles_[i].colour.r;;
            particle_data_[j + 4] = particles_[i].colour.g;
            particle_data_[j + 5] = particles_[i].colour.b;
            
            j += 6;
        }

        stride = 6;
    }
    
    num_particles_ = particles_.size();
    
    // Create VBO
    glGenBuffers(1, &vbo_);
    glBindBuffer(GL_ARRAY_BUFFER, vbo_);
    
    glBufferData(GL_ARRAY_BUFFER, num_particles_*stride*sizeof(GLfloat), &particle_data_[0], GL_DYNAMIC_DRAW);
    
    glEnableVertexAttribArray(vPosition_);
    glVertexAttribPointer(vPosition_, 3, GL_FLOAT, GL_FALSE, stride*sizeof(GLfloat), 0);
    
    if (!use_texture_)
    {
        glEnableVertexAttribArray(vColour_);
        glVertexAttribPointer(vColour_, 3, GL_FLOAT, GL_FALSE, 6*sizeof(GLfloat), (GLvoid*)(3*sizeof(GLfloat)));
    }
    else
    {
        glEnableVertexAttribArray(vRotation_);
        glVertexAttribPointer(vRotation_, 1, GL_FLOAT, GL_FALSE, 4*sizeof(GLfloat), (GLvoid*)(3*sizeof(GLfloat)));
    }
    
    if (loaded_vao_)
    {

#if !defined(TARGET_OS_IPHONE) && !defined(__ANDROID__)

        glBindVertexArray(0);

#else

        glBindVertexArrayOES(0);

#endif

    }
    
    glBindBuffer(GL_ARRAY_BUFFER, 0);
    initialized_ = true;
}

void ParticleSystem::play()
{
    if (!initialized_)
        return;

    for (unsigned int i = 0; i < particles_.size(); i++)
        particleSet_(&particles_[i], i, data_);

    // Initialize Data
    unsigned int j = 0;
    int stride;

    if (use_texture_)
    {
        for (unsigned int i = 0; i < particles_.size(); i++)
        {
            particles_[i].life_tick = continuous_ ? gu::random(0.0f, particles_[i].life_time) : 0.0f;

            particle_data_[j] = particles_[i].position.x;
            particle_data_[j + 1] = particles_[i].position.y;
            particle_data_[j + 2] = particles_[i].position.z;
            particle_data_[j + 3] = particles_[i].rotation;
            
            j += 4;
        }

        stride = 4;
    }
    else
    {
        for (unsigned int i = 0; i < particles_.size(); i++)
        {
            particles_[i].life_tick = continuous_ ? gu::random(0.0f, particles_[i].life_time) : 0.0f;

            particle_data_[j] = particles_[i].position.x;
            particle_data_[j + 1] = particles_[i].position.y;
            particle_data_[j + 2] = particles_[i].position.z;
            particle_data_[j + 3] = particles_[i].colour.r;
            particle_data_[j + 4] = particles_[i].colour.g;
            particle_data_[j + 5] = particles_[i].colour.b;
            
            j += 6;
        }

        stride = 6;
    }
    
    num_particles_ = particles_.size();

    if (loaded_vao_)
    {

#if !defined(TARGET_OS_IPHONE) && !defined(__ANDROID__)

        glBindVertexArray(vao_);

#else

        glBindVertexArrayOES(vao_);

#endif

    }

    glBindBuffer(GL_ARRAY_BUFFER, vbo_);
    glBufferData(GL_ARRAY_BUFFER, num_particles_*stride*sizeof(GLfloat), &particle_data_[0], GL_DYNAMIC_DRAW);

    active_ = true;
}

void ParticleSystem::updateLastPosition()
{
    last_emitter_position_ = emitter_position_;
}

void ParticleSystem::unload()
{
    deconstruct();

    particles_.clear();
    particle_data_.clear();
    shader_ = NULL;
    initialized_ = false;
    loaded_vao_ = false;
    vao_ = 0;
    vbo_ = 0;
    vPosition_ = 0;
    vColour_ = 0;
    vRotation_ = 0;
    point_size_ = 1.0f;
    use_texture_ = false;
    num_particles_ = 0;
    texture_ = 0;

    active_ = false;
    additive_blending_ = false;
    continuous_ = false;
    
    emitter_position_ = glm::vec3(0.0f);
    last_emitter_position_ = glm::vec3(0.0f);
    
    particleSet_ = NULL;
    data_ = NULL;
}

// Setters

void ParticleSystem::setPosition(const glm::vec3 &position, bool snap)
{
    emitter_position_ = position;

    if (snap)
        last_emitter_position_ = position;
}
    
void ParticleSystem::setPointSize(float s)
{
    point_size_ = s;
}

void ParticleSystem::setAdditiveBlending(bool b)
{
    additive_blending_ = b;
}

void ParticleSystem::setContinuous(bool b)
{
    continuous_ = b;
}

void ParticleSystem::setDataPointer(void *data)
{
    data_ = data;
}

// Getters

gu::Shader* ParticleSystem::getShader()
{
    return shader_;
}

const glm::vec3& ParticleSystem::getPosition() const
{
    return emitter_position_;
}
    
float ParticleSystem::getPointSize() const
{
    return point_size_;
}

bool ParticleSystem::getAdditiveBlending() const
{
    return additive_blending_;
}

bool ParticleSystem::getContinuous() const
{
    return continuous_;
}

void* ParticleSystem::getDataPointer()
{
    return data_;
}

GLuint ParticleSystem::getActiveParticles() const
{
    return num_particles_;
}

}
