#include "GUMI/Sound.h"

namespace gu
{

Sound::Sound() :
    chunk_(NULL),
    zip_rw_(NULL),
    zip_used_(false),
    channel_(-1),
    volume_(MIX_MAX_VOLUME)
{
}

Sound::~Sound()
{
    deconstruct();
}

void Sound::deconstruct()
{
    stop();
    Mix_FreeChunk(chunk_);
    if(zip_used_)
    {
        free(zip_sound_buffer_);
        SDL_RWclose(zip_rw_);
        zip_used_ = false;
    }
}

void Sound::loadZipWAV(const std::string &file_path)
{
    deconstruct();

    gu::Zip zip(file_path);
    zip_sound_buffer_ = zip.getBuffer();

    zip_rw_ = SDL_RWFromMem(zip.getBuffer(),zip.getSize());
    chunk_ = Mix_LoadWAV_RW(zip_rw_,0);
    
    if (chunk_ == NULL)
        SDL_Log("[GUMI] (Sound) Error: Could not load \"%s\"", file_path.c_str());

    zip_used_ = true;
}

void Sound::loadFileWAV(const std::string &file_path)
{
    if (gu::Zip::getRedirectFileLoading())
    {
        loadZipWAV(file_path);
        return;
    }

    deconstruct();
    
    chunk_ = Mix_LoadWAV(file_path.c_str());
    
    if (chunk_ == NULL)
        SDL_Log("[GUMI] (Sound) Error: Could not load \"%s\"", file_path.c_str());
    
    zip_used_ = false;
}

void Sound::play()
{
    channel_ = Mix_PlayChannel(-1, chunk_, 0);
    
    if (channel_ == -1)
        SDL_Log("[GUMI] (Sound) Error: Could not play sound: \"%s\"", Mix_GetError());
    else
        Mix_Volume(channel_,volume_);
}

void Sound::stop()
{
    if (channel_ != -1)
        Mix_HaltChannel(channel_);
}

// Setters
    
void Sound::setVolume(int volume)
{
    if (volume > MIX_MAX_VOLUME) volume = MIX_MAX_VOLUME;
    else if (volume < 0) volume = 0;
    
    volume_ = volume;
}

// Getters

int Sound::getVolume() const
{
    return volume_;
}

}
