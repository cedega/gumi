#include "GUMI/Button.h"

namespace gu
{

Button::Button() :
    held_(-1),
    use_circle_(true),
    active_radius_(0.0f),
    activate_on_release_(false),
    activate_on_contact_(false),
    has_pressed_texture_(false),
    force_pressed_texture_(-1),
    ignore_distance_(-1.0f)
{

#if !defined(TARGET_OS_IPHONE) && !defined(__ANDROID__)

    last_mouse_down_ = false;

#endif

}

Button::Button(const Button &obj) : 
    gu::Image(obj),
    held_(-1),
    start_held_(obj.start_held_),
    use_circle_(obj.use_circle_),
    active_radius_(obj.active_radius_),
    active_area_(obj.active_area_),
    activate_on_release_(obj.activate_on_release_),
    activate_on_contact_(obj.activate_on_contact_),
    force_pressed_texture_(obj.force_pressed_texture_),
    ignore_distance_(obj.ignore_distance_)
{
    if (obj.has_pressed_texture_)
    {
        loadPressedTexture(obj.pressed_texture_);
    }

#if !defined(TARGET_OS_IPHONE) && !defined(__ANDROID__)

    last_mouse_down_ = false;
    start_press_ = obj.start_press_;

#endif

}

Button::~Button()
{
    if (has_pressed_texture_)
    {
        gu::TextureLoader::deleteTexture(texture_);
        gu::TextureLoader::deleteTexture(pressed_texture_);
    }
}

Button& Button::operator=(const Button &obj)
{
    gu::Image::operator=(obj);

    if (has_pressed_texture_)
    {
        gu::TextureLoader::deleteTexture(texture_);
        gu::TextureLoader::deleteTexture(pressed_texture_);
    }

    held_ = -1;

    use_circle_ = obj.use_circle_;
    active_radius_ = obj.active_radius_;
    active_area_ = obj.active_area_;

    activate_on_release_ = obj.activate_on_release_;
    activate_on_contact_ = obj.activate_on_contact_;
    force_pressed_texture_ = obj.force_pressed_texture_;

    if (obj.has_pressed_texture_)
    {
        loadPressedTexture(obj.pressed_texture_);
    }

#if !defined(TARGET_OS_IPHONE) && !defined(__ANDROID__)

    last_mouse_down_ = false;

#endif

    return *this;
}

void Button::loadPressedTexture(GLuint texture_id)
{
    has_pressed_texture_ = true;

    gu::TextureInfo *ti = gu::TextureLoader::getTextureInfo(gu::Image::getTexture());
    ti->count++;
    texture_ = ti->id;
    pressed_texture_ = texture_id;

    // Will delete the currently-bound texture, but we've incremented
    // its reference count and stored it.
    glm::vec4 texture_rect = gu::Image::getTextureRect();
    gu::Image::setTexture(texture_);
    gu::Image::setTextureRect(texture_rect);
}

bool Button::poll()
{
    bool trigger = false;

#if !defined(TARGET_OS_IPHONE) && !defined(__ANDROID__)

    if (gu::Input::getLeftMouseDown() && (activate_on_contact_ || !last_mouse_down_) && held_ == -1)
    {
        if (processPress(gu::Input::getMousePosition()))
        {
            held_ = 1;
            start_held_ = gu::Input::getMousePosition();
        }
    }
    else if (!gu::Input::getLeftMouseDown() && held_ == 1)
    {
        trigger = processRelease(gu::Input::getMousePosition());
        held_ = -1;
        start_held_ = glm::vec2(0.0f);
    }
    else if (!activate_on_release_ && held_ != -1)
    {
        trigger = true;
    }

    last_mouse_down_ = gu::Input::getLeftMouseDown();

#else

    std::vector<gu::Finger> *fingers = gu::Input::getTouchFingers();
    
    for (unsigned int i = 0; i < fingers->size(); i++)
    {
        if (fingers->at(i).type == SDL_FINGERDOWN || (activate_on_contact_ && held_ == -1 && fingers->at(i).type == SDL_FINGERMOTION))
        {
            if (processPress(glm::vec2(fingers->at(i).x, fingers->at(i).y)))
            {
                held_ = fingers->at(i).index;
                start_held_ = glm::vec2(fingers->at(i).x, fingers->at(i).y);
            }
        }
        else if (fingers->at(i).type == SDL_FINGERUP && fingers->at(i).index == held_)
        {
            trigger = processRelease(glm::vec2(fingers->at(i).x, fingers->at(i).y));
            held_ = -1;
            start_held_ = glm::vec2(0.0f);
        }
    }

    if (!activate_on_release_ && held_ != -1)
    {
        trigger = true;
    }

#endif

    if (has_pressed_texture_ && force_pressed_texture_ < 0)
    {
        if (held_ != -1)
        {
            if (gu::Image::getTexture() != pressed_texture_)
            {
                glm::vec4 texture_rect = gu::Image::getTextureRect();
                gu::Image::setTexture(pressed_texture_);
                gu::Image::setTextureRect(texture_rect);
            }
        }
        else
        {
            if (gu::Image::getTexture() != texture_)
            {
                glm::vec4 texture_rect = gu::Image::getTextureRect();
                gu::Image::setTexture(texture_);
                gu::Image::setTextureRect(texture_rect);
            }
        }
    }

    return trigger;
}

bool Button::processPress(const glm::vec2 &press_position)
{
    if (use_circle_)
    {
        return glm::length(press_position - getPosition(false)) <= active_radius_;
    }
    else
    {
        return press_position.x >= active_area_.x &&
               press_position.x <= active_area_.x + active_area_.z &&
               press_position.y >= active_area_.y &&
               press_position.y <= active_area_.y + active_area_.w;
    }
}

bool Button::processRelease(const glm::vec2 &release_position)
{
    // Process the only-on-release trigger
    if (activate_on_release_)
    {
        if (use_circle_)
        {
            return processPress(release_position) && (ignore_distance_ < 0.0f || glm::length(release_position - start_held_) < ignore_distance_);
        }
        else
        {
            return processPress(release_position) && (ignore_distance_ < 0.0f || glm::length(release_position - start_held_) < ignore_distance_);
        }
    }

    return false;
}

// Setters

void Button::setActiveRadius(float r)
{
    active_radius_ = r;
    use_circle_ = true;
}

void Button::setActiveArea(const glm::vec4 &rect)
{
    active_area_ = rect;
    use_circle_ = false;
}

void Button::setActivateOnRelease(bool b)
{
    activate_on_release_ = b;
}

void Button::setForcePressedTexture(int i)
{
    force_pressed_texture_ = i;

    if (has_pressed_texture_ && force_pressed_texture_ >= 0)
    {
        if (force_pressed_texture_ >= 1)
        {
            if (gu::Image::getTexture() != pressed_texture_)
            {
                glm::vec4 texture_rect = gu::Image::getTextureRect();
                gu::Image::setTexture(pressed_texture_);
                gu::Image::setTextureRect(texture_rect);
            }
        }
        else
        {
            if (gu::Image::getTexture() != texture_)
            {
                glm::vec4 texture_rect = gu::Image::getTextureRect();
                gu::Image::setTexture(texture_);
                gu::Image::setTextureRect(texture_rect);
            }
        }
    }
}

void Button::setIgnoreDistance(float d)
{
    ignore_distance_ = d;
}

void Button::setActivateOnContact(bool b)
{
    activate_on_contact_ = b;
}

// Getters

float Button::getActiveRadius() const
{
    return active_radius_;
}

const glm::vec4& Button::getActiveArea() const
{
    return active_area_;
}

bool Button::getActivateOnRelease() const
{
    return activate_on_release_;
}

int Button::getForcePressedTexture() const
{
    return force_pressed_texture_;
}

float Button::getIgnoreDistance() const
{
    return ignore_distance_;
}

bool Button::getActivateOnContact() const
{
    return activate_on_contact_;
}

}
