#include "GUMI/World.h"

namespace gu
{

World::World()
{

}

World::~World()
{
    for (unsigned int i = 0; i < models_.size(); i++)
        delete models_[i];
}

void World::draw(const gu::Camera &camera)
{
    for (unsigned int i = 0; i < models_.size(); i++)
    {
        if (camera.visible(models_[i]->getModelAABB()))
            models_[i]->draw();
    }
}

void World::loadFileWorld(const std::string &file_path, gu::Shader *shader)
{

#if defined(TARGET_OS_IPHONE) || defined(__ANDROID__) 

    SDL_RWops *rw = SDL_RWFromFile(file_path.c_str(), "r");
    Sint64 size = rw->size(rw);
    char *buffer = new char[size];
    rw->read(rw, buffer, size, 1);
    std::string contents(buffer, size);
    delete[] buffer;
    SDL_FreeRW(rw);

    // Remove potential Windows file endings
    contents.erase(std::remove(contents.begin(), contents.end(), '\r'), contents.end()); 

    processWorld(contents, file_path.substr(0, file_path.find_last_of("/") + 1), shader, false);

#else

    std::ifstream file(file_path.c_str());

    std::string line;
    std::string contents;

    while (std::getline(file, line))
    {
        contents += line + "\n";
    }

    file.close();
    
    // Remove potential Windows file endings
    contents.erase(std::remove(contents.begin(), contents.end(), '\r'), contents.end()); 
    
    processWorld(contents, file_path.substr(0, file_path.find_last_of("/") + 1), shader, false);

#endif

}

void World::loadZipWorld(const std::string &file_path, gu::Shader *shader)
{

#if defined(TARGET_OS_IPHONE) || defined(__ANDROID__) 

    SDL_RWops *rw = SDL_RWFromFile(file_path.c_str(), "r");
    Sint64 size = rw->size(rw);
    char *buffer = new char[size];
    rw->read(rw, buffer, size, 1);
    std::string contents(buffer, size);
    delete[] buffer;
    SDL_FreeRW(rw);

    // Remove potential Windows file endings
    contents.erase(std::remove(contents.begin(), contents.end(), '\r'), contents.end()); 

    processWorld(contents, file_path.substr(0, file_path.find_last_of("/") + 1), shader, true);

#else

    gu::Zip zip(file_path);
    std::string contents(zip.getBuffer(), zip.getSize());
    free(zip.getBuffer());

    // Remove potential Windows file endings
    contents.erase(std::remove(contents.begin(), contents.end(), '\r'), contents.end()); 

    processWorld(contents, file_path.substr(0, file_path.find_last_of("/") + 1), shader, true);

#endif

}

void World::processWorld(const std::string &contents, const std::string &path, gu::Shader *shader, bool from_zip)
{
    size_t start = contents.find('\n') + 1;
    std::string line;
    
    int size = atoi(contents.substr(0, start - 1).c_str());
    models_.resize(size);
    int index = 0;
    
    while (true)
    {
        size_t endline = contents.find('\n', start);
        line = contents.substr(start, endline - start);
        
        if (line.compare("END") == 0)
            break;
        else
        {
            gu::Model *model = new gu::Model();

            // OBJ has Y-up by default
            model->setZUp(false);
            model->setGenerateAABB(true);

            if (from_zip)
                model->loadZipModel(path + line, shader);
            else
                model->loadFileModel(path + line, shader);

            models_[index] = model;
            index++;
        }

        start = endline + 1;
    }
}

// Getters

std::vector<gu::Model*>* World::getModels()
{
    return &models_;
}

}
