#include "GUMI/Camera.h"

namespace gu
{

Camera::Camera() :
    fps_camera_(false),
    window_(NULL),
    screen_x_(0),
    screen_y_(0),
    horizontal_angle_(PI),
    vertical_angle_(0.0f),
    lerp_speed_(8.0f)
{
}

void Camera::initFPSCamera(SDL_Window *window, int screen_x, int screen_y)
{
    fps_camera_ = true;
    window_ = window;
    screen_x_ = screen_x;
    screen_y_ = screen_y;

    SDL_WarpMouseInWindow(window_, screen_x_/2, screen_y_/2);
    SDL_ShowCursor(0);
}

void Camera::moveTo(const glm::vec3 &target)
{
    if (fps_camera_)
        return;

    glm::vec3 position_diff = target - position_ + position_offset_;
    glm::vec3 look_at_diff = target - look_at_ + look_at_offset_;

    // Update Last Data
    last_position_ = position_;
    last_look_at_ = look_at_;

    position_ += position_diff / lerp_speed_;
    look_at_ += look_at_diff / lerp_speed_;
}

void Camera::snapTo(const glm::vec3 &target)
{
    setPosition(target + position_offset_, true);
}

void Camera::applyPerspective(gu::Shader *shader)
{
    shader->modify("proj", perspective_);
}

void Camera::applyView(gu::Shader *shader)
{
    shader->modify("view", view_);
}

void Camera::updateView(const float interpolation)
{
    glm::vec3 interpolated_position = (interpolation * position_) + ((1.0f - interpolation) * last_position_);
    glm::vec3 interpolated_look_at = (interpolation * look_at_) + ((1.0f - interpolation) * last_look_at_);
    
    view_ = glm::lookAt(interpolated_position, interpolated_look_at, glm::vec3(0.0f, 1.0f, 0.0f));
    fcull_.set(interpolated_position, interpolated_look_at, glm::vec3(0.0f, 1.0f, 0.0f));

    // Update Default Shaders
    applyView(gu::Shader::SHADER_3D);
    applyView(gu::Shader::SHADER_3D_RIG);
    applyView(gu::Shader::SHADER_2D);
    applyView(gu::Shader::SHADER_2D_BILLBOARD);
    applyView(gu::Shader::SHADER_PS);
    applyView(gu::Shader::SHADER_PS_TEXTURE);
    gu::DrawRect::setView(view_);
}

void Camera::updateFPSView(bool up, bool down, bool left, bool right, float speed, float sensitivity, const float dt)
{
    if (!fps_camera_)
        return;

    // Update last for interpolation
    last_position_ = position_;
    last_look_at_ = look_at_;

    int mouse_x;
    int mouse_y;
    SDL_GetMouseState(&mouse_x, &mouse_y);
    SDL_WarpMouseInWindow(window_, screen_x_/2, screen_y_/2);

    int difference_x = (screen_x_/2) - mouse_x;
    int difference_y = (screen_y_/2) - mouse_y;

    horizontal_angle_ += difference_x * sensitivity * dt;
    vertical_angle_ += difference_y * sensitivity * dt;

    glm::vec3 direction(
        std::cos(vertical_angle_) * std::sin(horizontal_angle_),
        std::sin(vertical_angle_),
        std::cos(vertical_angle_) * std::cos(horizontal_angle_)
    );

    glm::vec3 R(
        std::sin(horizontal_angle_ - PI/2.0f),
        0,
        std::cos(horizontal_angle_ - PI/2.0f)
    );

    glm::vec3 U = glm::cross(R, direction);

    if (left)
    {
        position_ -= R * speed * dt;
    }
    if (right)
    {
        position_ += R * speed * dt;
    }
    if (up)
    {
        position_ += direction * speed * dt;
    }
    if (down)
    {
        position_ -= direction * speed * dt;
    }

    look_at_ = position_ + direction;
    view_ = glm::lookAt(position_, look_at_, U);
    fcull_.set(position_, look_at_, U);
}

bool Camera::visible(const gu::AABB &aabb) const
{
    return fcull_.visible(aabb);
}

// Setters

void Camera::setPerspective(float angle, float ratio, float near, float far)
{
    perspective_ = glm::perspective(glm::radians(angle), ratio, near, far);
    fcull_.init(angle, ratio, near, far);

    // Update Default Shaders
    applyPerspective(gu::Shader::SHADER_3D);
    applyPerspective(gu::Shader::SHADER_3D_RIG);
    applyPerspective(gu::Shader::SHADER_2D);
    applyPerspective(gu::Shader::SHADER_2D_BILLBOARD);
    applyPerspective(gu::Shader::SHADER_PS);
    applyPerspective(gu::Shader::SHADER_PS_TEXTURE);
    gu::DrawRect::setPerspective(perspective_);
}

void Camera::setView(const glm::mat4 &view)
{
    view_ = view;

    // Update Default Shaders
    applyView(gu::Shader::SHADER_3D);
    applyView(gu::Shader::SHADER_3D_RIG);
    applyView(gu::Shader::SHADER_2D);
    applyView(gu::Shader::SHADER_2D_BILLBOARD);
    applyView(gu::Shader::SHADER_PS);
    applyView(gu::Shader::SHADER_PS_TEXTURE);
    gu::DrawRect::setView(view_);
}

void Camera::setPosition(const glm::vec3 &position, bool hard_set)
{
    position_ = position;

    if (hard_set)
        last_position_ = position_;
}

void Camera::setLookAt(const glm::vec3 &look_at, bool hard_set)
{
    look_at_ = look_at;

    if (hard_set)
        last_look_at_ = look_at_;
}

void Camera::setPositionOffset(const glm::vec3 &offset)
{
    position_offset_ = offset;
}

void Camera::setLookAtOffset(const glm::vec3 &offset)
{
    look_at_offset_ = offset;
}

void Camera::setLerpSpeed(float lerp_speed)
{
    lerp_speed_ = lerp_speed;
}

// Getters

const glm::mat4& Camera::getPerspective() const
{
    return perspective_;
}

const glm::mat4& Camera::getView() const
{
    return view_;
}

const glm::vec3& Camera::getPosition() const
{
    return position_;
}

const glm::vec3& Camera::getLookAt() const
{
    return look_at_;
}

const glm::vec3& Camera::getPositionOffset() const
{
    return position_offset_;
}

const glm::vec3& Camera::getLookAtOffset() const
{
    return look_at_offset_;
}

float Camera::getLerpSpeed() const
{
    return lerp_speed_;
}

}
