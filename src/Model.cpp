#include "GUMI/Model.h"

namespace gu
{

bool Model::z_up_ = true;

Model::Model() : 
    shader_(NULL),
    local_z_up_(-1),
    armature_(NULL),
    model_loaded_(false),
    has_textures_(false),
    loaded_vao_(false),
    backface_culling_(true),
    scaling_(1.0f),
    colour_(1.0f),
    generate_aabb_(false)
{
}

Model::Model(const Model &obj) :
    gu::Drawable(obj),
    shader_(obj.shader_),
    local_z_up_(obj.local_z_up_),
    armature_(obj.armature_),
    file_name_(obj.file_name_),
    model_loaded_(false),
    has_textures_(false),
    loaded_vao_(false),
    backface_culling_(obj.backface_culling_),
    position_(obj.position_),
    last_position_(obj.last_position_),
    scaling_(obj.scaling_),
    rotation_(obj.rotation_),
    colour_(obj.colour_),
    generate_aabb_(obj.generate_aabb_),
    aabb_(obj.aabb_)
{
}

Model::~Model()
{
    deconstruct();
}

Model& Model::operator=(const Model &obj)
{
    gu::Drawable::operator=(obj);

    shader_ = obj.shader_;
    armature_ = obj.armature_;
    local_z_up_ = obj.local_z_up_;
    file_name_ = obj.file_name_;
    model_loaded_ = false;
    has_textures_ = false;
    loaded_vao_ = false;
    backface_culling_ = obj.backface_culling_;
    position_ = obj.position_;
    last_position_ = obj.last_position_;
    scaling_ = obj.scaling_;
    rotation_ = obj.rotation_;
    colour_ = obj.colour_;

    generate_aabb_ = obj.generate_aabb_;
    aabb_ = obj.aabb_;

    return *this;
}

void Model::init()
{
    shader_ = NULL;
    armature_ = NULL;
    model_loaded_ = false;
    has_textures_ = false;
    loaded_vao_ = false;
    backface_culling_ = true;

    position_ = glm::vec3(0.0f);
    last_position_ = position_;
    scaling_ = glm::vec3(1.0f);
    rotation_ = glm::vec3(0.0f);
    colour_ = glm::vec4(1.0f);

    generate_aabb_ = false;
    local_z_up_ = -1;
}

void Model::deconstruct()
{
    if (!vbo_.empty())
        glDeleteBuffers(vbo_.size(), &vbo_[0]);
    if (!ebo_.empty())
        glDeleteBuffers(ebo_.size(), &ebo_[0]);

    if (loaded_vao_)
    {

#if !defined(TARGET_OS_IPHONE) && !defined(__ANDROID__)

        glDeleteVertexArrays(vao_.size(), &vao_[0]);

#else

        glDeleteVertexArraysOES(vao_.size(), &vao_[0]);

#endif

    }
}

void Model::unload()
{
    deconstruct();

    vbo_.clear();
    ebo_.clear();
    ind_size_.clear();
    texture_names_.clear();
    texture_numbers_.clear();
    texcoord_locations_.clear();
    bone_id_locations_.clear();
    bone_weight_locations_.clear();
    textures_.clear();

    init();
}
    
void Model::updateAnimations(const float frame_time)
{
    if (armature_ != NULL)
        armature_->update(frame_time);

    // Only update textures which have strictly more than 1 texture for an animation
    for (unsigned int i = 0; i < textures_.size(); i++)
        if (textures_[i].getCurrentAnimatedTexture()->getTextures()->size() > 1)
            textures_[i].getCurrentAnimatedTexture()->update(frame_time);
}

void Model::draw(const float interpolation)
{    
    if (!model_loaded_)
        return;

    shader_->bind();

    if (backface_culling_) glEnable(GL_CULL_FACE);
    else                   glDisable(GL_CULL_FACE);
    
    if (colour_.a >= 1.0f) 
    {
        glDisable(GL_BLEND);
    }
    else
    {
        glEnable(GL_BLEND);
        glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
    }

    if (armature_ != NULL)
        shader_->modify("bones", armature_->getTransforms());

    // Apply linear interpolation
    glm::vec3 interpolated_position = getInterpolatedPosition(interpolation);

    shader_->modify("pos", glm::translate(glm::mat4(1.0f), interpolated_position)); 
    shader_->modify("scale", glm::scale(glm::mat4(1.0f), scaling_));
    
    // Account for Z-up models (like the ones from Blender)
    // Models with a DAE animation and armature have the correct orientation
    float x_rotation = rotation_.x;
    float y_rotation = rotation_.y;
    float z_rotation = rotation_.z;

    if ((local_z_up_ == -1 && z_up_) || local_z_up_ == 1)
    {
        if (armature_ == NULL)
        {
            // Models which are NULL need to have z and y rotations swapped
            x_rotation -= 90.0f;
            z_rotation = rotation_.y;
            y_rotation = rotation_.z;
        }
        // else if (getLoadedAnimation()->getFileExtension().compare(".fbx") == 0)
        //    x_rotation -= 90.0f;
    }

    glm::mat4 rotate_x = glm::rotate(glm::mat4(1.0f), glm::radians(x_rotation), glm::vec3(1.0f,0.0f,0.0f));
    glm::mat4 rotate_y = glm::rotate(glm::mat4(1.0f), glm::radians(y_rotation), glm::vec3(0.0f,1.0f,0.0f));
    glm::mat4 rotate_z = glm::rotate(glm::mat4(1.0f), glm::radians(z_rotation), glm::vec3(0.0f,0.0f,1.0f));

    shader_->modify("rotate", rotate_x * rotate_y * rotate_z);
    shader_->modify("colour", colour_);

    for (unsigned int i = 0; i < vbo_.size(); i++)
    {
        if (loaded_vao_)
        {

#if !defined(TARGET_OS_IPHONE) && !defined(__ANDROID__)

            glBindVertexArray(vao_[i]);

#else

            glBindVertexArrayOES(vao_[i]);

#endif

        }
        else
        {
            glBindBuffer(GL_ARRAY_BUFFER, vbo_[i]);

            glEnableVertexAttribArray(vPosition_);
            glEnableVertexAttribArray(vTexCoord_);

            glVertexAttribPointer(vPosition_, 3, GL_FLOAT, GL_FALSE, 0, 0);
            glVertexAttribPointer(vTexCoord_, 2, GL_FLOAT, GL_FALSE, 0, reinterpret_cast<void*>(texcoord_locations_[i]));

            if (armature_ != NULL)
            {
                glEnableVertexAttribArray(boneids_);
                glEnableVertexAttribArray(weights_);

#if !defined(TARGET_OS_IPHONE) && !defined(__ANDROID__)

                glVertexAttribIPointer(boneids_, 4, GL_INT, 0, reinterpret_cast<void*>(bone_id_locations_[i]));

#else // APPLE OR ANDROID

                glVertexAttribPointer(boneids_, 4, GL_FLOAT, GL_FALSE, 0, reinterpret_cast<void*>(bone_id_locations_[i]));

#endif

                glVertexAttribPointer(weights_, 4, GL_FLOAT, GL_FALSE, 0, reinterpret_cast<void*>(bone_weight_locations_[i]));
            }

            glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, ebo_[i]);
        }

        // -------------------
        // Textures
        // -------------------
        
        if (!textures_.empty())
        {
            glActiveTexture(GL_TEXTURE0);
            glBindTexture(GL_TEXTURE_2D, textures_[texture_numbers_[i]].getCurrentAnimatedTexture()->getCurrentTexture());
        }

        glDrawElements(GL_TRIANGLES, ind_size_[i], GL_UNSIGNED_SHORT, 0);
    }

    if (loaded_vao_)
    {

#if !defined(TARGET_OS_IPHONE) && !defined(__ANDROID__)

        glBindVertexArray(0);

#else

        glBindVertexArrayOES(0);

#endif

    }
    else
    {
        glDisableVertexAttribArray(vPosition_);
        glDisableVertexAttribArray(vTexCoord_);

        if (armature_ != NULL)
        {
            glDisableVertexAttribArray(boneids_);
            glDisableVertexAttribArray(weights_);
        }

        glBindBuffer(GL_ARRAY_BUFFER, 0);
        glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
    }

    gu::printOpenGLErrors("Model: ");
}

void Model::bindShader(gu::Shader *shaders)
{
    shader_ = shaders;
    vPosition_ = shaders->getAttribLocation("vPosition");
    vTexCoord_ = shaders->getAttribLocation("vTexCoord");
    textureSample_ = shaders->getUniformLocation("textureSample");
    boneids_ = shaders->getAttribLocation("boneids");
    weights_ = shaders->getAttribLocation("weights");
    glUniform1i(textureSample_, 0);
}

void Model::bindArmature(gu::Armature *armature)
{
    if (armature_ == NULL && model_loaded_)
        SDL_Log("[GUMI] (Model) Error: Cannot bind armature to a model which has already been loaded");
    else
        armature_ = armature;
}

void Model::loadTexture(GLuint texture)
{
    textures_.push_back(gu::AnimatedTextureList(gu::AnimatedTexture(texture, 0.0f)));
    has_textures_ = true;
}

bool Model::loadFileModel(const std::string &file_path, gu::Shader *shader, bool auto_load_textures)
{

#if !defined(TARGET_OS_IPHONE) && !defined(__ANDROID__)

    if (gu::Zip::getRedirectFileLoading())
        return loadZipModel(file_path, shader, auto_load_textures);

#endif

    Assimp::Importer importer;
    const aiScene *scene = NULL;

#if defined(__ANDROID__)

    importer.SetIOHandler(new gu::IOSystem());

#endif
    
    // Assimp::DefaultLogger::create("", Assimp::Logger::VERBOSE, aiDefaultLogStream_STDOUT);

    scene = importer.ReadFile(file_path.c_str(), aiProcess_Triangulate
            | aiProcess_JoinIdenticalVertices  
            | aiProcess_ImproveCacheLocality
            | aiProcess_SplitLargeMeshes 
            | aiProcess_FindDegenerates 
            | aiProcess_FlipUVs 
            | aiProcess_LimitBoneWeights 
            | aiProcess_RemoveRedundantMaterials  
            | aiProcess_SortByPType  
            | aiProcess_FindInvalidData
            | aiProcess_FindInstances 
            | aiProcess_OptimizeMeshes  
            | aiProcess_OptimizeGraph);

    // Assimp::DefaultLogger::kill();

    if (scene == NULL)
    {
        SDL_Log("[GUMI] (Model) Error: Could not load object: \"%s\"", file_path.c_str());
        return false;
    }

    if (shader != NULL)
        bindShader(shader);
    else if (shader_ == NULL)
    {
        SDL_Log("[GUMI] (Model) Error: Could not load object (shader not bound): \"%s\"", file_path.c_str());
        return false;
    }

    loadObject(file_path, scene);

    if (auto_load_textures)
    {
        std::string path = file_path.substr(0, file_path.find_last_of("/") + 1);
        autoLoadTextures(false, path);
    }

    return true;
}

bool Model::loadZipModel(const std::string &zip_path, gu::Shader *shader, bool auto_load_textures)
{

#if defined(TARGET_OS_IPHONE) || defined(__ANDROID__)

    return loadFileModel(zip_path, shader, auto_load_textures);

#else

    std::string file_extension = zip_path.substr(zip_path.find_last_of("."));

    Assimp::Importer importer;
    const aiScene *scene = NULL;

    gu::Zip zip(zip_path);
    scene = importer.ReadFileFromMemory(zip.getBuffer(), zip.getSize(), aiProcess_Triangulate
            | aiProcess_JoinIdenticalVertices  
            | aiProcess_ImproveCacheLocality
            | aiProcess_SplitLargeMeshes 
            | aiProcess_FindDegenerates 
            | aiProcess_FlipUVs 
            | aiProcess_LimitBoneWeights 
            | aiProcess_RemoveRedundantMaterials  
            | aiProcess_SortByPType  
            | aiProcess_FindInvalidData
            | aiProcess_FindInstances 
            | aiProcess_OptimizeMeshes  
            | aiProcess_OptimizeGraph, file_extension.c_str());

    free(zip.getBuffer());

    if (scene == NULL)
    {
        SDL_Log("[GUMI] (Model) Error: Could not load object: \"%s\"", zip_path.c_str());
        return false;
    }

    if (shader != NULL)
        bindShader(shader);
    else if (shader_ == NULL)
    {
        SDL_Log("[GUMI] (Model) Error: Could not load object (shader not bound): \"%s\"", zip_path.c_str());
        return false;
    }

    loadObject(zip_path, scene);

    if (auto_load_textures)
    {
        std::string path = zip_path.substr(0, zip_path.find_last_of("/") + 1);
        autoLoadTextures(true, path);
    }

    return true;

#endif

}

bool Model::loadObject(const std::string &object_path, const aiScene *scene)
{
    unsigned int i1 = object_path.find_last_of("/") + 1;
    unsigned int i2 = object_path.find_last_of(".");
    file_name_ = object_path.substr(i1, i2 - i1);
    model_loaded_ = true;

    bool has_bones = armature_ != NULL;

    // Grab Texture Material Names
    texture_names_.resize(scene->mNumMaterials);

    for (unsigned int i = 0; i < texture_names_.size(); i++)
    {
        aiString string;
        scene->mMaterials[i]->GetTexture(aiTextureType_DIFFUSE,0,&string);
        std::string file(string.C_Str());

        size_t found1 = file.find_last_of("/");
        size_t found2 = file.find_last_of("\\");
        size_t use = 0;

        if (found1 == std::string::npos && found2 != std::string::npos)
            use = found2 + 1;
        else if (found2 == std::string::npos && found1 != std::string::npos)
            use = found1 + 1;
        else if (found1 != std::string::npos && found2 != std::string::npos)
            use = std::max(found1, found2) + 1;

        texture_names_[i] = file.substr(use);
    }

    // Generate AABB
    if (generate_aabb_)
        generateAABB(scene);

    // Resize vectors
    vao_.resize(scene->mNumMeshes);
    vbo_.resize(scene->mNumMeshes);
    ebo_.resize(scene->mNumMeshes);
    ind_size_.resize(scene->mNumMeshes);
    texture_numbers_.resize(scene->mNumMeshes);
    texcoord_locations_.resize(scene->mNumMeshes);
    bone_id_locations_.resize(scene->mNumMeshes);
    bone_weight_locations_.resize(scene->mNumMeshes);

    // Create VAO and bind it

#if !defined(TARGET_OS_IPHONE) && !defined(__ANDROID__)

    if (gu::Shader::getOpenGLVersion() > 2)
    {
        if (!vao_.empty())
        {
            glGenVertexArrays(vao_.size(), &vao_[0]);
            loaded_vao_ = true;
        }
    }

#else

    if (gu::Drawable::hasOES())
    {
        if (!vao_.empty())
        {
            glGenVertexArraysOES(vao_.size(), &vao_[0]);
            loaded_vao_ = true;
        }
    }

#endif

    // Create VBOs and EBOs
    if (!vbo_.empty())
        glGenBuffers(vbo_.size(), &vbo_[0]);
    if (!ebo_.empty())
        glGenBuffers(ebo_.size(), &ebo_[0]);

    // Fill Data
    for (unsigned int i = 0; i < vbo_.size(); i++)
    {

#if !defined(TARGET_OS_IPHONE) && !defined(__ANDROID__)

        if (loaded_vao_)
            glBindVertexArray(vao_[i]);

#else

        if (loaded_vao_)
            glBindVertexArrayOES(vao_[i]);

#endif

        GLushort *face_data = new GLushort[3*scene->mMeshes[i]->mNumFaces];

        unsigned int e_count = 0;

        for (unsigned int j = 0; j < scene->mMeshes[i]->mNumFaces; j++)
        {
            const aiFace *face = &scene->mMeshes[i]->mFaces[j];
            
            face_data[e_count] = face->mIndices[0];
            face_data[e_count + 1] = face->mIndices[1];
            face_data[e_count + 2] = face->mIndices[2];

            e_count += 3;
        }

        GLfloat *texture_coords = new GLfloat[2*scene->mMeshes[i]->mNumVertices];

        if (scene->mMeshes[i]->HasTextureCoords(0))
        {
            for (unsigned int k = 0; k < scene->mMeshes[i]->mNumVertices; k++)
            {
                texture_coords[k*2]   = scene->mMeshes[i]->mTextureCoords[0][k].x;
                texture_coords[k*2 + 1] = scene->mMeshes[i]->mTextureCoords[0][k].y; 
            }
		}
        else
            SDL_Log("[GUMI] (Model) Warning: \"%s\" does not have texture coordinates (mesh #%u)", object_path.c_str(), i);

        const std::vector<glm::ivec4> *bone_ids = has_bones ? armature_->getBoneIDs(i) : NULL;
        const std::vector<glm::vec4> *bone_weights = has_bones ? armature_->getBoneWeights(i) : NULL;

        //TODO: switch to GL_SHORT
        std::vector<glm::vec4> bone_ids_float;
        if (has_bones)
        {
            bone_ids_float.resize(bone_ids->size());
            for (unsigned int i = 0; i < bone_ids->size(); i++)
            {
                const glm::ivec4 &ref = bone_ids->at(i);
                bone_ids_float[i].x = ref.x;
                bone_ids_float[i].y = ref.y;
                bone_ids_float[i].z = ref.z;
                bone_ids_float[i].w = ref.w;
            }
        }

        // Calculate Sizes
        const GLuint vert_size = 3*scene->mMeshes[i]->mNumVertices*sizeof(GLfloat);
        const GLuint tex_size = scene->mMeshes[i]->HasTextureCoords(0) ? 2*scene->mMeshes[i]->mNumVertices*sizeof(GLfloat) : 0;
        const GLuint ele_size = 3*scene->mMeshes[i]->mNumFaces*sizeof(GLushort);
        const GLuint bone_weight_size = has_bones ? 4*bone_weights->size()*sizeof(GLfloat) : 0;
        const GLuint bone_id_size = has_bones ? 4*bone_ids->size()*sizeof(GLfloat) : 0;

        // Calculate Peristant Rendering Data
        ind_size_[i] = e_count;
        texture_numbers_[i] = scene->mMeshes[i]->mMaterialIndex;
        texcoord_locations_[i] = vert_size;
        bone_id_locations_[i] = vert_size + tex_size;
        bone_weight_locations_[i] = vert_size + tex_size + bone_id_size;

        // Fill VBO with Vertices and Texture Coordinates
        glBindBuffer(GL_ARRAY_BUFFER, vbo_[i]);

        if (has_bones)
             glBufferData(GL_ARRAY_BUFFER, vert_size + tex_size + bone_id_size + bone_weight_size, NULL, GL_STATIC_DRAW);
        else
             glBufferData(GL_ARRAY_BUFFER, vert_size + tex_size, NULL, GL_STATIC_DRAW);
        
        glBufferSubData(GL_ARRAY_BUFFER, 0, vert_size, scene->mMeshes[i]->mVertices);
        glBufferSubData(GL_ARRAY_BUFFER, vert_size, tex_size, texture_coords);

        if (has_bones)
        {
            glBufferSubData(GL_ARRAY_BUFFER, vert_size + tex_size, bone_id_size, &bone_ids_float.at(0));
            glBufferSubData(GL_ARRAY_BUFFER, vert_size + tex_size + bone_id_size, bone_weight_size, &bone_weights->at(0));
        }

        // Fill EBO with Face Indices
        glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, ebo_[i]);
        glBufferData(GL_ELEMENT_ARRAY_BUFFER, ele_size, face_data, GL_STATIC_DRAW);

        // Set Pointers
        glEnableVertexAttribArray(vPosition_);
        glVertexAttribPointer(vPosition_, 3, GL_FLOAT, GL_FALSE, 0, 0);

        glEnableVertexAttribArray(vTexCoord_);
        glVertexAttribPointer(vTexCoord_, 2, GL_FLOAT, GL_FALSE, 0, reinterpret_cast<void*>(texcoord_locations_[i]));

        if (has_bones)
        {
            glEnableVertexAttribArray(boneids_);
            glEnableVertexAttribArray(weights_);

            glVertexAttribPointer(boneids_, 4, GL_FLOAT, GL_FALSE, 0, reinterpret_cast<void*>(bone_id_locations_[i]));
            glVertexAttribPointer(weights_, 4, GL_FLOAT, GL_FALSE, 0, reinterpret_cast<void*>(bone_weight_locations_[i]));
        }

        delete[] face_data;
        delete[] texture_coords;
    }
    
    if (loaded_vao_)
    {

#if !defined(TARGET_OS_IPHONE) && !defined(__ANDROID__)

        glBindVertexArray(0);

#else

        glBindVertexArrayOES(0);

#endif

    }
    
    glBindBuffer(GL_ARRAY_BUFFER, 0);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);

    gu::printOpenGLErrors("Model Initialization: ");
    return has_bones;
}

void Model::generateAABB(const aiScene *scene)
{
    float min_x = FLT_MAX;
    float min_y = FLT_MAX;
    float min_z = FLT_MAX;

    float max_x = -FLT_MAX;
    float max_y = -FLT_MAX;
    float max_z = -FLT_MAX;

    for (unsigned int i = 0; i < scene->mNumMeshes; i++)
    {
        aiMesh *mesh = scene->mMeshes[i];

        for (unsigned int j = 0; j < mesh->mNumVertices; j++)
        {
            aiVector3D &vertex = mesh->mVertices[j];

            if (vertex.x < min_x)
                min_x = vertex.x;

            if (vertex.y < min_y)
                min_y = vertex.y;

            if (vertex.z < min_z)
                min_z = vertex.z;

            if (vertex.x > max_x)
                max_x = vertex.x;

            if (vertex.y > max_y)
                max_y = vertex.y;

            if (vertex.z > max_z)
                max_z = vertex.z;
        }
    }

    glm::vec3 radius;
    glm::vec3 centre;

    if ((local_z_up_ == -1 && z_up_) || local_z_up_)
    {
        radius.x = (max_x - min_x) / 2.0f;
        radius.z = (max_y - min_y) / 2.0f;
        radius.y = (max_z - min_z) / 2.0f;

        centre.x = radius.x + min_x;
        centre.z = radius.z + min_y;
        centre.y = radius.y + min_z;
    }
    else
    {
        radius.x = (max_x - min_x) / 2.0f;
        radius.y = (max_y - min_y) / 2.0f;
        radius.z = (max_z - min_z) / 2.0f;

        centre.x = radius.x + min_x;
        centre.y = radius.y + min_y;
        centre.z = radius.z + min_z;
    }

    radius += glm::vec3(0.0001f);
    aabb_ = gu::AABB(centre, radius);
}

void Model::autoLoadTextures(bool from_zip, const std::string &append)
{
    for (unsigned int i = 0; i < texture_names_.size(); i++)
    {
        if (texture_names_[i].size() > 0)
        {
            GLuint tex;

            if (!from_zip)
                tex = gu::TextureLoader::loadFileTexture(append + texture_names_[i]);
            else
                tex = gu::TextureLoader::loadZipTexture(append + texture_names_[i]);

            textures_.push_back(gu::AnimatedTextureList(gu::AnimatedTexture(tex, 0.0f)));
            has_textures_ = true;
        }
        else
        {
            textures_.push_back(gu::AnimatedTextureList(gu::AnimatedTexture(0, 0.0f)));
            SDL_Log("[GUMI] (Model) Error: Could not automatically load one of the textures -- assuming 0");
        }
    }
}

void Model::updateLastPosition()
{
    last_position_ = position_;
}

// Static

void Model::setAllZUp(bool b)
{
    z_up_ = b;
}

bool Model::getAllZUp()
{
    return z_up_;
}

// Setters

void Model::setPosition(const glm::vec3 &position, bool snap)
{
    position_ = position;

    if (snap)
        last_position_ = position;
}

void Model::setScaling(const glm::vec3 &scaling)
{
    scaling_ = scaling;
}

void Model::setRotation(const glm::vec3 &rotation)
{
    rotation_ = rotation;
}

void Model::setColour(const glm::vec4 &colour)
{
    colour_ = colour;
}
    
void Model::setLastPosition(const glm::vec3 &last_position)
{
    last_position_ = last_position;
}

void Model::setBackfaceCulling(bool b)
{
    backface_culling_ = b;
}

void Model::setGenerateAABB(bool b)
{
    generate_aabb_ = b;
}

void Model::setZUp(bool b)
{
    local_z_up_ = b ? 1 : 0;
}

// Getters

gu::Shader* Model::getShader()
{
    return shader_;
}

gu::Armature* Model::getArmature()
{
    return armature_;
}

const std::string& Model::getFileName() const
{
    return file_name_;
}

std::vector<std::string>* Model::getTextureNames()
{
    return &texture_names_;
}

const glm::vec3& Model::getPosition() const
{
    return position_;
}

const glm::vec3& Model::getScaling() const
{
    return scaling_;
}

const glm::vec3& Model::getRotation() const
{
    return rotation_;
}

const glm::vec4& Model::getColour() const
{
    return colour_;
}
    
const glm::vec3& Model::getLastPosition() const
{
    return last_position_;
}

bool Model::getBackfaceCulling() const
{
    return backface_culling_;
}

glm::vec3 Model::getInterpolatedPosition(const float interpolation) const
{
    return (interpolation * position_) + ((1.0f - interpolation) * last_position_);
}

gu::AABB Model::getModelAABB() const
{
    gu::AABB temp;
    temp.centre = aabb_.centre + position_;
    temp.radius = aabb_.radius;
    temp.radius.x *= scaling_.x;
    temp.radius.y *= scaling_.y;
    temp.radius.z *= scaling_.z;

    return temp;
}

gu::YOBB Model::getModelYOBB() const
{
    gu::YOBB temp;
    temp.centre = aabb_.centre + position_;
    temp.radius = aabb_.radius;
    temp.radius.x *= scaling_.x;
    temp.radius.y *= scaling_.y;
    temp.radius.z *= scaling_.z;
    temp.rotation = rotation_.y;

    return temp;
}

bool Model::getGenerateAABB() const
{
    return generate_aabb_;
}

gu::AnimatedTextureList* Model::getAnimatedTextureList(const std::string &first_texture_name)
{
    gu::TextureInfo *ti = gu::TextureLoader::getTextureInfo(first_texture_name);

    if (ti != NULL)
    {
        // Return the AnimatedTextureList if the very first list entry and first texture equal the name
        // This works because the first AnimatedTexture of a list is always the default, non-animated texture,
        // which always has exactly 1 entry at index 0
        for (unsigned int i = 0; i < textures_.size(); i++)
            if (ti->id == textures_[i].getAnimatedTextures()->at(0).getTextures()->at(0))
                return &textures_[i];
    }

    return NULL;
}

bool Model::getZUp() const
{
    return local_z_up_ == 1;
}

}
