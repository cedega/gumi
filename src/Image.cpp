#include "GUMI/Image.h"

namespace gu 
{

glm::ivec2 Image::render_size_;

Image::Image() :
    shader_(NULL),
    position_(0.0f),
    last_position_(0.0f),
    scaling_(1.0f),
    rotation_(0.0f),
    dimensions_(0.0f),
    colour_(1.0f),
    texture_rect_(-1.0f),
    scale_percent_(1.0f),
    scale_percent_axis_(gu::AXIS_X),
    pure_2d_(true),
    loaded_vao_(false),
    loaded_vbo_(false),
    enable_alpha_blending_(true),
    vbo_change_count_(0),
    owns_texture_(false),
    texture_(0)
{
}

Image::Image(const Image &obj) : 
    gu::Drawable(obj),
    shader_(obj.shader_),
    position_(obj.position_),
    last_position_(obj.last_position_),
    scaling_(obj.scaling_),
    rotation_(obj.rotation_),
    original_dimensions_(obj.original_dimensions_),
    dimensions_(obj.dimensions_),
    colour_(obj.colour_),
    position_percent_(obj.position_percent_),
    scale_percent_(obj.scale_percent_),
    scale_percent_axis_(obj.scale_percent_axis_),
    pure_2d_(obj.pure_2d_),
    loaded_vao_(false),
    loaded_vbo_(false),
    enable_alpha_blending_(obj.enable_alpha_blending_),
    vbo_change_count_(0)
{
    if (obj.owns_texture_)
    {
        loadTexture(obj.texture_, obj.shader_, pure_2d_);

        // Increment reference for gu::TextureLoader
        gu::TextureInfo *info = gu::TextureLoader::getTextureInfo(obj.texture_);
        info->count++;

        if (obj.texture_rect_.w >= 0.0f)
            setTextureRect(obj.texture_rect_);
    }
    else if (obj.texture_ > 0)
    {
        setTexture(obj.texture_, obj.pure_2d_);

        if (obj.texture_rect_.w >= 0.0f)
            setTextureRect(obj.texture_rect_);
    }
    else
    {
        shader_ = NULL;
        owns_texture_ = false;
        loaded_vao_ = false;
        loaded_vbo_ = false;
        texture_ = 0;
    }
}

Image::~Image()
{
    deconstruct();
}

Image& Image::operator=(const Image &obj)
{
    gu::Drawable::operator=(obj);

    deconstruct();

    shader_ = obj.shader_;
    position_ = obj.position_;
    last_position_ = obj.last_position_;
    scaling_ = obj.scaling_;
    rotation_ = obj.rotation_;
    original_dimensions_ = obj.original_dimensions_;
    dimensions_ = obj.dimensions_;
    colour_ = obj.colour_;

    position_percent_ = obj.position_percent_;
    scale_percent_ = obj.scale_percent_;
    scale_percent_axis_ = obj.scale_percent_axis_;

    pure_2d_ = obj.pure_2d_;
    enable_alpha_blending_ = obj.enable_alpha_blending_;
    vbo_change_count_ = 0;
    loaded_vao_ = false;
    loaded_vbo_ = false;

    if (obj.owns_texture_)
    {
        loadTexture(obj.texture_, obj.shader_, pure_2d_);

        // Increment reference for gu::TextureLoader
        gu::TextureInfo *info = gu::TextureLoader::getTextureInfo(obj.texture_);
        info->count++;

        if (obj.texture_rect_.w >= 0.0f)
            setTextureRect(obj.texture_rect_);
    }
    else if (obj.texture_ > 0)
    {
        setTexture(obj.texture_, obj.pure_2d_);

        if (obj.texture_rect_.w >= 0.0f)
            setTextureRect(obj.texture_rect_);
    }
    else
    {
        shader_ = NULL;
        owns_texture_ = false;
        loaded_vao_ = false;
        loaded_vbo_ = false;
        texture_ = 0;
    }

    return *this;
}

void Image::deconstruct()
{
    if (loaded_vbo_)
    {
        glDeleteBuffers(1, &vbo_);
        glDeleteBuffers(1, &ebo_);
    }

#if !defined(TARGET_OS_IPHONE) && !defined(__ANDROID__)

    if (loaded_vao_)
        glDeleteVertexArrays(1, &vao_);

#else

    if (gu::Drawable::hasOES() && loaded_vao_)
        glDeleteVertexArraysOES(1, &vao_);

#endif

    if (owns_texture_)
    {
        gu::TextureLoader::deleteTexture(texture_);
    }
}

void Image::init(const glm::ivec2 &render_size)
{
    render_size_ = render_size;
}

void Image::draw(const float interpolation)
{
    if (shader_ == NULL || !loaded_vbo_)
        return;

    if (pure_2d_)
        glDisable(GL_DEPTH_TEST);
    
    if (enable_alpha_blending_)
    {
        glEnable(GL_BLEND);
        glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
    }
    else
        glDisable(GL_BLEND);

    shader_->bind();

    glm::vec3 interpolated_position = position_*interpolation + last_position_*(1.0f - interpolation);

    shader_->modify("pos", glm::translate(glm::mat4(1.0f), interpolated_position));
    shader_->modify("scale", glm::scale(glm::mat4(1.0f), glm::vec3(scaling_, scaling_.x)));

    glm::mat4 rotate_x = glm::rotate(glm::mat4(1.0f), glm::radians(rotation_.x), glm::vec3(1.0f,0.0f,0.0f));
    glm::mat4 rotate_y = glm::rotate(glm::mat4(1.0f), glm::radians(rotation_.y), glm::vec3(0.0f,1.0f,0.0f));
    glm::mat4 rotate_z = glm::rotate(glm::mat4(1.0f), glm::radians(rotation_.z), glm::vec3(0.0f,0.0f,1.0f));

    shader_->modify("rotate", rotate_x * rotate_y * rotate_z);
    shader_->modify("colour", colour_);

    glDisable(GL_CULL_FACE);

    if (loaded_vao_)
    {

#if !defined(TARGET_OS_IPHONE) && !defined(__ANDROID__)

        glBindVertexArray(vao_);

#else

        glBindVertexArrayOES(vao_);

#endif

    }
    else
    {
        glBindBuffer(GL_ARRAY_BUFFER, vbo_);

        glEnableVertexAttribArray(vPosition_);
        glEnableVertexAttribArray(vTexCoord_);

        glVertexAttribPointer(vPosition_, 2, GL_FLOAT, GL_FALSE, 0, 0);
        glVertexAttribPointer(vTexCoord_, 2, GL_FLOAT, GL_FALSE, 0, reinterpret_cast<void*>(8*sizeof(GLfloat)));

        glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, ebo_);
    }

    glActiveTexture(GL_TEXTURE0);
    glBindTexture(GL_TEXTURE_2D, texture_);

    glDrawElements(GL_TRIANGLES, 6, GL_UNSIGNED_SHORT, 0);

    if (loaded_vao_)
    {

#if !defined(TARGET_OS_IPHONE) && !defined(__ANDROID__)

        glBindVertexArray(0);

#else

        glBindVertexArrayOES(0);

#endif

    }
    else
    {
        glDisableVertexAttribArray(vPosition_);
        glDisableVertexAttribArray(vTexCoord_);
        glBindBuffer(GL_ARRAY_BUFFER, 0);
        glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
    }

    glEnable(GL_CULL_FACE);

    if (pure_2d_)
        glEnable(GL_DEPTH_TEST);
}

void Image::bindShader(gu::Shader *shaders)
{
    shader_ = shaders;
    vPosition_ = shaders->getAttribLocation("vPosition");
    vTexCoord_ = shaders->getAttribLocation("vTexCoord");
    textureSample_ = shaders->getUniformLocation("textureSample");
    glUniform1i(textureSample_, 0);
}

void Image::loadColour(GLfloat w, GLfloat h, const glm::vec4 &colour, gu::Shader *shader, bool pure_2D)
{
    deconstruct();

    owns_texture_ = false;

    if (shader != NULL)
        bindShader(shader);

    initVBO(w, h, pure_2D);
    setColour(colour);
}

void Image::loadTexture(GLuint texture, gu::Shader *shader, bool pure_2D)
{
    deconstruct();
    gu::TextureInfo *ti = gu::TextureLoader::getTextureInfo(texture);

    if (ti != NULL)
    {
        texture_ = ti->id;
        owns_texture_ = true;

        if (shader != NULL)
            bindShader(shader);

        initVBO(ti->w, ti->h, pure_2D);
    }
}

void Image::initVBO(GLfloat width, GLfloat height, bool pure_2D)
{
    pure_2d_ = pure_2D;

    GLfloat vertices[8];

    // Top-left
    vertices[0] = -width/2;
    vertices[1] = height/2;

    // Bottom-left
    vertices[2] = -width/2;
    vertices[3] = -height/2;

    // Bottom-right
    vertices[4] = width/2;
    vertices[5] = -height/2;

    // Top-right
    vertices[6] = width/2;
    vertices[7] = height/2;

    GLushort elements[] = {
        0, 1, 2,
        2, 3, 0
    };

    GLfloat texcoords[] = {
        0.0f, 0.0f,
        0.0f, 1.0f,
        1.0f, 1.0f,
        1.0f, 0.0f
    };

    GLfloat texcoords_2D[] = {
        0.0f, 1.0f,
        0.0f, 0.0f,
        1.0f, 0.0f,
        1.0f, 1.0f
    };

#if !defined(TARGET_OS_IPHONE) && !defined(__ANDROID__)

    if (gu::Shader::getOpenGLVersion() > 2)
    {
        glGenVertexArrays(1, &vao_);
        glBindVertexArray(vao_);
        loaded_vao_ = true;
    }

#else

    if (gu::Drawable::hasOES())
    {
        glGenVertexArraysOES(1, &vao_);
        glBindVertexArrayOES(vao_);
        loaded_vao_ = true;
    }

#endif

    const GLuint vertex_size = 8*sizeof(GLfloat);
    const GLuint element_size = 6*sizeof(GLushort);
    const GLuint texcoord_size = 8*sizeof(GLfloat);

    glGenBuffers(1, &vbo_);
    glGenBuffers(1, &ebo_);

    glBindBuffer(GL_ARRAY_BUFFER, vbo_);

    glBufferData(GL_ARRAY_BUFFER, vertex_size + texcoord_size, NULL, GL_STATIC_DRAW);
    glBufferSubData(GL_ARRAY_BUFFER, 0, vertex_size, vertices);

    if (pure_2D)
        glBufferSubData(GL_ARRAY_BUFFER, vertex_size, texcoord_size, texcoords_2D);
    else
        glBufferSubData(GL_ARRAY_BUFFER, vertex_size, texcoord_size, texcoords);

    // Fill EBO with Face Indices
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, ebo_);
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, element_size, elements, GL_STATIC_DRAW);
   
    // Set Pointers
    glEnableVertexAttribArray(vPosition_);
    glVertexAttribPointer(vPosition_, 2, GL_FLOAT, GL_FALSE, 0, 0);

    glEnableVertexAttribArray(vTexCoord_);
    glVertexAttribPointer(vTexCoord_, 2, GL_FLOAT, GL_FALSE, 0, reinterpret_cast<void*>(8*sizeof(GLfloat)));

    loaded_vbo_ = true;
    dimensions_.x = width;
    dimensions_.y = height;
    original_dimensions_ = dimensions_;

    if (loaded_vao_)
    {

#if !defined(TARGET_OS_IPHONE) && !defined(__ANDROID__)

        glBindVertexArray(0);

#else

        glBindVertexArrayOES(0);

#endif

    }
    
    glBindBuffer(GL_ARRAY_BUFFER, 0);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
}

void Image::updateLastPosition()
{
    last_position_ = position_;
}

void Image::enableAlphaBlending(bool b)
{
    enable_alpha_blending_ = b;
}

void Image::applyDynamicScaling()
{
    float render_size;
    float dimension;

    if (scale_percent_axis_ == gu::AXIS_X)
    {
        render_size = render_size_.x;
        dimension = dimensions_.x;
    }
    else if (scale_percent_axis_ == gu::AXIS_Y)
    {
        render_size = render_size_.y;
        dimension = dimensions_.y;
    }
    else // gu::AXIS_XY
    {
        scaling_ = glm::vec2(render_size_.x * scale_percent_ / dimensions_.x, render_size_.y * scale_percent_ / dimensions_.y);
        return;
    }

    float factor = render_size * scale_percent_ / dimension;
    scaling_ = glm::vec2(factor);
}
    
// Setters

void Image::setPosition(const glm::vec2 &position, bool top_left_origin, bool snap)
{
    if (top_left_origin)
    {
        position_.x = position.x + (scaling_.x * dimensions_.x) / 2.0f;
        position_.y = position.y + (scaling_.y * dimensions_.y) / 2.0f;
    }
    else
    {
        position_.x = position.x;
        position_.y = position.y;
    }

    if (snap)
        last_position_ = position_;
}

void Image::setPosition(const glm::vec3 &position, bool snap)
{
    position_ = position;

    if (snap)
        last_position_ = position;
}

void Image::setLastPosition(const glm::vec3 &last_position)
{
    last_position_ = last_position;
}

void Image::setPositionPercent(const glm::vec2 &position_percent)
{
    position_percent_ = position_percent;
    position_.x = position_percent_.x * render_size_.x;
    position_.y = position_percent_.y * render_size_.y;
}

void Image::setScalePercent(float percent, gu::Axis axis)
{
    if (axis != gu::AXIS_X && axis != gu::AXIS_Y && axis != gu::AXIS_XY)
        return;

    scale_percent_ = percent;
    scale_percent_axis_ = axis;
    applyDynamicScaling();
}

void Image::setScaling(const glm::vec2 &scaling)
{
    scaling_ = scaling;
}

void Image::setRotation(const glm::vec3 &rotation)
{
    rotation_ = rotation;
}

void Image::setTextureRect(const glm::vec4 &rect)
{
    texture_rect_ = rect;
    GLfloat data[16];

    if (pure_2d_)
    {
        data[8] = rect.x / original_dimensions_.x;
        data[9] = (rect.y + rect.w) / original_dimensions_.y;

        data[10] = rect.x / original_dimensions_.x;
        data[11] = rect.y / original_dimensions_.y;

        data[12] = (rect.x + rect.z) / original_dimensions_.x;
        data[13] = rect.y / original_dimensions_.y;

        data[14] = (rect.x + rect.z) / original_dimensions_.x;
        data[15] = (rect.y + rect.w) / original_dimensions_.y;
    }
    else
    {
        data[8] = rect.x / original_dimensions_.x;
        data[9] = rect.y / original_dimensions_.y;

        data[10] = rect.x / original_dimensions_.x;
        data[11] = (rect.y + rect.w) / original_dimensions_.y;

        data[12] = (rect.x + rect.z) / original_dimensions_.x;
        data[13] = (rect.y + rect.w) / original_dimensions_.y;

        data[14] = (rect.x + rect.z) / original_dimensions_.x;
        data[15] = rect.y / original_dimensions_.y;
    }

    // Process new dimensions
    dimensions_.x = rect.z;
    dimensions_.y = rect.w;

    // Top-left
    data[0] = -dimensions_.x / 2;
    data[1] = dimensions_.y / 2;

    // Bottom-left
    data[2] = -dimensions_.x / 2;
    data[3] = -dimensions_.y / 2;

    // Bottom-right
    data[4] = dimensions_.x / 2;
    data[5] = -dimensions_.y / 2;

    // Top-right
    data[6] = dimensions_.x / 2;
    data[7] = dimensions_.y / 2;

    glBindBuffer(GL_ARRAY_BUFFER, vbo_);

    if (vbo_change_count_ < 2)
        glBufferData(GL_ARRAY_BUFFER, 16*sizeof(GLfloat), data, GL_STATIC_DRAW);
    else
        glBufferData(GL_ARRAY_BUFFER, 16*sizeof(GLfloat), data, GL_DYNAMIC_DRAW);

    glBindBuffer(GL_ARRAY_BUFFER, 0);

    vbo_change_count_++;
}

void Image::setColour(const glm::vec4 &colour)
{
    colour_ = colour;
}

void Image::setTexture(GLuint texture_id, bool pure_2D)
{
    deconstruct();
    texture_ = texture_id;
    gu::TextureInfo *ti = gu::TextureLoader::getTextureInfo(texture_);

    int w = 0;
    int h = 0;

    if (ti != NULL)
    {
        w = ti->w;
        h = ti->h;
    }

    owns_texture_ = false;
    initVBO(w, h, pure_2D);
}

void Image::setTexture(GLuint texture_id, int width, int height, bool pure_2D)
{
    deconstruct();
    texture_ = texture_id;
    owns_texture_ = false;
    initVBO(width, height, pure_2D);
}

// Getters

gu::Shader* Image::getShader()
{
    return shader_;
}

glm::vec2 Image::getPosition(bool top_left_origin) const
{
    glm::vec2 temp;
    temp.x = position_.x;
    temp.y = position_.y;

    if (top_left_origin)
        return temp - glm::vec2((scaling_.x * dimensions_.x) / 2.0f, (scaling_.y * dimensions_.y) / 2.0f);
    else
        return temp;
}

const glm::vec3& Image::getPosition() const
{
    return position_;
}

const glm::vec3& Image::getLastPosition() const
{
    return last_position_;
}

const glm::vec2& Image::getPositionPercent() const
{
    return position_percent_;
}

float Image::getScalePercent(gu::Axis *axis) const
{
    if (axis != NULL)
        *axis = scale_percent_axis_;

    return scale_percent_;
}

const glm::vec2& Image::getScaling() const
{
    return scaling_;
}

const glm::vec3& Image::getRotation() const
{
    return rotation_;
}
    
const glm::vec2& Image::getDimensions() const
{
    return dimensions_;
}

const glm::vec2& Image::getOriginalDimensions() const
{
    return original_dimensions_;
}

const glm::vec4& Image::getColour() const
{
    return colour_;
}

glm::vec4 Image::getTextureRect() const
{
    if (texture_rect_.w < 0.0f)
    {
        return glm::vec4(0, 0, dimensions_.x, dimensions_.y);
    }
    else
    {
        return texture_rect_;
    }
}

gu::AABB Image::getImageAABB() const
{
    gu::AABB temp;
    temp.centre = position_;
    temp.radius = glm::vec3(dimensions_.x / 2.0f, dimensions_.y / 2.0f, 0.0f);

    glm::mat4 rotate_x = glm::rotate(glm::mat4(1.0f), glm::radians(rotation_.x), glm::vec3(1.0f,0.0f,0.0f));
    glm::mat4 rotate_y = glm::rotate(glm::mat4(1.0f), glm::radians(rotation_.y), glm::vec3(0.0f,1.0f,0.0f));
    glm::mat4 rotate_z = glm::rotate(glm::mat4(1.0f), glm::radians(rotation_.z), glm::vec3(0.0f,0.0f,1.0f));

    glm::vec4 new_radius = rotate_x * rotate_y * rotate_z * glm::vec4(temp.radius, 1.0f);
    temp.radius.x = gu::absval(new_radius.x);
    temp.radius.y = gu::absval(new_radius.y);
    temp.radius.z = gu::absval(new_radius.z);

    temp.radius.x *= scaling_.x;
    temp.radius.y *= scaling_.y;
    temp.radius.z *= scaling_.x;

    return temp;
}

gu::AABB Image::getBillboardAABB(const glm::mat4 &view) const
{
    glm::mat4 inverse = glm::transpose(view);
    inverse[0][3] = 0.0f;
    inverse[1][3] = 0.0f;
    inverse[2][3] = 0.0f;

    gu::AABB temp;
    temp.centre = position_;
    temp.radius = glm::vec3(dimensions_.x / 2.0f, dimensions_.y / 2.0f, 0.0f);

    glm::vec4 new_radius = inverse * glm::vec4(temp.radius, 1.0f);
    temp.radius.x = gu::absval(new_radius.x);
    temp.radius.y = gu::absval(new_radius.y);
    temp.radius.z = gu::absval(new_radius.z);

    temp.radius.x *= scaling_.x;
    temp.radius.y *= scaling_.y;
    temp.radius.z *= scaling_.x;

    return temp;
}

glm::vec4 Image::getAsRectangle() const
{
    glm::vec4 rect;
    rect.z = dimensions_.x * scaling_.x;
    rect.w = dimensions_.y * scaling_.y;
    rect.x = position_.x - rect.z / 2.0f;
    rect.y = position_.y - rect.w / 2.0f;

    return rect;
}

GLuint Image::getTexture() const
{
    return texture_;
}

}
