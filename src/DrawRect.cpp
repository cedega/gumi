#include "GUMI/DrawRect.h"

namespace gu
{

bool DrawRect::loaded_vao_ = false;
bool DrawRect::loaded_vbo_ = false;
gu::Shader DrawRect::shader_;
GLuint DrawRect::vao_;
GLuint DrawRect::vbo_;
GLuint DrawRect::ebo_;
GLuint DrawRect::vPosition_;

void DrawRect::draw(const gu::AABB &aabb, float outline_width)
{

#if defined(TARGET_OS_IPHONE) || defined(__ANDROID__)

    return;

#else

    if (!loaded_vbo_)
        return;

    glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
    
    glDisable(GL_CULL_FACE);
    glLineWidth(outline_width);

    shader_.bind();

    if (gu::Shader::getOpenGLVersion() > 2)
        glBindVertexArray(vao_);

    glEnableVertexAttribArray(vPosition_);

    glBindBuffer(GL_ARRAY_BUFFER, vbo_);

    GLfloat vertices[24];

    const GLfloat left = aabb.centre.x - aabb.radius.x;
    const GLfloat right = aabb.centre.x + aabb.radius.x;
    const GLfloat up = aabb.centre.y + aabb.radius.y;
    const GLfloat down = aabb.centre.y - aabb.radius.y;
    const GLfloat near = aabb.centre.z - aabb.radius.z;
    const GLfloat far = aabb.centre.z + aabb.radius.z;

    vertices[0] = left;
    vertices[1] = up;
    vertices[2] = near;

    vertices[3] = right;
    vertices[4] = up;
    vertices[5] = near;

    vertices[6] = right;
    vertices[7] = down;
    vertices[8] = near;

    vertices[9] = left;
    vertices[10] = down;
    vertices[11] = near;

    vertices[12] = left;
    vertices[13] = up;
    vertices[14] = far;

    vertices[15] = right;
    vertices[16] = up;
    vertices[17] = far;

    vertices[18] = right;
    vertices[19] = down;
    vertices[20] = far;

    vertices[21] = left;
    vertices[22] = down;
    vertices[23] = far;

    glBufferSubData(GL_ARRAY_BUFFER, 0, sizeof(vertices), vertices);
    glVertexAttribPointer(vPosition_, 3, GL_FLOAT, GL_FALSE, 0, 0);

    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, ebo_);
    glDrawElements(GL_TRIANGLES, 36, GL_UNSIGNED_INT, 0);

    glDisableVertexAttribArray(vPosition_);

    glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);

#endif

}

void DrawRect::draw(const gu::YOBB &yobb, float outline_width)
{

#if defined(TARGET_OS_IPHONE) || defined(__ANDROID__)

    return;

#else

    if (!loaded_vbo_)
        return;

    glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
    
    glDisable(GL_CULL_FACE);
    glLineWidth(outline_width);

    shader_.bind();

    if (gu::Shader::getOpenGLVersion() > 2)
        glBindVertexArray(vao_);

    glEnableVertexAttribArray(vPosition_);

    glBindBuffer(GL_ARRAY_BUFFER, vbo_);

    GLfloat vertices[24];
    
    std::vector<glm::vec3> temp;
    gu::corners(yobb, &temp);
    for (unsigned int i = 0; i < 8; i++)
    {
        vertices[i*3 + 0] = temp[i].x;
        vertices[i*3 + 1] = temp[i].y;
        vertices[i*3 + 2] = temp[i].z;
    }
    
    glBufferSubData(GL_ARRAY_BUFFER, 0, sizeof(vertices), vertices);
    glVertexAttribPointer(vPosition_, 3, GL_FLOAT, GL_FALSE, 0, 0);

    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, ebo_);
    glDrawElements(GL_TRIANGLES, 36, GL_UNSIGNED_INT, 0);

    glDisableVertexAttribArray(vPosition_);

    glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);

#endif

}

void DrawRect::draw(const glm::vec4 &rect, float depth, float outline_width)
{

#if defined(TARGET_OS_IPHONE) || defined(__ANDROID__)

    return;

#else

    if (!loaded_vbo_)
        return;

    const float x = rect.x;
    const float y = rect.y;
    const float w = rect.z;
    const float h = rect.w;

    glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
    
    glDisable(GL_CULL_FACE);
    glLineWidth(outline_width);

    shader_.bind();

    if (gu::Shader::getOpenGLVersion() > 2)
        glBindVertexArray(vao_);

    glEnableVertexAttribArray(vPosition_);

    glBindBuffer(GL_ARRAY_BUFFER, vbo_);

    GLfloat vertices[12];

    const GLfloat left = x;
    const GLfloat right = x + w;
    const GLfloat up = -y;
    const GLfloat down = -y - h;

    vertices[0] = left;
    vertices[1] = up;
    vertices[2] = depth;

    vertices[3] = right;
    vertices[4] = up;
    vertices[5] = depth;

    vertices[6] = right;
    vertices[7] = down;
    vertices[8] = depth;

    vertices[9] = left;
    vertices[10] = down;
    vertices[11] = depth;

    glBufferSubData(GL_ARRAY_BUFFER, 0, 12*sizeof(GLfloat), vertices);
    glVertexAttribPointer(vPosition_, 3, GL_FLOAT, GL_FALSE, 0, 0);

    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, ebo_);
    glDrawElements(GL_TRIANGLES, 6, GL_UNSIGNED_INT, 0);

    glDisableVertexAttribArray(vPosition_);

    glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);

#endif

}

void DrawRect::draw(const glm::vec3 &ray_position, const glm::vec3 &ray_direction, float distance, const glm::vec4 &colour, float outline_width)
{

#if defined(TARGET_OS_IPHONE) || defined(__ANDROID__)

    return;

#else

    if (!loaded_vbo_)
        return;

    glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
    
    glDisable(GL_CULL_FACE);
    glLineWidth(outline_width);

    shader_.bind();
    shader_.modify("colour", colour);

    if (gu::Shader::getOpenGLVersion() > 2)
        glBindVertexArray(vao_);

    glEnableVertexAttribArray(vPosition_);

    glBindBuffer(GL_ARRAY_BUFFER, vbo_);

    GLfloat vertices[6];
    
    vertices[0] = ray_position.x;
    vertices[1] = ray_position.y;
    vertices[2] = ray_position.z;

    vertices[3] = ray_position.x + (ray_direction.x * distance);
    vertices[4] = ray_position.y + (ray_direction.y * distance);
    vertices[5] = ray_position.z + (ray_direction.z * distance);
    
    glBufferSubData(GL_ARRAY_BUFFER, 0, sizeof(vertices), vertices);
    glVertexAttribPointer(vPosition_, 3, GL_FLOAT, GL_FALSE, 0, 0);

    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, ebo_);
    glDrawElements(GL_LINES, 2, GL_UNSIGNED_INT, 0);

    glDisableVertexAttribArray(vPosition_);

    glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);

    shader_.modify("colour", glm::vec4(0.0f, 0.0f, 0.0f, 1.0f));

#endif

}

void DrawRect::init()
{

#if defined(TARGET_OS_IPHONE) || defined(__ANDROID__)

    return;

#else

    destroy();

    const char *v_shader =
    "in vec3 vPosition;\n"
    "uniform mat4 view;\n"
    "uniform mat4 proj;\n"
    "void main()\n"
    "{\n"
    "    vec4 newPos = vec4(vPosition, 1.0);\n"
    "    gl_Position = proj * view * newPos;\n"
    "}\n";

    const char *f_shader =
    "uniform vec4 colour;\n"
    "void main()\n"
    "{\n"
    "   gl_FragColor = colour;\n"
    "}\n";
    
    shader_.compile(v_shader, f_shader, "130");
    shader_.modify("proj", glm::mat4(1.0f));
    shader_.modify("view", glm::mat4(1.0f));
    shader_.modify("colour", glm::vec4(0.0f, 0.0f, 0.0f, 1.0f));

    GLfloat vertices[24];

    // Top-left [0]
    vertices[0] = -0.5f;
    vertices[1] = 0.5f;
    vertices[2] = -0.5f;

    // Top-right [1]
    vertices[3] = 0.5f;
    vertices[4] = 0.5f;
    vertices[5] = -0.5f;

    // Bottom-right [2]
    vertices[6] = 0.5f;
    vertices[7] = -0.5f;
    vertices[8] = -0.5f;

    // Bottom-left [3]
    vertices[9] = -0.5f;
    vertices[10] = -0.5f;
    vertices[11] = -0.5f;

    // Top-left [4]
    vertices[12] = -0.5f;
    vertices[13] = 0.5f;
    vertices[14] = 0.5f;

    // Top-right [5]
    vertices[15] = 0.5f;
    vertices[16] = 0.5f;
    vertices[17] = 0.5f;

    // Bottom-right [6]
    vertices[18] = 0.5f;
    vertices[19] = -0.5f;
    vertices[20] = 0.5f;

    // Bottom-left [7]
    vertices[21] = -0.5f;
    vertices[22] = -0.5f;
    vertices[23] = 0.5f;

    GLuint elements[] = {
        // Front
        0, 1, 2,
        2, 3, 0,
        // Right
        1, 5, 6,
        6, 2, 1,
        // Left
        4, 0, 3,
        3, 7, 4,
        // Back
        5, 4, 7,
        7, 6, 5,
        // Top
        4, 5, 1,
        1, 0, 4,
        // Bottom
        3, 2, 6,
        6, 7, 3
    };

    if (gu::Shader::getOpenGLVersion() > 2)
    {
        glGenVertexArrays(1, &vao_);
        glBindVertexArray(vao_);
        loaded_vao_ = true;
    }

    glGenBuffers(1 ,&vbo_);
    glGenBuffers(1 ,&ebo_);

    glBindBuffer(GL_ARRAY_BUFFER, vbo_);
    glBufferData(GL_ARRAY_BUFFER, sizeof(vertices), vertices, GL_DYNAMIC_DRAW);

    // Fill EBO with Face Indices
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, ebo_);
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(elements), elements, GL_STATIC_DRAW);
    
    vPosition_ = shader_.getAttribLocation("vPosition");

    // Set Pointers
    glVertexAttribPointer(vPosition_, 3, GL_FLOAT, GL_FALSE, 0, 0);
    glEnableVertexAttribArray(vPosition_);

    loaded_vbo_ = true;

#endif

}

void DrawRect::destroy()
{

#if defined(TARGET_OS_IPHONE) || defined(__ANDROID__)

    return;

#else

    if (loaded_vbo_)
    {
        glDeleteBuffers(1, &vbo_);
        glDeleteBuffers(1, &ebo_);
    }

    if (loaded_vao_)
        glDeleteVertexArrays(1, &vao_);

#endif

}

// Setters

void DrawRect::setPerspective(const glm::mat4 &perspective)
{
    
#if defined(TARGET_OS_IPHONE) || defined(__ANDROID__)

    return;

#endif

    shader_.modify("proj", perspective);
}

void DrawRect::setView(const glm::mat4 &view)
{

#if defined(TARGET_OS_IPHONE) || defined(__ANDROID__)

    return;

#endif

    shader_.modify("view", view);
}

}
