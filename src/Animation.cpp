#include "GUMI/Animation.h"

namespace gu
{

Animation::Animation() :
    time_(0.0f),
    ticks_per_second_(1.0f),
    duration_(0.0f),
    playback_speed_factor_(1.0f),
    looping_(true),
    stopped_(false)
{
}

Animation::Animation(const Animation &obj) :
    time_(obj.time_),
    ticks_per_second_(obj.ticks_per_second_),
    duration_(obj.duration_),
    playback_speed_factor_(obj.playback_speed_factor_),
    looping_(obj.looping_),
    stopped_(obj.stopped_)
{
    bone_filter_ = obj.bone_filter_;

    channels_.resize(obj.channels_.size());
    for (unsigned int i = 0; i < obj.channels_.size(); i++)
    {
        channels_[i].valid = obj.channels_[i].valid;
        channels_[i].name = obj.channels_[i].name;
        channels_[i].positions = obj.channels_[i].positions;
        channels_[i].rotations = obj.channels_[i].rotations;
        channels_[i].scalings = obj.channels_[i].scalings;
    }
}

Animation& Animation::operator=(const Animation &obj)
{
    time_ = obj.time_;
    ticks_per_second_ = obj.ticks_per_second_;
    duration_ = obj.duration_;
    playback_speed_factor_ = obj.playback_speed_factor_;
    looping_ = obj.looping_;
    stopped_ = obj.stopped_;
    bone_filter_ = obj.bone_filter_;

    channels_.resize(obj.channels_.size());
    for (unsigned int i = 0; i < obj.channels_.size(); i++)
    {
        channels_[i].valid = obj.channels_[i].valid;
        channels_[i].name = obj.channels_[i].name;
        channels_[i].positions = obj.channels_[i].positions;
        channels_[i].rotations = obj.channels_[i].rotations;
        channels_[i].scalings = obj.channels_[i].scalings;
    }

    return *this;
}

void Animation::tick(const float ft)
{
    time_ += (ft * playback_speed_factor_);
}

void Animation::addBoneFilter(const std::string &bone_name)
{
    bone_filter_.push_back(bone_name);
}

bool Animation::loadFileAnimation(const std::string &file_path)
{
    if (gu::Zip::getRedirectFileLoading())
        return loadZipAnimation(file_path);

    Assimp::Importer importer;

    std::string file_extension = file_path.substr(file_path.find_last_of("."));

    importer.SetPropertyInteger(AI_CONFIG_PP_RVC_FLAGS, aiComponent_TANGENTS_AND_BITANGENTS | aiComponent_COLORS | aiComponent_TEXCOORDS | aiComponent_BONEWEIGHTS | aiComponent_TEXTURES | aiComponent_LIGHTS | aiComponent_CAMERAS | aiComponent_MESHES | aiComponent_MATERIALS);

#if defined(TARGET_OS_IPHONE) || defined(__ANDROID__)

    SDL_RWops *rw = SDL_RWFromFile(file_path.c_str(), "rb");
    Sint64 size = rw->size(rw);
    char *buffer = new char[size];
    rw->read(rw, buffer, size, 1);
    const aiScene *scene = importer.ReadFileFromMemory(buffer, size, aiProcess_RemoveComponent, file_extension.c_str());
    delete[] buffer;
    SDL_FreeRW(rw);
    
    if (scene == NULL)
    {
        SDL_Log("[GUMI] (Animation) Error: Failed to load animation file: \"%s\"", file_path.c_str());
        return false;
    }
    
    return loadScene(scene);

#else

    const aiScene *scene = importer.ReadFile(file_path.c_str(), aiProcess_RemoveComponent);
    
    if (scene == NULL)
    {
        SDL_Log("[GUMI] (Animation) Error: Failed to load animation from importer: \"%s\"", file_path.c_str());
        return false;
    }
    
    return loadScene(scene);

#endif

}

bool Animation::loadZipAnimation(const std::string &zip_path)
{
    Assimp::Importer importer;

    std::string file_extension = zip_path.substr(zip_path.find_last_of("."));

    importer.SetPropertyInteger(AI_CONFIG_PP_RVC_FLAGS, aiComponent_TANGENTS_AND_BITANGENTS | aiComponent_COLORS | aiComponent_TEXCOORDS | aiComponent_BONEWEIGHTS | aiComponent_TEXTURES | aiComponent_LIGHTS | aiComponent_CAMERAS | aiComponent_MESHES | aiComponent_MATERIALS);

#if defined(TARGET_OS_IPHONE) || defined(__ANDROID__)

    SDL_RWops *rw = SDL_RWFromFile(zip_path.c_str(), "rb");
    Sint64 size = rw->size(rw);
    char *buffer = new char[size];
    rw->read(rw, buffer, size, 1);
    const aiScene *scene = importer.ReadFileFromMemory(buffer, size, aiProcess_RemoveComponent, file_extension.c_str());
    delete[] buffer;
    SDL_FreeRW(rw);
    
    if (scene == NULL)
    {
        SDL_Log("[GUMI] (Animation) Error: Failed to load animation file: \"%s\"", zip_path.c_str());
        return false;
    }
    
    return loadScene(scene);

#else
    
    gu::Zip zip(zip_path);
    const aiScene *scene = importer.ReadFileFromMemory(zip.getBuffer(), zip.getSize(), aiProcess_RemoveComponent, file_extension.c_str());
    free(zip.getBuffer());

    if (scene == NULL)
    {
        SDL_Log("[GUMI] (Animation) Error: Failed to load animation from importer: \"%s\"", zip_path.c_str());
        return false;
    }

    return loadScene(scene);

#endif

}

bool Animation::loadScene(const aiScene *scene)
{
    if (!scene->HasAnimations())
    {
        SDL_Log("[GUMI] (Animation) Error: No animations in scene to load");
        return false;
    }

    const aiAnimation *anim = scene->mAnimations[0];

    duration_ = anim->mDuration;
    ticks_per_second_ = anim->mTicksPerSecond;

    // Copy data to channels
    channels_.resize(anim->mNumChannels);

    for (unsigned int i = 0; i < anim->mNumChannels; i++)
    {
        channels_[i].valid = true;
        channels_[i].name = anim->mChannels[i]->mNodeName.data;

        // If filter is empty, load all bones. If it's not, bone needs to be in the filter
        bool found = bone_filter_.empty();

        for (unsigned int j = 0; j < bone_filter_.size(); j++)
            if (bone_filter_[j].compare(channels_[i].name) == 0)
            {
                found = true;
                break;
            }

        if (!found)
        {
            channels_[i].valid = false;
        }

        for (unsigned int j = 0; j < anim->mChannels[i]->mNumPositionKeys; j++)
            channels_[i].positions.push_back(anim->mChannels[i]->mPositionKeys[j]);

        for (unsigned int j = 0; j < anim->mChannels[i]->mNumRotationKeys; j++)
            channels_[i].rotations.push_back(anim->mChannels[i]->mRotationKeys[j]);

        for (unsigned int j = 0; j < anim->mChannels[i]->mNumScalingKeys; j++)
            channels_[i].scalings.push_back(anim->mChannels[i]->mScalingKeys[j]);

        if (channels_[i].positions.size() != channels_[i].rotations.size() || channels_[i].positions.size() != channels_[i].scalings.size())
        {
            SDL_Log("[GUMI] (Animation) Warning: Individual channel sizes do not match");
        }
    }

    return true;
}

void Animation::sample(unsigned int *frame, unsigned int *last_frame, float *factor)
{
    sampleDAE(frame, last_frame, factor);
}

void Animation::sampleDAE(unsigned int *frame, unsigned int *last_frame, float *factor)
{
    // ------------------------
    // No channels in the animation!
    // ------------------------

    if (channels_.empty())
    {
        if (frame != NULL)
            *frame = 0;

        if (last_frame != NULL)
            *last_frame = 0;

        if (factor != NULL)
            *factor = 1.0f;

        return;
    }

    // ------------------------
    // Time is after the last animation frame -- assume last frame
    // ------------------------

    const unsigned int LAST = channels_[0].rotations.empty() ? 0 : channels_[0].rotations.size() - 1;

    if (time_ > duration_)
    {
        if (looping_)
            time_ -= duration_;
        else
        {
            if (frame != NULL)
                *frame = LAST;

            if (last_frame != NULL)
                *last_frame = LAST;

            if (factor != NULL)
                *factor = 1.0f;

            stopped_ = true;
            return;
        }
    }

    // ------------------------
    // Time is in between two key frames
    // ------------------------

    const float time_step = 
        channels_[0].rotations.size() >= 2 ?
        channels_[0].rotations[1].mTime - channels_[0].rotations[0].mTime : 
        0.0f;

    const unsigned int f = time_step > 0 ? time_ / time_step : 0;

    if (frame != NULL)
        *frame = f; 

    if (last_frame != NULL)
    {
        if (looping_)
            *last_frame = f > 0 ? f - 1 : LAST;
        else
            *last_frame = f > 0 ? f - 1 : 0;
    }

    if (factor != NULL)
        *factor = (time_ / time_step) - f;
}

void Animation::restart()
{
    time_ = 0.0f;
    stopped_ = false;
}

bool Animation::stopped() const
{
    return stopped_;
}

// Setters

void Animation::setLooping(bool b)
{
    looping_ = b;
}

void Animation::setTime(float t)
{
    time_ = t;
}

void Animation::setPlaybackSpeedFactor(float factor)
{
    playback_speed_factor_ = factor;
}

// Getters

const std::vector<Channel>& Animation::getChannels() const
{
    return channels_;
}

bool Animation::getLooping() const
{
    return looping_;
}

float Animation::getTime() const
{
    return time_;
}

float Animation::getDuration() const
{
    return duration_;
}

float Animation::getPlaybackSpeedFactor() const
{
    return playback_speed_factor_;
}

}
