#include "GUMI/Tools.h"
#include "GUMI/Engine.h"

gu::Engine *g_engine = NULL;

namespace gu
{

void attachEngine(gu::Engine *engine)
{
    g_engine = engine;
}

std::string toString(bool boolean)
{
	if (boolean)
		return "true";
	else
		return "false";
}

std::string toString(char character)
{
   std::stringstream ss;
   ss << character;
   return ss.str();	
}

std::string toString(float number)
{
   std::stringstream ss;
   ss << std::fixed << number;
   return ss.str();	
}

std::string toString(int number)
{
   std::stringstream ss;
   ss << number;
   return ss.str();	
}

std::string toString(double number)
{
   std::stringstream ss;
   ss << number;
   return ss.str();	
}

std::string toString(long number)
{
   std::stringstream ss;
   ss << number;
   return ss.str();
}

std::string toString(const glm::vec2 &v)
{
    std::string temp = "(";
    temp += toString(v.x) + ",";
    temp += toString(v.y) + ")";
    return temp;
}

std::string toString(const glm::vec3 &v)
{
    std::string temp = "(";
    temp += toString(v.x) + ",";
    temp += toString(v.y) + ",";
    temp += toString(v.z) + ")";
    return temp;
}

std::string toString(const glm::vec4 &v)
{
    std::string temp = "(";
    temp += toString(v.x) + ",";
    temp += toString(v.y) + ",";
    temp += toString(v.z) + ",";
    temp += toString(v.w) + ")";
    return temp;
}

bool fileExists(const std::string &path)
{
    std::ifstream infile(path.c_str());
    return infile.good();
}

float absval(float number)
{
    return number < 0 ? number * -1 : number;
}

int round(float num)
{
	return num + 0.5f;
}

int random(int min, int max)
{
    int ran = ((static_cast<double>(std::rand())/RAND_MAX) * (max - min + 1)) + min;
    return ran > max ? max : ran;
}

float random(float min, float max, int precision)
{
    return gu::random(static_cast<int>(min*precision), static_cast<int>(max*precision)) / static_cast<float>(precision);
}

void corners(const gu::AABB &aabb, std::vector<glm::vec3> *output)
{
    if (output == NULL)
        return;

    glm::vec3 temp;
    output->resize(8);

    // top-left-front
    temp.x = -aabb.radius.x + aabb.centre.x; // -- == +
    temp.y = aabb.radius.y + aabb.centre.y;
    temp.z = -aabb.radius.z + aabb.centre.z;
    output->at(0) = temp;

    // top-right-front
    temp.x = aabb.radius.x + aabb.centre.x; // -- == +
    temp.y = aabb.radius.y + aabb.centre.y;
    temp.z = -aabb.radius.z + aabb.centre.z;
    output->at(1) = temp;

    // bottom-right-front
    temp.x = aabb.radius.x + aabb.centre.x; // -- == +
    temp.y = -aabb.radius.y + aabb.centre.y;
    temp.z = -aabb.radius.z + aabb.centre.z;
    output->at(2) = temp;

    // bottom-left-front
    temp.x = -aabb.radius.x + aabb.centre.x; // -- == +
    temp.y = -aabb.radius.y + aabb.centre.y;
    temp.z = -aabb.radius.z + aabb.centre.z;
    output->at(3) = temp;

    // top-left-back
    temp.x = -aabb.radius.x + aabb.centre.x; // -- == +
    temp.y = aabb.radius.y + aabb.centre.y;
    temp.z = aabb.radius.z + aabb.centre.z;
    output->at(4) = temp;

    // top-right-back
    temp.x = aabb.radius.x + aabb.centre.x; // -- == +
    temp.y = aabb.radius.y + aabb.centre.y;
    temp.z = aabb.radius.z + aabb.centre.z;
    output->at(5) = temp;

    // bottom-right-back
    temp.x = aabb.radius.x + aabb.centre.x; // -- == +
    temp.y = -aabb.radius.y + aabb.centre.y;
    temp.z = aabb.radius.z + aabb.centre.z;
    output->at(6) = temp;

    // bottom-left-back
    temp.x = -aabb.radius.x + aabb.centre.x; // -- == +
    temp.y = -aabb.radius.y + aabb.centre.y;
    temp.z = aabb.radius.z + aabb.centre.z;
    output->at(7) = temp;
}

void corners(const gu::YOBB &yobb, std::vector<glm::vec3> *output)
{
    if (output == NULL)
        return;

    float radian = -yobb.rotation*PI/180.0f;
    float cos = std::cos(radian);
    float sin = std::sin(radian);
    glm::vec3 temp;
    output->resize(8);

    // top-left-front
    temp.x = -yobb.radius.x*cos + yobb.radius.z*sin + yobb.centre.x; // -- == +
    temp.y = yobb.radius.y + yobb.centre.y;
    temp.z = -yobb.radius.x*sin - yobb.radius.z*cos + yobb.centre.z;
    output->at(0) = temp;

    // top-right-front
    temp.x = yobb.radius.x*cos + yobb.radius.z*sin + yobb.centre.x; // -- == +
    temp.y = yobb.radius.y + yobb.centre.y;
    temp.z = yobb.radius.x*sin - yobb.radius.z*cos + yobb.centre.z;
    output->at(1) = temp;
    
    // bottom-right-front
    temp.x = yobb.radius.x*cos + yobb.radius.z*sin + yobb.centre.x; // -- == +
    temp.y = -yobb.radius.y + yobb.centre.y;
    temp.z = yobb.radius.x*sin - yobb.radius.z*cos + yobb.centre.z;
    output->at(2) = temp;

    // bottom-left-front
    temp.x = -yobb.radius.x*cos + yobb.radius.z*sin + yobb.centre.x; // -- == +
    temp.y = -yobb.radius.y + yobb.centre.y;
    temp.z = -yobb.radius.x*sin - yobb.radius.z*cos + yobb.centre.z;
    output->at(3) = temp;

    // top-left-back
    temp.x = -yobb.radius.x*cos - yobb.radius.z*sin + yobb.centre.x; // -- == +
    temp.y = yobb.radius.y + yobb.centre.y;
    temp.z = -yobb.radius.x*sin + yobb.radius.z*cos + yobb.centre.z;
    output->at(4) = temp;

    // top-right-back
    temp.x = yobb.radius.x*cos - yobb.radius.z*sin + yobb.centre.x; // -- == +
    temp.y = yobb.radius.y + yobb.centre.y;
    temp.z = yobb.radius.x*sin + yobb.radius.z*cos + yobb.centre.z;
    output->at(5) = temp;

    // bottom-right-back
    temp.x = yobb.radius.x*cos - yobb.radius.z*sin + yobb.centre.x; // -- == +
    temp.y = -yobb.radius.y + yobb.centre.y;
    temp.z = yobb.radius.x*sin + yobb.radius.z*cos + yobb.centre.z;
    output->at(6) = temp;

    // bottom-left-back
    temp.x = -yobb.radius.x*cos - yobb.radius.z*sin + yobb.centre.x; // -- == +
    temp.y = -yobb.radius.y + yobb.centre.y;
    temp.z = -yobb.radius.x*sin + yobb.radius.z*cos + yobb.centre.z;
    output->at(7) = temp;
}
    
void corners2D(const gu::YOBB &yobb, glm::vec3 output[])
{
    float radian = -yobb.rotation*PI/180.0f;
    float cos = std::cos(radian);
    float sin = std::sin(radian);
    glm::vec3 temp;
    
    // top-left-front
    temp.x = -yobb.radius.x*cos + yobb.radius.z*sin + yobb.centre.x; // -- == +
    temp.y = yobb.radius.y + yobb.centre.y;
    temp.z = -yobb.radius.x*sin - yobb.radius.z*cos + yobb.centre.z;
    output[0] = temp;
    
    // top-right-front
    temp.x = yobb.radius.x*cos + yobb.radius.z*sin + yobb.centre.x; // -- == +
    temp.y = yobb.radius.y + yobb.centre.y;
    temp.z = yobb.radius.x*sin - yobb.radius.z*cos + yobb.centre.z;
    output[1] = temp;
    
    // top-left-back
    temp.x = -yobb.radius.x*cos - yobb.radius.z*sin + yobb.centre.x; // -- == +
    temp.y = yobb.radius.y + yobb.centre.y;
    temp.z = -yobb.radius.x*sin + yobb.radius.z*cos + yobb.centre.z;
    output[2] = temp;
    
    // top-right-back
    temp.x = yobb.radius.x*cos - yobb.radius.z*sin + yobb.centre.x; // -- == +
    temp.y = yobb.radius.y + yobb.centre.y;
    temp.z = yobb.radius.x*sin + yobb.radius.z*cos + yobb.centre.z;
    output[3] = temp;
}
    
void normalsXZ(glm::vec3 corners[], glm::vec2 output[])
{
    glm::vec2 top_left;
    glm::vec2 bot_left;
    glm::vec2 bot_right;
    
    top_left.x = corners[0].x;
    top_left.y = corners[0].z;
    
    bot_left.x = corners[2].x;
    bot_left.y = corners[2].z;
    
    bot_right.x = corners[3].x;
    bot_right.y = corners[3].z;
    
    // Find lines
    glm::vec2 left_edge = bot_left - top_left;
    glm::vec2 bottom_edge = bot_right - bot_left;
    
    // Compute normals
    float temp;
    
    temp = left_edge.x;
    left_edge.x = -left_edge.y;
    left_edge.y = temp;
    
    temp = bottom_edge.x;
    bottom_edge.x = -bottom_edge.y;
    bottom_edge.y = temp;
    
    // Normalize
    left_edge = glm::normalize(left_edge);
    bottom_edge = glm::normalize(bottom_edge);
    
    // Write to output
    output[0] = left_edge;
    output[1] = bottom_edge;
}
    
void projectXZ(glm::vec3 corners2D[], const glm::vec2 &axis, float *min, float *max)
{
    *min = FLT_MAX;
    *max = -FLT_MAX;
    
    glm::vec2 temp;
    float dot;
    
    for (int i = 0; i < 4 /*2D CORNERS*/; i++)
    {
        temp.x = corners2D[i].x;
        temp.y = corners2D[i].z;
        dot = glm::dot(axis, temp);
        
        if (dot < *min)
            *min = dot;
        if (dot > *max)
            *max = dot;
    }
}

bool overlaps(float min_A, float max_A, float min_B, float max_B, float *overlap)
{
    // If the min of B is between the values of A
    if (min_A <= min_B && min_B <= max_A)
    {
        if (overlap != NULL)
            *overlap = max_A - min_B;

        return true;
    }

    // If the min of A is between the values of B
    if (min_B <= min_A && min_A <= max_B)
    {
        if (overlap != NULL)
            *overlap = max_B - min_A;

        return true;
    }

    if (overlap != NULL)
        *overlap = 0.0f;

    return false;
}

bool contains(const gu::AABB &A, const gu::AABB &B)
{
    if ((A.centre.x - A.radius.x) < (B.centre.x - B.radius.x)) return false;
    if ((A.centre.x + A.radius.x) > (B.centre.x + B.radius.x)) return false;

    if ((A.centre.y - A.radius.y) < (B.centre.y - B.radius.y)) return false;
    if ((A.centre.y + A.radius.y) > (B.centre.y + B.radius.y)) return false;

    if ((A.centre.z - A.radius.z) < (B.centre.z - B.radius.z)) return false;
    if ((A.centre.z + A.radius.z) > (B.centre.z + B.radius.z)) return false;

    return true;
}

bool containsXZ(const gu::AABB &A, const gu::AABB &B)
{
    if ((A.centre.x - A.radius.x) < (B.centre.x - B.radius.x)) return false;
    if ((A.centre.x + A.radius.x) > (B.centre.x + B.radius.x)) return false;

    if ((A.centre.z - A.radius.z) < (B.centre.z - B.radius.z)) return false;
    if ((A.centre.z + A.radius.z) > (B.centre.z + B.radius.z)) return false;

    return true;
}

bool intersects(const gu::AABB &A, const gu::AABB &B)
{
    if (gu::absval(A.centre.x - B.centre.x) > (A.radius.x + B.radius.x)) return false; 
    if (gu::absval(A.centre.y - B.centre.y) > (A.radius.y + B.radius.y)) return false; 
    if (gu::absval(A.centre.z - B.centre.z) > (A.radius.z + B.radius.z)) return false; 

    return true; 
}

bool intersects(const gu::YOBB &A, const gu::YOBB &B, glm::vec3 *mtv)
{
    const float bump = 0.00001f;

    // If not on the correct hight plane, don't bother checking collisions 
    float y_overlap; 
    if (!overlaps(A.centre.y - A.radius.y, A.centre.y + A.radius.y, B.centre.y - B.radius.y, B.centre.y + B.radius.y, &y_overlap))
        return false;
    
    glm::vec3 corners_A[4];
    glm::vec3 corners_B[4];

    gu::corners2D(A, corners_A);
    gu::corners2D(B, corners_B);

    glm::vec2 axis_A[2];
    glm::vec2 axis_B[2];

    gu::normalsXZ(corners_A, axis_A);
    gu::normalsXZ(corners_B, axis_B);

    float min_overlap = FLT_MAX;
    glm::vec2 min_axis;
    
    float min_A, max_A, min_B, max_B, overlap;

    for (unsigned int i = 0; i < 2 /*AXIS SIZE*/; i++)
    {
        projectXZ(corners_A, axis_A[i], &min_A, &max_A);
        projectXZ(corners_B, axis_A[i], &min_B, &max_B);

        if (!gu::overlaps(min_A, max_A, min_B, max_B, &overlap))
            return false;
        else if (overlap < min_overlap)
        {
            min_overlap = overlap;
            min_axis = axis_A[i];
        }
    }

    for (unsigned int i = 0; i < 2 /*AXIS SIZE*/; i++)
    {
        projectXZ(corners_A, axis_B[i], &min_A, &max_A);
        projectXZ(corners_B, axis_B[i], &min_B, &max_B);

        if (!gu::overlaps(min_A, max_A, min_B, max_B, &overlap))
            return false;
        else if (overlap < min_overlap)
        {
            min_overlap = overlap;
            min_axis = axis_B[i];
        }
    }


    if (mtv != NULL)
    {
        if (y_overlap < min_overlap)
        {
            // A is on top of B
            if (B.centre.y < A.centre.y)
            {
                mtv->x = 0.0f;
                mtv->y = -(y_overlap + bump); // this is reversed because you are suppose
                                              // to subtract the mtv vector
                mtv->z = 0.0f;
            }
            // B is on top of A
            else
            {
                mtv->x = 0.0f;
                mtv->y = y_overlap + bump;
                mtv->z = 0.0f;
            }
        }
        else
        {
            min_axis *= (min_overlap + bump);
            mtv->x = min_axis.x; 
            mtv->y = 0.0f;
            mtv->z = min_axis.y;

            glm::vec3 centre = B.centre - A.centre;
            if (glm::dot(centre, *mtv) < 0)
            {
                mtv->x *= -1;
                mtv->y *= -1;
                mtv->z *= -1;
            }
        }
    }

    return true;
}

// From zacharmarz @ stackexchange.com
// http://gamedev.stackexchange.com/questions/18436/most-efficient-aabb-vs-ray-collision-algorithms 
bool intersects(const glm::vec3 &ray_position, glm::vec3 ray_direction, const gu::AABB &aabb, float *distance)
{
    ray_direction = glm::normalize(ray_direction);

    glm::vec3 frac;
    frac.x = 1.0f / ray_direction.x;
    frac.y = 1.0f / ray_direction.y;
    frac.z = 1.0f / ray_direction.z;

    // lb is the corner of AABB with minimal coordinates - left bottom, rt is maximal corner
    // r.org is origin of ray
    glm::vec3 lb = aabb.centre - aabb.radius;
    glm::vec3 rt = aabb.centre + aabb.radius;

    float t1 = (lb.x - ray_position.x)*frac.x;
    float t2 = (rt.x - ray_position.x)*frac.x;
    float t3 = (lb.y - ray_position.y)*frac.y;
    float t4 = (rt.y - ray_position.y)*frac.y;
    float t5 = (lb.z - ray_position.z)*frac.z;
    float t6 = (rt.z - ray_position.z)*frac.z;

    float tmin = std::max(std::max(std::min(t1, t2), std::min(t3, t4)), std::min(t5, t6));
    float tmax = std::min(std::min(std::max(t1, t2), std::max(t3, t4)), std::max(t5, t6));

    // if tmax < 0, ray (line) is intersecting AABB, but whole AABB is behind us
    if (tmax < 0)
    {
        if (distance != NULL)
            *distance = tmax;

        return false;
    }

    // if tmin > tmax, ray doesn't intersect AABB
    if (tmin > tmax)
    {
        if (distance != NULL)
            *distance = tmax;

        return false;
    }

    if (distance != NULL)
        *distance = tmin;

    return true;
}

// Taken from SFML user Disch
bool intersects(const glm::vec2 &A1, const glm::vec2 &A2, const glm::vec2 &B1, const glm::vec2 &B2, glm::vec2 *intersection)
{
    glm::vec2 a(A2-A1);
    glm::vec2 b(B2-B1);
    glm::vec2 c(B2-A2);

    float f = (a.y * b.x) - (a.x * b.y);
    if (f == 0)
        return false;

    float aa = (a.y * c.x) - (a.x * c.y);
    float bb = (b.y * c.x) - (b.x * c.y);

    if (f < 0)
    {
        if (aa > 0) return false;
        if (bb > 0) return false;
        if (aa < f) return false;
        if (bb < f) return false;
    }
    else
    {
        if (aa < 0) return false;
        if (bb < 0) return false;
        if (aa > f) return false;
        if (bb > f) return false;
    }

    if (intersection != NULL)
        *intersection = b * (1.0f - (aa / f)) + B1;

    return true;
}

bool printOpenGLErrors(std::string output_prefix, bool output_on_no_error)
{
    bool result = false;

    while (true)
    {
        GLenum error = glGetError();

        if (error == GL_NO_ERROR)
        {
            if (!result && output_on_no_error)
                SDL_Log("[GUMI] %sGL_NO_ERROR", output_prefix.c_str());

            return result;
        }

        result = true;

        switch (error)
        {
            case GL_INVALID_ENUM: output_prefix += "GL_INVALID_ENUM"; break;
            case GL_INVALID_VALUE: output_prefix += "GL_INVALID_VALUE"; break;
            case GL_INVALID_OPERATION: output_prefix += "GL_INVALID_OPERATION"; break;
            case GL_INVALID_FRAMEBUFFER_OPERATION: output_prefix += "GL_INVALID_FRAMEBUFFER_OPERATION"; break;
            case GL_OUT_OF_MEMORY: output_prefix += "GL_OUT_OF_MEMORY"; break;
            default: output_prefix += "GL_UNKNOWN_ERROR"; break;
        }

        SDL_Log("[GUMI] %s", output_prefix.c_str());
    }
}

std::string getOpenGLErrors(const std::string &output_prefix, bool output_on_no_error)
{
    std::string result;
    
    while (true)
    {
        GLenum error = glGetError();
        
        if (error == GL_NO_ERROR)
        {
            if (result.empty() && output_on_no_error)
                result = output_prefix + "GL_NO_ERROR";
            
            return result;
        }
        
        result = true;
        
        switch (error)
        {
            case GL_INVALID_ENUM: result = output_prefix + "GL_INVALID_ENUM"; break;
            case GL_INVALID_VALUE: result = output_prefix + "GL_INVALID_VALUE"; break;
            case GL_INVALID_OPERATION: result = output_prefix + "GL_INVALID_OPERATION"; break;
            case GL_INVALID_FRAMEBUFFER_OPERATION: result = output_prefix + "GL_INVALID_FRAMEBUFFER_OPERATION"; break;
            case GL_OUT_OF_MEMORY: result = output_prefix + "GL_OUT_OF_MEMORY"; break;
            default: result = output_prefix + "GL_UNKNOWN_ERROR";
        }
    }
    
    return result;
}

gu::AABB toAABB(const gu::YOBB &yobb)
{
    gu::AABB temp;
    temp.centre = yobb.centre;

    glm::vec3 corners[4];
    gu::corners2D(yobb, corners);

    float min_x = FLT_MAX;
    float max_x = -FLT_MAX;
    float min_z = FLT_MAX;
    float max_z = -FLT_MAX;

    for (unsigned int i = 0; i < 4; i++)
    {
        if (corners[i].x < min_x)
            min_x = corners[i].x;

        if (corners[i].x > max_x)
            max_x = corners[i].x;

        if (corners[i].z < min_z)
            min_z = corners[i].z;

        if (corners[i].z > max_z)
            max_z = corners[i].z;
    }

    min_x -= yobb.centre.x;
    max_x -= yobb.centre.x;
    min_z -= yobb.centre.z;
    max_z -= yobb.centre.z;

    temp.radius.x = (max_x - min_x) / 2.0f;
    temp.radius.y = yobb.radius.y;
    temp.radius.z = (max_z - min_z) / 2.0f;

    return temp;
}

gu::YOBB toYOBB(const gu::AABB &aabb)
{
    return gu::YOBB(aabb.centre, aabb.radius, 0.0f);
}

const glm::ivec2& getRenderSize()
{
    return g_engine->getRenderSize();
}

}
