#include "GUMI/Armature.h"

namespace gu
{

Armature::Armature() :
    root_(NULL),
    bone_ids_(NULL),
    bone_weights_(NULL),
    number_of_meshes_(0),
    standard_channel_size_(0),
    error_displayed_(false),
    remove_blend_duration_(0.0f),
    remove_blend_progress_(0.0f),
    remove_blend_animation_(NULL)
{
    loaded_animations_[0] = NULL;
    blending_[0] = false;
    blend_duration_[0] = 0.0f;
    blend_progress_[0] = 0.0f;

    loaded_animations_[1] = NULL;
    blending_[1] = false;
    blend_duration_[1] = 0.0f;
    blend_progress_[1] = 0.0f;
}

Armature::Armature(const Armature &obj) :
    number_of_meshes_(obj.number_of_meshes_),
    standard_channel_size_(0),
    error_displayed_(false),
    remove_blend_duration_(0.0f),
    remove_blend_progress_(0.0f),
    remove_blend_animation_(NULL)
{
    root_ = copyBoneTree(obj.root_, NULL);

    bone_ids_ = new std::vector<glm::ivec4>[number_of_meshes_];
    for (unsigned int i = 0; i < number_of_meshes_; i++)
        bone_ids_[i] = obj.bone_ids_[i];

    bone_weights_ = new std::vector<glm::vec4>[number_of_meshes_];
    for (unsigned int i = 0; i < number_of_meshes_; i++)
        bone_weights_[i] = obj.bone_weights_[i];

    loaded_animations_[0] = NULL;
    blending_[0] = false;
    blend_duration_[0] = 0.0f;
    blend_progress_[0] = 0.0f;

    loaded_animations_[1] = NULL;
    blending_[1] = false;
    blend_duration_[1] = 0.0f;
    blend_progress_[1] = 0.0f;
}

Armature::~Armature()
{
    deconstruct();
}

Armature& Armature::operator=(const Armature &obj)
{
    deconstruct();

    root_ = copyBoneTree(obj.root_, NULL);
    number_of_meshes_ = obj.number_of_meshes_;

    bone_ids_ = new std::vector<glm::ivec4>[number_of_meshes_];
    for (unsigned int i = 0; i < number_of_meshes_; i++)
        bone_ids_[i] = obj.bone_ids_[i];

    bone_weights_ = new std::vector<glm::vec4>[number_of_meshes_];
    for (unsigned int i = 0; i < number_of_meshes_; i++)
        bone_weights_[i] = obj.bone_weights_[i];

    standard_channel_size_ = 0;
    error_displayed_ = false;

    loaded_animations_[0] = NULL;
    blending_[0] = false;
    blend_duration_[0] = 0.0f;
    blend_progress_[0] = 0.0f;

    loaded_animations_[1] = NULL;
    blending_[1] = false;
    blend_duration_[1] = 0.0f;
    blend_progress_[1] = 0.0f;

    remove_blend_animation_ = NULL;
    remove_blend_duration_ = 0.0f;
    remove_blend_progress_ = 0.0f;

    return *this;
}

Bone* Armature::copyBoneTree(Bone *start, Bone *copy_parent)
{
    if (start == NULL)
        return NULL;

    Bone *copy = new Bone(start->name, copy_parent);
    copy->offset = start->offset;
    copy->transform = start->transform;
    copy->world_transform = start->world_transform;

    bones_.push_back(copy);

    for (unsigned int i = 0; i < start->children.size(); i++)
    {
        copy->children.push_back(copyBoneTree(start->children[i], copy));
    }

    return copy;
}

void Armature::init()
{
    root_ = NULL;
    bone_ids_ = NULL;
    bone_weights_ = NULL;
    standard_channel_size_ = 0;
    error_displayed_ = false;

    loaded_animations_[0] = NULL;
    blending_[0] = false;
    blend_duration_[0] = 0.0f;
    blend_progress_[0] = 0.0f;

    loaded_animations_[1] = NULL;
    blending_[1] = false;
    blend_duration_[1] = 0.0f;
    blend_progress_[1] = 0.0f;

    remove_blend_duration_ = 0.0f;
    remove_blend_progress_ = 0.0f;
    remove_blend_animation_ = NULL;
}

void Armature::deconstruct()
{
    if (root_ != NULL)
        for (unsigned int i = 0; i < bones_.size(); i++)
            delete bones_[i];

    bones_.clear();

    if (bone_ids_ != NULL)
    {
        delete[] bone_ids_;
        bone_ids_ = NULL;
    }

    if (bone_weights_ != NULL)
    {
        delete[] bone_weights_;
        bone_weights_ = NULL;
    }
}

bool Armature::loadFileArmature(const std::string &file_path)
{
    if (gu::Zip::getRedirectFileLoading())
        return loadZipArmature(file_path);

    Assimp::Importer importer;

    std::string file_extension = file_path.substr(file_path.find_last_of("."));

    importer.SetPropertyInteger(AI_CONFIG_PP_RVC_FLAGS, aiComponent_TANGENTS_AND_BITANGENTS | aiComponent_COLORS | aiComponent_TEXCOORDS | aiComponent_TEXTURES | aiComponent_LIGHTS | aiComponent_CAMERAS | aiComponent_MATERIALS);

#if defined(TARGET_OS_IPHONE) || defined(__ANDROID__)

    SDL_RWops *rw = SDL_RWFromFile(file_path.c_str(), "rb");
    Sint64 size = rw->size(rw);
    char *buffer = new char[size];
    rw->read(rw, buffer, size, 1);

    const aiScene *scene = importer.ReadFileFromMemory(buffer, size, aiProcess_Triangulate
            | aiProcess_RemoveComponent 
            | aiProcess_JoinIdenticalVertices  
            | aiProcess_ImproveCacheLocality
            | aiProcess_SplitLargeMeshes 
            | aiProcess_FindDegenerates 
            | aiProcess_FlipUVs 
            | aiProcess_LimitBoneWeights 
            | aiProcess_RemoveRedundantMaterials  
            | aiProcess_SortByPType  
            | aiProcess_FindInvalidData
            | aiProcess_FindInstances 
            | aiProcess_OptimizeMeshes  
            | aiProcess_OptimizeGraph, file_extension.c_str());

    delete[] buffer;
    SDL_FreeRW(rw);
    
    if (scene == NULL)
    {
        SDL_Log("[GUMI] (Armature) Error: Failed to load armature: \"%s\"", file_path.c_str());
        return false;
    }
    
    return loadBones(scene);

#else

    const aiScene *scene = importer.ReadFile(file_path.c_str(), aiProcess_Triangulate
            | aiProcess_RemoveComponent 
            | aiProcess_JoinIdenticalVertices  
            | aiProcess_ImproveCacheLocality
            | aiProcess_SplitLargeMeshes 
            | aiProcess_FindDegenerates 
            | aiProcess_FlipUVs 
            | aiProcess_LimitBoneWeights 
            | aiProcess_RemoveRedundantMaterials  
            | aiProcess_SortByPType  
            | aiProcess_FindInvalidData
            | aiProcess_FindInstances 
            | aiProcess_OptimizeMeshes  
            | aiProcess_OptimizeGraph);
            
    if (scene == NULL)
    {
        SDL_Log("[GUMI] (Armature) Error: Failed to load armature: \"%s\"", file_path.c_str());
        return false;
    }
    
    return loadBones(scene);

#endif

}

bool Armature::loadZipArmature(const std::string &zip_path)
{
    Assimp::Importer importer;

    std::string file_extension = zip_path.substr(zip_path.find_last_of("."));

    importer.SetPropertyInteger(AI_CONFIG_PP_RVC_FLAGS, aiComponent_TANGENTS_AND_BITANGENTS | aiComponent_COLORS | aiComponent_TEXCOORDS | aiComponent_TEXTURES | aiComponent_LIGHTS | aiComponent_CAMERAS | aiComponent_MATERIALS);

#if defined(TARGET_OS_IPHONE) || defined(__ANDROID__)

    SDL_RWops *rw = SDL_RWFromFile(zip_path.c_str(), "rb");
    Sint64 size = rw->size(rw);
    char *buffer = new char[size];
    rw->read(rw, buffer, size, 1);

    const aiScene *scene = importer.ReadFileFromMemory(buffer, size, aiProcess_Triangulate
            | aiProcess_RemoveComponent 
            | aiProcess_JoinIdenticalVertices  
            | aiProcess_ImproveCacheLocality
            | aiProcess_SplitLargeMeshes 
            | aiProcess_FindDegenerates 
            | aiProcess_FlipUVs 
            | aiProcess_LimitBoneWeights 
            | aiProcess_RemoveRedundantMaterials  
            | aiProcess_SortByPType  
            | aiProcess_FindInvalidData
            | aiProcess_FindInstances 
            | aiProcess_OptimizeMeshes  
            | aiProcess_OptimizeGraph, file_extension.c_str());

    delete[] buffer;
    SDL_FreeRW(rw);
    
    if (scene == NULL)
    {
        SDL_Log("[GUMI] (Armature) Error: Failed to load armature: \"%s\"", zip_path.c_str());
        return false;
    }
    
    return loadBones(scene);

#else

    gu::Zip zip(zip_path);

    const aiScene *scene = importer.ReadFileFromMemory(zip.getBuffer(), zip.getSize(), aiProcess_Triangulate
            | aiProcess_RemoveComponent 
            | aiProcess_JoinIdenticalVertices  
            | aiProcess_ImproveCacheLocality
            | aiProcess_SplitLargeMeshes 
            | aiProcess_FindDegenerates 
            | aiProcess_FlipUVs 
            | aiProcess_LimitBoneWeights 
            | aiProcess_RemoveRedundantMaterials  
            | aiProcess_SortByPType  
            | aiProcess_FindInvalidData
            | aiProcess_FindInstances 
            | aiProcess_OptimizeMeshes  
            | aiProcess_OptimizeGraph, file_extension.c_str());

    free(zip.getBuffer());
    
    if (scene == NULL)
    {
        SDL_Log("[GUMI] (Armature) Error: Failed to load armature: \"%s\"", zip_path.c_str());
        return false;
    }
    
    return loadBones(scene);

#endif

}

void copyMatrix(const aiMatrix4x4 &src, glm::mat4 &dest);

bool Armature::loadBones(const aiScene *scene)
{
    // Check if there are bones in the scene
    if (!scene->HasMeshes())
        return false;

    bool has_bones = false;
    for (unsigned int i = 0; i < scene->mNumMeshes; i++)
    {
        if (scene->mMeshes[i]->HasBones())
        {
            has_bones = true;
            break;
        }
    }

    if (!has_bones)
        return false;

    // Load the bone heirarchy
    root_ = loadBoneTree(scene->mRootNode, NULL);

    if (root_ == NULL)
        return false;

    // Load offset matrices from mesh to bone structure
    for (unsigned int i = 0; i < scene->mNumMeshes; i++)
    {
        const aiMesh *mesh = scene->mMeshes[i];

        for (unsigned int j = 0; j < mesh->mNumBones; j++)
        {
            const aiBone *bone = mesh->mBones[j];

            for (unsigned int k = 0; k < bones_.size(); k++)
                if (bones_[k]->name.compare(bone->mName.data) == 0)
                    copyMatrix(bone->mOffsetMatrix, bones_[k]->offset);
        }
    }

    assignVertices(scene);

    return true;
}

void Armature::unload()
{
    deconstruct();

    bones_.clear();
    transforms_.clear();

    last_interpolated_rotation_.clear();
    last_interpolated_position_.clear();
    last_interpolated_scaling_.clear();

    last_blend_interpolated_rotation_.clear();
    last_blend_interpolated_position_.clear();
    last_blend_interpolated_scaling_.clear();

    storage_interpolated_rotation_.clear();
    storage_interpolated_position_.clear();
    storage_interpolated_scaling_.clear();

    remove_blend_channels_.clear();
    remove_blend_rotation_.clear();
    remove_blend_position_.clear();
    remove_blend_scaling_.clear();

    init();
}

void copyMatrix(const aiMatrix4x4 &src, glm::mat4 &dest)
{
    // First column
    dest[0][0] = src.a1;
    dest[0][1] = src.b1;
    dest[0][2] = src.c1;
    dest[0][3] = src.d1;

    // Second column
    dest[1][0] = src.a2;
    dest[1][1] = src.b2;
    dest[1][2] = src.c2;
    dest[1][3] = src.d2;

    // Third column
    dest[2][0] = src.a3;
    dest[2][1] = src.b3;
    dest[2][2] = src.c3;
    dest[2][3] = src.d3;

    // Fourth column
    dest[3][0] = src.a4;
    dest[3][1] = src.b4;
    dest[3][2] = src.c4;
    dest[3][3] = src.d4;
}

void outputMatrix(const glm::mat4 &m)
{
    std::cout << m[0][0] << "," << m[1][0] << "," << m[2][0] << "," << m[3][0] << std::endl;
    std::cout << m[0][1] << "," << m[1][1] << "," << m[2][1] << "," << m[3][1] << std::endl;
    std::cout << m[0][2] << "," << m[1][2] << "," << m[2][2] << "," << m[3][2] << std::endl;
    std::cout << m[0][3] << "," << m[1][3] << "," << m[2][3] << "," << m[3][3] << std::endl;
}

void Armature::loadAnimation(gu::Animation *anim, float blend_time, gu::AnimationLayer index)
{
    if (standard_channel_size_ == 0)
    {
        standard_channel_size_ = anim->getChannels().size();

        // Should only need 1 resize because the number of bones shouldn't change.
        // If it does, it's reported to console below. 
        last_interpolated_rotation_.resize(anim->getChannels().size());
        last_interpolated_position_.resize(anim->getChannels().size());
        last_interpolated_scaling_.resize(anim->getChannels().size());

        last_blend_interpolated_rotation_.resize(anim->getChannels().size());
        last_blend_interpolated_position_.resize(anim->getChannels().size());
        last_blend_interpolated_scaling_.resize(anim->getChannels().size());

        storage_interpolated_rotation_.resize(anim->getChannels().size());
        storage_interpolated_position_.resize(anim->getChannels().size());
        storage_interpolated_scaling_.resize(anim->getChannels().size());
    }

    if (!error_displayed_ && anim->getChannels().size() != standard_channel_size_)
    {
        SDL_Log("[GUMI] (Armature) Error: Channel (Bone) size does not match the armature standard");
        error_displayed_ = true;
    }

    if (blend_time > 0.0f)
    {
        gu::Animation *blend_from_anim = loaded_animations_[index];
        bool force = false;

        // Force the loading of interpolated blend frames & the remove animation
        // if the armature was removing an overload before hand
        if (index == 1 && !remove_blend_channels_.empty())
        {
            blend_from_anim = remove_blend_animation_;
            force = true;
        }

        // Animation was interupted in a blend sequence
        if ((blend_from_anim != NULL && blending_[index]) || force)
        {
            // Only apply blending for channels with are valid
            for (unsigned int i = 0; i < blend_from_anim->getChannels().size(); i++)
            {
                if (blend_from_anim->getChannels().at(i).valid)
                {
                    last_interpolated_rotation_[i] = last_blend_interpolated_rotation_[i];
                    last_interpolated_position_[i] = last_blend_interpolated_position_[i];
                    last_interpolated_scaling_[i] = last_blend_interpolated_scaling_[i];
                }
            }
        }

        // Save any old animation channels
        if (index == 0)
        {
            unsigned int frame, last_frame;
            float factor;

            loaded_animations_[index]->sample(&frame, &last_frame, &factor);

            for (unsigned int i = 0; i < loaded_animations_[index]->getChannels().size(); i++)
            {
                const Channel &channel = loaded_animations_[index]->getChannels().at(i);

                aiQuaternion i_rotation;

                aiQuaternion::Interpolate(i_rotation, channel.rotations[last_frame].mValue, channel.rotations[frame].mValue, factor);
                aiVector3D i_position = factor * channel.positions[frame].mValue + (1.0f - factor) * channel.positions[last_frame].mValue;
                aiVector3D i_scaling = factor * channel.scalings[frame].mValue + (1.0f - factor) * channel.scalings[last_frame].mValue;

                storage_interpolated_rotation_[i] = i_rotation;
                storage_interpolated_position_[i] = i_position;
                storage_interpolated_scaling_[i] = i_scaling;
            }
        }

        blending_[index] = true;
        blend_duration_[index] = blend_time;
    }
    else
    {
        blending_[index] = false;
        blend_duration_[index] = 0.0f;
    }

    blend_progress_[index] = 0.0f;
    loaded_animations_[index] = anim;
    loaded_animations_[index]->restart();

    // If loading a new overload animation, clear remove_blend because
    // it's no longer needed and will falsely trigger remove blending
    if (index == 1)
    {
        remove_blend_duration_ = 0.0f;
        remove_blend_progress_ = 0.0f;
        remove_blend_channels_.clear();
        remove_blend_rotation_.clear();
        remove_blend_position_.clear();
        remove_blend_scaling_.clear();
    }
}

void Armature::removeOverloadedAnimation(float blend_time)
{
    const gu::AnimationLayer index = ANIM_OVERLOAD;

    if (loaded_animations_[index] == NULL)
        return;

    // Determine channels to "remove blend"
    remove_blend_duration_ = blend_time;
    remove_blend_progress_ = 0.0f;
    remove_blend_channels_.clear();
    remove_blend_rotation_.clear();
    remove_blend_position_.clear();
    remove_blend_scaling_.clear();

    // Determine what frame & interpolation this removed animation is on, and save the transformations for it
    unsigned int frame, last_frame;
    float factor;
    loaded_animations_[index]->sample(&frame, &last_frame, &factor);

    // For every bone
    for (unsigned int i = 0; i < loaded_animations_[index]->getChannels().size(); i++)
    {
        const Channel &channel = loaded_animations_[index]->getChannels().at(i);

        // If the bone is valid
        if (channel.valid)
        {
            // Go to the last frame of the bone and calculate interpolation.
            // Save interpolation.

            remove_blend_channels_.push_back(i);
            aiQuaternion i_rotation;

            aiQuaternion::Interpolate(i_rotation, channel.rotations[last_frame].mValue, channel.rotations[frame].mValue, factor);
            aiVector3D i_position = factor * channel.positions[frame].mValue + (1.0f - factor) * channel.positions[last_frame].mValue;
            aiVector3D i_scaling = factor * channel.scalings[frame].mValue + (1.0f - factor) * channel.scalings[last_frame].mValue;

            remove_blend_rotation_.push_back(i_rotation);
            remove_blend_position_.push_back(i_position);
            remove_blend_scaling_.push_back(i_scaling);
        }
    }

    remove_blend_animation_ = loaded_animations_[index];
    loaded_animations_[index] = NULL;
    blending_[index] = false;
}

bool Armature::isAnimating(const gu::Animation *anim, gu::AnimationLayer index) const
{
    return anim == loaded_animations_[index];
}

// Getters

gu::Animation* Armature::getLoadedAnimation(gu::AnimationLayer index)
{
    return loaded_animations_[index];
}

void Armature::update(const float ft)
{
    if (root_ == NULL)
        return;

    // Process "remove" blend animations
    if (!remove_blend_channels_.empty())
    {
        remove_blend_progress_ += ft;
        
        if (remove_blend_progress_ >= remove_blend_duration_)
        {
            remove_blend_progress_ = 0.0f;
            remove_blend_duration_ = 0.0f;
            remove_blend_channels_.clear();
        }
    }

    processAnimation(ANIM_BASE, ft);
    processAnimation(ANIM_OVERLOAD, ft);

    recursiveUpdate(root_);

    transforms_.resize(bones_.size());
    for (unsigned int i = 0; i < bones_.size(); i++)
    {
        transforms_[i] = bones_[i]->world_transform * bones_[i]->offset;
    }
}

void Armature::processAnimation(gu::AnimationLayer index, const float ft)
{
    if (loaded_animations_[index] == NULL)
        return;

    // Determine animation information
    if (!blending_[index])
        loaded_animations_[index]->tick(ft);

    unsigned int frame, last_frame;
    float alpha;
    loaded_animations_[index]->sample(&frame, &last_frame, &alpha);

    // std::cout << frame << " " << last_frame << " " << alpha << std::endl;

    // Process blend animations
    if (blending_[index])
    {
        blend_progress_[index] += ft;

        if (blend_progress_[index] >= blend_duration_[index])
        {
            blending_[index] = false;
            blend_duration_[index] = 0.0f;
            blend_progress_[index] = 0.0f;
        }
    }

    // Apply channels to bones
    for (unsigned int i = 0; i < loaded_animations_[index]->getChannels().size(); i++)
    {
        const Channel &channel = loaded_animations_[index]->getChannels().at(i);
        Bone *bone = NULL;

        if (!channel.valid)
            continue;

        for (unsigned int j = 0; j < bones_.size(); j++)
        {
            if (channel.name.compare(bones_[j]->name) == 0)
            {
                bone = bones_[j];
                break;
            }
        }

        if (bone != NULL)
        {
            // Interpolation
            aiQuaternion i_rotation;
            aiVector3D i_position;
            aiVector3D i_scaling;

            int remove_blend_index = -1;

            // Determine if the "remove blend" data applies to this channel
            if (!remove_blend_channels_.empty())
            {
                for (unsigned int j = 0; j < remove_blend_channels_.size(); j++)
                {
                    if (static_cast<int>(i) == remove_blend_channels_[j])
                    {
                        remove_blend_index = j;
                        break;
                    }
                }
            }

            if (remove_blend_index == -1)
            {
                if (!blending_[index])
                {
                    aiQuaternion::Interpolate(i_rotation, channel.rotations[last_frame].mValue, channel.rotations[frame].mValue, alpha);
                    i_position = alpha * channel.positions[frame].mValue + (1.0f - alpha) * channel.positions[last_frame].mValue;
                    i_scaling = alpha * channel.scalings[frame].mValue + (1.0f - alpha) * channel.scalings[last_frame].mValue;

                    // Save last position for blending
                    
                    // If the index is 0, and there is no loaded animation being overwritten or its not valid, 
                    // then save the position for interpolation
                    //
                    // If index is 1, then always save the animation because it'll only run here if valid
                    if (index == 1 || loaded_animations_[1] == NULL || !loaded_animations_[1]->getChannels().at(i).valid)
                    {
                        last_interpolated_rotation_[i] = i_rotation;
                        last_interpolated_position_[i] = i_position;
                        last_interpolated_scaling_[i] = i_scaling;
                    }
                }
                // Blend animations
                else
                {
                    float blend_alpha = blend_progress_[index] / blend_duration_[index];

                    // Interpolate the last saved position and the first frame of the current animation
                    aiQuaternion::Interpolate(i_rotation, last_interpolated_rotation_[i], channel.rotations[0].mValue, blend_alpha);
                    i_position = blend_alpha * channel.positions[0].mValue + (1.0f - blend_alpha) * last_interpolated_position_[i];
                    i_scaling = blend_alpha * channel.scalings[0].mValue + (1.0f - blend_alpha) * last_interpolated_scaling_[i];

                    // Save last position for blending
                   
                    if (index == 1 || loaded_animations_[1] == NULL || !loaded_animations_[1]->getChannels().at(i).valid)
                    {
                        last_blend_interpolated_rotation_[i] = i_rotation;
                        last_blend_interpolated_position_[i] = i_position;
                        last_blend_interpolated_scaling_[i] = i_scaling;
                    }
                }
            }
            // Process Remove Blend
            else // runs on index 0
            {
                // Calculate The Remove Blend Values
                float remove_blend_alpha = remove_blend_progress_ / remove_blend_duration_;
                
                if (!blending_[0])
                {
                    aiQuaternion::Interpolate(i_rotation, remove_blend_rotation_[remove_blend_index], channel.rotations[frame].mValue, remove_blend_alpha);
                    i_position = remove_blend_alpha * channel.positions[frame].mValue + (1.0f - remove_blend_alpha) * remove_blend_position_[remove_blend_index];
                    i_scaling = remove_blend_alpha * channel.scalings[frame].mValue + (1.0f - remove_blend_alpha) * remove_blend_scaling_[remove_blend_index];
                }
                else
                {
                    float blend_alpha = blend_progress_[index] / blend_duration_[index];

                    aiQuaternion main_rotation;
                    aiVector3D main_position;
                    aiVector3D main_scaling;

                    // Interpolate the last saved position and the first frame of the current animation
                    aiQuaternion::Interpolate(main_rotation, storage_interpolated_rotation_[i], channel.rotations[0].mValue, blend_alpha);
                    main_position = blend_alpha * channel.positions[0].mValue + (1.0f - blend_alpha) * storage_interpolated_position_[i];
                    main_scaling = blend_alpha * channel.scalings[0].mValue + (1.0f - blend_alpha) * storage_interpolated_scaling_[i];

                    last_interpolated_rotation_[i] = main_rotation;
                    last_interpolated_position_[i] = main_position;
                    last_interpolated_scaling_[i] = main_scaling;

                    // Calculate new remove blend based on blend position
                    aiQuaternion::Interpolate(i_rotation, remove_blend_rotation_[remove_blend_index], main_rotation, remove_blend_alpha);
                    i_position = remove_blend_alpha * main_position + (1.0f - remove_blend_alpha) * remove_blend_position_[remove_blend_index];
                    i_scaling = remove_blend_alpha * main_scaling + (1.0f - remove_blend_alpha) * remove_blend_scaling_[remove_blend_index];
                }

                // Save last position for blending
                last_blend_interpolated_rotation_[i] = i_rotation;
                last_blend_interpolated_position_[i] = i_position;
                last_blend_interpolated_scaling_[i] = i_scaling;
            }

            glm::mat4 matrix;

            // Rotation
            aiMatrix4x4 rotation = aiMatrix4x4(i_rotation.GetMatrix());
            copyMatrix(rotation, matrix);

            // Position
            matrix[3][0] = i_position.x;
            matrix[3][1] = i_position.y;
            matrix[3][2] = i_position.z;

            // Scaling
            matrix[0][0] *= i_scaling.x;
            matrix[0][1] *= i_scaling.x;
            matrix[0][2] *= i_scaling.x;

            matrix[1][0] *= i_scaling.y;
            matrix[1][1] *= i_scaling.y;
            matrix[1][2] *= i_scaling.y;

            matrix[2][0] *= i_scaling.z;
            matrix[2][1] *= i_scaling.z;
            matrix[2][2] *= i_scaling.z;

            bone->transform = matrix;
        }
    }
}

void Armature::recursiveUpdate(Bone *bone)
{
    for (unsigned int i = 0; i < bone->children.size(); i++)
    {
        calculateBoneWorldTransforms(bone->children.at(i));
        recursiveUpdate(bone->children.at(i));
    }
}

Bone* Armature::loadBoneTree(const aiNode *node, Bone *parent)
{
    std::string name(node->mName.data);

    // Optimization: Blender exporting seems to include the Scene as a node
    // This is worthless, so we (hope) that there's a bone called "Armature"
    if (name.compare("Scene") == 0)
    {
        std::string child_name;
        for (unsigned int i = 0; i < node->mNumChildren; i++)
        {
            child_name = node->mChildren[i]->mName.data;
            if (child_name.compare("Armature") == 0)
                return loadBoneTree(node->mChildren[i], NULL);
        }

        SDL_Log("[GUMI] (Armature) Error: Could not find object \"Armature\" in scene");
        return NULL;
    }

    // Get name & parent of bone
    Bone *bone = new Bone(name, parent);
    bones_.push_back(bone); 
    
    // Move the aiMatrix4x4 transformation matrix to a glm::mat4 
    // this is just initialization, in case it doesn't get re-updated when we read channels
    copyMatrix(node->mTransformation, bone->transform);

    // Calculate world transform for bone
    calculateBoneWorldTransforms(bone);

    // Calculate world coordinate of bone
    for (unsigned int i = 0; i < node->mNumChildren; i++)
    {
        Bone *child = loadBoneTree(node->mChildren[i], bone); 
        bone->children.push_back(child);
    }

    return bone;
}

void Armature::calculateBoneWorldTransforms(Bone *bone)
{
    // Initialize to the transform matrix
    bone->world_transform = bone->transform;

    // Update the transform based on the parent bones it's connected to
    Bone *p_bone = bone->parent;
    while (p_bone != NULL)
    {
        bone->world_transform = p_bone->transform * bone->world_transform;
        p_bone = p_bone->parent;
    }
}

void Armature::assignVertices(const aiScene *scene)
{
    number_of_meshes_ = scene->mNumMeshes;

    bone_ids_ = new std::vector<glm::ivec4>[scene->mNumMeshes];
    bone_weights_ = new std::vector<glm::vec4>[scene->mNumMeshes];

    for (unsigned int i = 0; i < scene->mNumMeshes; i++)
    {
        const aiMesh *mesh = scene->mMeshes[i];

        bone_ids_[i].resize(mesh->mNumVertices);
        for (unsigned int j = 0; j < mesh->mNumVertices; j++)
            bone_ids_[i].at(j) = glm::ivec4(-1,-1,-1,-1);

        bone_weights_[i].resize(mesh->mNumVertices);
        for (unsigned int j = 0; j < mesh->mNumVertices; j++)
            bone_weights_[i].at(j) = glm::vec4(0.0f,0.0f,0.0f,0.0f);

        for (unsigned int j = 0; j < mesh->mNumBones; j++)
        {
            const aiBone *bone = mesh->mBones[j];

            int bone_index = -1;
            for (unsigned int k = 0; k < bones_.size(); k++)
                if (bones_[k]->name.compare(bone->mName.data) == 0)
                {
                    bone_index = k;
                    break;
                }

            if (bone_index == -1)
                continue;

            for (unsigned int k = 0; k < bone->mNumWeights; k++)
            {
                unsigned int vertex_index = bone->mWeights[k].mVertexId;
                float weight = bone->mWeights[k].mWeight;

                if (bone_ids_[i].at(vertex_index).x == -1)      
                {
                    bone_ids_[i].at(vertex_index).x = bone_index;
                    bone_weights_[i].at(vertex_index).x = weight;
                }
                else if (bone_ids_[i].at(vertex_index).y == -1) 
                {
                    bone_ids_[i].at(vertex_index).y = bone_index;
                    bone_weights_[i].at(vertex_index).y = weight;
                }
                else if (bone_ids_[i].at(vertex_index).z == -1) 
                {
                    bone_ids_[i].at(vertex_index).z = bone_index;
                    bone_weights_[i].at(vertex_index).z = weight;
                }
                else if (bone_ids_[i].at(vertex_index).w == -1)
                {
                    bone_ids_[i].at(vertex_index).w = bone_index;
                    bone_weights_[i].at(vertex_index).w = weight;
                }
            }
        }
    }

    // Reset indices to 0 if they weren't set. They will be canceled out by 0 weight in the shader.
    // This is to prevent any unintended results with refering to a -1 index for the bone array on the vertex shader
    for (unsigned int i = 0; i < scene->mNumMeshes; i++)
    {
        for (unsigned int j = 0; j < scene->mMeshes[i]->mNumVertices; j++)
        {
            if (bone_ids_[i].at(j).x == -1) bone_ids_[i].at(j).x = 0;
            if (bone_ids_[i].at(j).y == -1) bone_ids_[i].at(j).y = 0;
            if (bone_ids_[i].at(j).z == -1) bone_ids_[i].at(j).z = 0;
            if (bone_ids_[i].at(j).w == -1) bone_ids_[i].at(j).w = 0;
        }
    }
}

const std::vector<glm::mat4>* Armature::getTransforms() const
{
    return &transforms_;
}

const std::vector<Bone*>* Armature::getBones() const
{
    return &bones_;
}

const std::vector<glm::ivec4>* Armature::getBoneIDs(unsigned int mesh_id) const
{
    return &bone_ids_[mesh_id];
}

const std::vector<glm::vec4>* Armature::getBoneWeights(unsigned int mesh_id) const
{
    return &bone_weights_[mesh_id];
}

}
