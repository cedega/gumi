#include "GUMI/Shader.h"

namespace gu
{

int Shader::opengl_major_ = 0;
GLuint Shader::bound_shader_ = -1;

Shader *Shader::SHADER_3D = NULL;
Shader *Shader::SHADER_3D_RIG = NULL;
Shader *Shader::SHADER_2D = NULL;
Shader *Shader::SHADER_2D_BILLBOARD = NULL;
Shader *Shader::SHADER_PS = NULL;
Shader *Shader::SHADER_PS_TEXTURE = NULL;
Shader *Shader::SHADER_UI = NULL;

Shader::Shader() :
    loaded_(false)
{
}

Shader::~Shader()
{
    deconstruct();
}

void Shader::deconstruct()
{
    if (loaded_)
    {
        glDeleteProgram(program_);
        glDeleteShader(vertex_shader_);
        glDeleteShader(fragment_shader_);
    }

    loaded_ = false;
}

bool Shader::compile(std::string vertex, std::string fragment, const std::string &version, const std::string &vertex_file_name, const std::string &fragment_file_name)
{
    vertex = "#version " + version + "\n" + vertex;
    fragment = "#version " + version + "\n" + fragment;
    return compileShaders(vertex.c_str(), fragment.c_str(), vertex_file_name, fragment_file_name);
}

bool Shader::loadFileShader(const std::string &vertex_file, const std::string &fragment_file, const std::string &version)
{
    if (gu::Zip::getRedirectFileLoading())
        return loadZipShader(vertex_file, fragment_file, version);
    
    deconstruct();

#if defined(TARGET_OS_IPHONE) || defined(__ANDROID__)

    SDL_RWops *vrw = SDL_RWFromFile(vertex_file.c_str(), "r");
    SDL_RWops *frw = SDL_RWFromFile(fragment_file.c_str(), "r");

    if (vrw == NULL)
    {
        SDL_Log("[GUMI] (Shader) Error: Could not open vertex shader file: \"%s\"", vertex_file.c_str());
        return false;
    }

    if (frw == NULL)
    {
        SDL_Log("[GUMI] (Shader) Error: Could not open fragment shader file: \"%s\"", fragment_file.c_str());
        return false;
    }

    Sint64 vsize = vrw->size(vrw);
    Sint64 fsize = frw->size(frw);

    char *vbuffer = new char[vsize];
    char *fbuffer = new char[fsize];

    vrw->read(vrw, vbuffer, vsize, 1);
    frw->read(frw, fbuffer, fsize, 1);

    std::string vshader(vbuffer, vsize);
    std::string fshader(fbuffer, fsize);

    vshader = "#version " + version + "\n" + vshader;
    fshader = "#version " + version + "\n" + fshader;

    delete[] vbuffer;
    delete[] fbuffer;
    SDL_FreeRW(vrw);
    SDL_FreeRW(frw);

    return compileShaders(vshader.c_str(), fshader.c_str(), vertex_file, fragment_file);

#else

    // Load Vertex File
    std::ifstream v_file(vertex_file.c_str());

    if (!v_file.is_open())
    {
        SDL_Log("[GUMI] (Shader) Error: Could not open vertex shader file: \"%s\"", vertex_file.c_str());
        return false;
    }

    std::string v_contents;
    std::string line;
    while (std::getline(v_file, line))
    {
        v_contents += line + "\n";
    }

    line.clear();

    // Load Fragment File
    std::ifstream f_file(fragment_file.c_str()); 

    if (!f_file.is_open())
    {
        SDL_Log("[GUMI] (Shader) Error: Could not open fragment shader file: \"%s\"", fragment_file.c_str());
        return false;
    }
    
    std::string f_contents;
    while (std::getline(f_file, line))
    {
        f_contents += line + "\n";
    }

    v_contents = "#version " + version + "\n" + v_contents;
    f_contents = "#version " + version + "\n" + f_contents;

    return compileShaders(v_contents.c_str(), f_contents.c_str(), vertex_file, fragment_file);

#endif

}

bool Shader::loadZipShader(const std::string &zip_vertex_file, const std::string &zip_fragment_file, const std::string &version)
{
    deconstruct();

#if defined(TARGET_OS_IPHONE) || defined(__ANDROID__)

    SDL_RWops *vrw = SDL_RWFromFile(zip_vertex_file.c_str(), "r");
    SDL_RWops *frw = SDL_RWFromFile(zip_fragment_file.c_str(), "r");

    if (vrw == NULL)
    {
        SDL_Log("[GUMI] (Shader) Error: Could not open vertex shader file: \"%s\"", zip_vertex_file.c_str());
        return false;
    }

    if (frw == NULL)
    {
        SDL_Log("[GUMI] (Shader) Error: Could not open fragment shader file: \"%s\"", zip_fragment_file.c_str());
        return false;
    }

    Sint64 vsize = vrw->size(vrw);
    Sint64 fsize = frw->size(frw);

    char *vbuffer = new char[vsize];
    char *fbuffer = new char[fsize];

    vrw->read(vrw, vbuffer, vsize, 1);
    frw->read(frw, fbuffer, fsize, 1);

    std::string vshader(vbuffer, vsize);
    std::string fshader(fbuffer, fsize);

    vshader = "#version " + version + "\n" + vshader;
    fshader = "#version " + version + "\n" + fshader;

    delete[] vbuffer;
    delete[] fbuffer;
    SDL_FreeRW(vrw);
    SDL_FreeRW(frw);

    return compileShaders(vshader.c_str(), fshader.c_str(), zip_vertex_file, zip_fragment_file);

#else

    gu::Zip zip_vs(zip_vertex_file);
    gu::Zip zip_fs(zip_fragment_file);

    std::string vertex(zip_vs.getBuffer(), zip_vs.getSize());
    std::string fragment(zip_fs.getBuffer(), zip_fs.getSize());

    free(zip_vs.getBuffer());
    free(zip_fs.getBuffer());

    vertex = "#version " + version + "\n" + vertex;
    fragment = "#version " + version + "\n" + fragment;

    return compileShaders(vertex.c_str(), fragment.c_str(), zip_vertex_file, zip_fragment_file);

#endif

}

bool Shader::compileShaders(const char *vertex, const char *fragment, const std::string &vertex_file_name, const std::string &fragment_file_name)
{
    // Vertex Shader
    vertex_shader_ = glCreateShader(GL_VERTEX_SHADER);
    fragment_shader_ = glCreateShader(GL_FRAGMENT_SHADER);
    program_ = glCreateProgram();
    loaded_ = true;

    glShaderSource(vertex_shader_, 1, &vertex, NULL);
    glCompileShader(vertex_shader_);

    GLint status;
    glGetShaderiv(vertex_shader_, GL_COMPILE_STATUS, &status);
    if (status != GL_TRUE)
    {
        SDL_Log("[GUMI] (Shader) Error: Vertex shader \"%s\" failed to compile:", vertex_file_name.c_str());

        GLchar buffer[500];
        GLsizei length;
        glGetShaderInfoLog(vertex_shader_, 500, &length, buffer);

        std::string error(buffer, length);
        SDL_Log("%s", error.c_str());

        return false;
    }

    // Fragment Shader
    glShaderSource(fragment_shader_, 1, &fragment, NULL);
    glCompileShader(fragment_shader_);

    glGetShaderiv(fragment_shader_, GL_COMPILE_STATUS, &status);
    if (status != GL_TRUE)
    {
        SDL_Log("[GUMI] (Shader) Error: Fragment shader \"%s\" failed to compile:", fragment_file_name.c_str());

        GLchar buffer[500];
        GLsizei length;
        glGetShaderInfoLog(fragment_shader_, 500, &length, buffer);

        std::string error(buffer, length);
        SDL_Log("%s", error.c_str());

        return false;
    }

    // Create Program
    glAttachShader(program_, vertex_shader_);
    glAttachShader(program_, fragment_shader_);

    // if (opengl_major_ > 2)
    //    glBindFragDataLocation(program_, 0, "outColor");

    glLinkProgram(program_);
    
    GLint linked;
    glGetProgramiv(program_, GL_LINK_STATUS, &linked);
    if (linked != GL_TRUE)
    {
        SDL_Log("[GUMI] (Shader) Error: Linking to program failed");

        GLchar buffer[500];
        GLsizei length;
        glGetProgramInfoLog(program_, 500, &length, buffer);

        std::string error(buffer, length);
        SDL_Log("%s", error.c_str());

        return false;
    }
    

    modify("proj", glm::mat4(1.0f), false);
    modify("view", glm::mat4(1.0f), false);

    return true;
}

void Shader::bind() const
{
    if (bound_shader_ != program_)
    {
        glUseProgram(program_);
        bound_shader_ = program_;
    }
}

void Shader::modify(const std::string &u, int i, bool cache)
{
    bind();

    if (cache)
    {
        // Cache location on first use
        if (locations_.count(u) == 0)
        {
            GLuint location = glGetUniformLocation(program_, u.c_str());
            locations_.insert(std::pair<std::string, GLuint>(u, location));
        }

        glUniform1i(locations_[u], i);
    }
    else
    {
        glUniform1i(getUniformLocation(u), i);
    }
}
    
void Shader::modify(const std::string &u, float f, bool cache)
{
    bind();
    
    if (cache)
    {
        // Cache location on first use
        if (locations_.count(u) == 0)
        {
            GLuint location = glGetUniformLocation(program_, u.c_str());
            locations_.insert(std::pair<std::string, GLuint>(u, location));
        }
        
        glUniform1f(locations_[u], f);
    }
    else
    {
        glUniform1f(getUniformLocation(u), f);
    }
}

void Shader::modify(const std::string &u, const glm::vec2 &v, bool cache)
{
    bind();

    if (cache)
    {
        // Cache location on first use
        if (locations_.count(u) == 0)
        {
            GLuint location = glGetUniformLocation(program_, u.c_str());
            locations_.insert(std::pair<std::string, GLuint>(u, location));
        }

        glUniform2fv(locations_[u], 1, glm::value_ptr(v));
    }
    else
    {
         glUniform2fv(getUniformLocation(u), 1, glm::value_ptr(v));
    }
}

void Shader::modify(const std::string &u, const glm::vec3 &v, bool cache)
{
    bind();

    if (cache)
    {
        // Cache location on first use
        if (locations_.count(u) == 0)
        {
            GLuint location = glGetUniformLocation(program_, u.c_str());
            locations_.insert(std::pair<std::string, GLuint>(u, location));
        }

        glUniform3fv(locations_[u], 1, glm::value_ptr(v));
    }
    else
    {
         glUniform3fv(getUniformLocation(u), 1, glm::value_ptr(v));
    }
}

void Shader::modify(const std::string &u, const glm::vec4 &v, bool cache)
{
    bind();
    
    if (cache)
    {
        // Cache location on first use
        if (locations_.count(u) == 0)
        {
            GLuint location = glGetUniformLocation(program_, u.c_str());
            locations_.insert(std::pair<std::string, GLuint>(u, location));
        }

        glUniform4fv(locations_[u], 1, glm::value_ptr(v));
    }
    else
    {
         glUniform4fv(getUniformLocation(u), 1, glm::value_ptr(v));
    }
}

void Shader::modify(const std::string &u, const glm::mat4 &m, bool cache)
{
    bind();

    if (cache)
    {
        // Cache location on first use
        if (locations_.count(u) == 0)
        {
            GLuint location = glGetUniformLocation(program_, u.c_str());
            locations_.insert(std::pair<std::string, GLuint>(u, location));
        }

        glUniformMatrix4fv(locations_[u], 1, GL_FALSE, glm::value_ptr(m));
    }
    else
    {
        glUniformMatrix4fv(getUniformLocation(u), 1, GL_FALSE, glm::value_ptr(m));
    }
}

void Shader::modify(const std::string &u, const std::vector<glm::mat4> *m, bool cache)
{
    bind();

    if (cache)
    {
        // Cache location on first use
        if (locations_.count(u) == 0)
        {
            GLuint location = glGetUniformLocation(program_, u.c_str());
            locations_.insert(std::pair<std::string, GLuint>(u, location));
        }

        glUniformMatrix4fv(locations_[u], m->size(), GL_FALSE, glm::value_ptr(m->at(0)));
    }
    else
    {
        glUniformMatrix4fv(getUniformLocation(u), m->size(), GL_FALSE, glm::value_ptr(m->at(0)));
    }
}

// Static

void Shader::setOpenGLVersion(int major)
{
    opengl_major_ = major;
}

int Shader::getOpenGLVersion()
{
    return opengl_major_;
}

void Shader::initDefaultShaders()
{
    // ------------------------
    // SHADER 3D
    // ------------------------
    
    const char *v1 =
    "\n#ifdef GL_ES\n"
    "precision highp float;"
    "\n#endif\n"
    ""
    "\n#ifdef GL_ES\n"
    "attribute vec3 vPosition;"
    "attribute lowp vec2 vTexCoord;"
    "\n#else\n"
    "in vec3 vPosition;"
    "in vec2 vTexCoord;"
    "\n#endif\n"
    ""
    "uniform mat4 pos;"
    "uniform mat4 view;"
    "uniform mat4 scale;"
    "uniform mat4 rotate;"
    "uniform mat4 proj;"
    ""
    "\n#ifdef GL_ES\n"
    "varying lowp vec2 texcoord;"
    "\n#else\n"
    "out vec2 texcoord;"
    "\n#endif\n"
    ""
    "void main()"
    "{"
    "    texcoord = vTexCoord;"
    "    gl_Position = proj * view * pos * scale * rotate * vec4(vPosition, 1.0);"
    "}";

    const char *f1 = 
    "\n#ifdef GL_ES\n"
    "precision lowp float;"
    "\n#endif\n"
    ""
    "\n#ifdef GL_ES\n"
    "varying vec2 texcoord;"
    "\n#else\n"
    "in vec2 texcoord;"
    "\n#endif\n"
    ""
    "uniform sampler2D textureSample;"
    "uniform vec4 colour;"
    ""
    "void main()"
    "{"
    "\n#ifdef GL_ES\n"
    "    gl_FragColor = texture2D(textureSample, texcoord) * colour;"
    "\n#else\n"
    "    gl_FragColor = texture(textureSample, texcoord) * colour;"
    "\n#endif\n"
    "}";

    SHADER_3D = new Shader();

#if defined(TARGET_OS_IPHONE) || defined(__ANDROID__)

    SHADER_3D->compile(v1, f1, "100");

#else

    SHADER_3D->compile(v1, f1, "130");

#endif

    // ------------------------
    // SHADER 3D RIG
    // ------------------------

    const char *v2 =
    "\n#ifdef GL_ES\n"
    "precision highp float;\n"
    "\n#endif\n"
    ""
    "\n#ifdef GL_ES\n"
    "attribute vec3 vPosition;"
    "attribute lowp vec2 vTexCoord;"
    "attribute vec4 boneids;"
    "attribute vec4 weights;"
    "\n#else\n"
    "in vec3 vPosition;"
    "in vec2 vTexCoord;"
    "in vec4 boneids;"
    "in vec4 weights;"
    "\n#endif\n"
    ""
    "uniform mat4 pos;"
    "uniform mat4 view;"
    "uniform mat4 scale;"
    "uniform mat4 rotate;"
    "uniform mat4 proj;"
    "uniform mat4 bones[50];"
    ""
    "\n#ifdef GL_ES\n"
    "varying lowp vec2 texcoord;"
    "\n#else\n"
    "out vec2 texcoord;"
    "\n#endif\n"
    ""
    "void main()"
    "{"
    "    texcoord = vTexCoord;"
    ""
    "    mat4 bone_transform = bones[int(boneids[0])] * weights[0];"
    "    bone_transform = bone_transform + (bones[int(boneids[1])] * weights[1]);"
    "    bone_transform = bone_transform + (bones[int(boneids[2])] * weights[2]);"
    "    bone_transform = bone_transform + (bones[int(boneids[3])] * weights[3]);"
    ""
    "    gl_Position = proj * view * pos * scale * rotate * bone_transform * vec4(vPosition, 1.0);"
    "}";

    SHADER_3D_RIG = new Shader();

#if defined(TARGET_OS_IPHONE) || defined(__ANDROID__)

    SHADER_3D_RIG->compile(v2, f1, "100");

#else

    SHADER_3D_RIG->compile(v2, f1, "130");

#endif

    // ------------------------
    // SHADER 2D
    // ------------------------

    const char *v3 =
    "\n#ifdef GL_ES\n"
    "precision highp float;"
    "\n#endif\n"
    ""
    "\n#ifdef GL_ES\n"
    "attribute vec2 vPosition;"
    "attribute mediump vec2 vTexCoord;"
    "\n#else\n"
    "in vec2 vPosition;"
    "in vec2 vTexCoord;"
    "\n#endif\n"
    ""
    "uniform mat4 pos;"
    "uniform mat4 view;"
    "uniform mat4 scale;"
    "uniform mat4 rotate;"
    "uniform mat4 proj;"
    ""
    "\n#ifdef GL_ES\n"
    "varying mediump vec2 texcoord;"
    "\n#else\n"
    "out vec2 texcoord;"
    "\n#endif\n"
    ""
    "void main()"
    "{"
    "    texcoord = vTexCoord;"
    "    gl_Position = proj * view * pos * scale * rotate * vec4(vPosition, 0.0, 1.0);"
    "}";

    const char *f2 = 
    "\n#ifdef GL_ES\n"
    "precision mediump float;"
    "\n#endif\n"
    ""
    "uniform sampler2D textureSample;"
    "uniform vec4 colour;"
    ""
    "\n#ifdef GL_ES\n"
    "varying vec2 texcoord;"
    "\n#else\n"
    "in vec2 texcoord;"
    "\n#endif\n"
    ""
    "void main()"
    "{"
    "\n#ifdef GL_ES\n"
    "    gl_FragColor = texture2D(textureSample, texcoord) * colour;"
    "\n#else\n"
    "    gl_FragColor = texture(textureSample, texcoord) * colour;"
    "\n#endif\n"
    "}";

    SHADER_2D = new Shader();
    SHADER_UI = new Shader();

#if defined(TARGET_OS_IPHONE) || defined(__ANDROID__)

    SHADER_2D->compile(v3, f2, "100");
    SHADER_UI->compile(v3, f2, "100");

#else

    SHADER_2D->compile(v3, f2, "130");
    SHADER_UI->compile(v3, f2, "130");

#endif

    // ------------------------
    // SHADER 2D BILLBOARD
    // ------------------------

    const char *v4 =
    "\n#ifdef GL_ES\n"
    "precision highp float;\n"
    "\n#endif\n"
    ""
    "\n#ifdef GL_ES\n"
    "attribute vec2 vPosition;"
    "attribute mediump vec2 vTexCoord;"
    "\n#else\n"
    "in vec2 vPosition;"
    "in vec2 vTexCoord;"
    "\n#endif\n"
    ""
    "uniform mat4 pos;"
    "uniform mat4 view;"
    "uniform mat4 scale;"
    "uniform mat4 rotate;"
    "uniform mat4 proj;"
    ""
    "\n#ifdef GL_ES\n"
    "varying mediump vec2 texcoord;"
    "\n#else\n"
    "out vec2 texcoord;"
    "\n#endif\n"
    ""
    "void main()"
    "{"
    "    texcoord = vTexCoord;"
    "    mat4 mvm = view * pos * scale * rotate;"
    "    mvm[0][0] = scale[0][0];"
    "    mvm[0][1] = 0.0;"
    "    mvm[0][2] = 0.0;"
    "    mvm[1][0] = 0.0;"
    "    mvm[1][1] = scale[1][1];"
    "    mvm[1][2] = 0.0;"
    "    mvm[2][0] = 0.0;"
    "    mvm[2][1] = 0.0;"
    "    mvm[2][2] = scale[2][2];"
    "    gl_Position = proj * mvm * vec4(vPosition, 0.0, 1.0);"
    "}";

    SHADER_2D_BILLBOARD = new Shader();

#if defined(TARGET_OS_IPHONE) || defined(__ANDROID__)

    SHADER_2D_BILLBOARD->compile(v4, f2, "100");

#else

    SHADER_2D_BILLBOARD->compile(v4, f2, "130");

#endif

    // ------------------------
    // SHADER PS
    // ------------------------

    const char *v5 =
    "\n#ifdef GL_ES\n"
    "precision highp float;\n"
    "\n#endif\n"
    ""
    "\n#ifdef GL_ES\n"
    "attribute vec3 vPosition;"
    "attribute mediump vec3 vColour;"
    "\n#else\n"
    "in vec3 vPosition;"
    "in vec3 vColour;"
    "\n#endif\n"
    ""
    "uniform mat4 pos;"
    "uniform mat4 view;"
    "uniform mat4 proj;"
    "uniform float point_size;"
    ""
    "\n#ifdef GL_ES\n"
    "varying mediump vec3 colour;"
    "\n#else\n"
    "out vec3 colour;"
    "\n#endif\n"
    ""
    "void main()"
    "{"
    "    colour = vColour;"
    "    gl_PointSize = point_size;"
    "    gl_Position = proj * view * pos * vec4(vPosition, 1.0);"
    "}";

    const char *f3 = 
    "\n#ifdef GL_ES\n"
    "precision mediump float;\n"
    "\n#endif\n"
    ""
    "\n#ifdef GL_ES\n"
    "varying vec3 colour;"
    "\n#else\n"
    "in vec3 colour;"
    "\n#endif\n"
    ""
    "void main()"
    "{"
    "    gl_FragColor.rgb = colour;"
    "}";

    SHADER_PS = new Shader();

#if defined(TARGET_OS_IPHONE) || defined(__ANDROID__)

    SHADER_PS->compile(v5, f3, "100");

#else

    SHADER_PS->compile(v5, f3, "130");

#endif
    
    // ------------------------
    // SHADER PS TEXTURE
    // ------------------------

    const char *v6 =
    "\n#ifdef GL_ES\n"
    "precision highp float;\n"
    "\n#endif\n"
    ""
    "\n#ifdef GL_ES\n"
    "attribute vec3 vPosition;"
    "attribute float vRotation;"
    "varying mat4 rotation;"
    "\n#else\n"
    "in vec3 vPosition;"
    "in float vRotation;"
    "out mat4 rotation;"
    "\n#endif\n"
    ""
    "uniform mat4 pos;"
    "uniform mat4 view;"
    "uniform mat4 proj;"
    "uniform float point_size;"
    ""
    "void main()"
    "{"
    ""
    "   float sin = sin(vRotation);"
    "   float cos = cos(vRotation);"
    ""
    "   mat4 rot;"
    ""
    "   rot[0][0] = cos;"
    "   rot[0][1] = sin;"
    "   rot[0][2] = 0.0;"
    "   rot[0][3] = 0.0;"
    ""
    "   rot[1][0] = -sin;"
    "   rot[1][1] = cos;"
    "   rot[1][2] = 0.0;"
    "   rot[1][3] = 0.0;"
    ""
    "   rot[2][0] = 0.0;"
    "   rot[2][1] = 0.0;"
    "   rot[2][2] = 1.0;"
    "   rot[2][3] = 0.0;"
    ""
    "   rot[3][0] = 0.5*(-cos + sin + 1.0);"
    "   rot[3][1] = 0.5*(-sin - cos + 1.0);"
    "   rot[3][2] = 0.0;"
    "   rot[3][3] = 1.0;"
    ""
    "    rotation = rot;"
    "    gl_PointSize = point_size;"
    "    gl_Position = proj * view * pos * vec4(vPosition, 1.0);"
    "}";

    const char *f4 = 
    "\n#ifdef GL_ES\n"
    "precision highp float;\n"
    "\n#endif\n"
    ""
    "uniform sampler2D textureSample;"
    ""
    "\n#ifdef GL_ES\n"
    "varying mat4 rotation;"
    "\n#else\n"
    "in mat4 rotation;"
    "\n#endif\n"
    ""
    "void main()"
    "{"
    "\n#ifdef GL_ES\n"
    "    gl_FragColor = texture2D(textureSample, (rotation * vec4(gl_PointCoord, 0.0, 1.0)).xy);"
    "\n#else\n"
    "    gl_FragColor = texture(textureSample, (rotation * vec4(gl_PointCoord, 0.0, 1.0)).xy);"
    "\n#endif\n"
    "}";

    SHADER_PS_TEXTURE = new Shader();

#if defined(TARGET_OS_IPHONE) || defined(__ANDROID__)

    SHADER_PS_TEXTURE->compile(v6, f4, "100");

#else

    SHADER_PS_TEXTURE->compile(v6, f4, "130");

#endif

}

void Shader::deleteDefaultShaders()
{
    delete SHADER_3D;
    delete SHADER_3D_RIG;
    delete SHADER_2D;
    delete SHADER_2D_BILLBOARD;
    delete SHADER_PS;
    delete SHADER_PS_TEXTURE;
    delete SHADER_UI;
}

// Getters

GLuint Shader::getProgram() const
{
    return program_;
}

GLuint Shader::getAttribLocation(const std::string &str) const
{
    bind();

    return glGetAttribLocation(program_, str.c_str());
}

GLuint Shader::getUniformLocation(const std::string &str) const
{
    bind();

    return glGetUniformLocation(program_, str.c_str());
}

}
