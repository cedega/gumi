#include "GUMI/Music.h"

namespace gu
{

Music::Music()
{
    zip_used_ = false;
    music_ = NULL;
    zip_rw_ = NULL;
    zip_music_buffer_ = NULL;
    volume_ = MIX_MAX_VOLUME;
}

Music::~Music()
{
    deconstruct();
}

void Music::deconstruct()
{
    Mix_HaltMusic();
    Mix_FreeMusic(music_);
    if (zip_used_)
    {
        free(zip_music_buffer_);
        SDL_RWclose(zip_rw_);
        zip_used_ = false;
    }
}

void Music::loadZipMusic(const std::string &file_path)
{
    deconstruct();

    gu::Zip zip(file_path);
    zip_music_buffer_ = zip.getBuffer();
    
    zip_rw_ = SDL_RWFromMem(zip.getBuffer(),zip.getSize());
    music_ = Mix_LoadMUS_RW(zip_rw_,0);
    
    if (music_ == NULL)
        SDL_Log("[GUMI] (Music) Error: Could not load \"%s\"", file_path.c_str());
    
    zip_used_ = true;
}

void Music::loadFileMusic(const std::string &file_path)
{
    if (gu::Zip::getRedirectFileLoading())
    {
        loadZipMusic(file_path);
        return;
    }

    deconstruct();
    
    music_ = Mix_LoadMUS(file_path.c_str());
    
    if (music_ == NULL)
        SDL_Log("[GUMI] (Music) Error: Could not load \"%s\"", file_path.c_str());
    
    zip_used_ = false;
}

void Music::play()
{
    if (Mix_PlayMusic(music_,-1) != 0)
        SDL_Log("[GUMI] (Music) Error: Could not play music: \"%s\"", Mix_GetError());
}

void Music::stop()
{
    Mix_HaltMusic();
}

void Music::fadeOut(int ms)
{
    Mix_FadeOutMusic(ms);
}

void Music::fadeIn(int ms)
{
    Mix_FadeInMusic(music_, -1, ms);
}

// Setters

void Music::setVolume(int volume)
{
    if (volume > MIX_MAX_VOLUME) volume = MIX_MAX_VOLUME;
    else if (volume < 0) volume = 0;
    
    volume_ = volume;
    Mix_VolumeMusic(volume_);
}

// Getters

int Music::getVolume() const
{
    return volume_;
}

}
