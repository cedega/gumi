#include "GUMI/ThumbStick.h"

namespace gu
{

ThumbStick::ThumbStick() :
    active_radius_(0.0f),
    distance_threshold_(10.0f),
    snap_index_(-1),
    last_snap_distance_(0)
{
}

void ThumbStick::loadTextures(GLuint front_texture_id, GLuint back_texture_id, gu::Shader *front_shader, gu::Shader *back_shader)
{
    if (back_shader == NULL)
        back_shader = front_shader;

    button_.loadTexture(front_texture_id, front_shader);
    button_bg_.loadTexture(back_texture_id, back_shader);
}

void ThumbStick::draw()
{
    button_bg_.draw();
    button_.draw();
}

glm::vec2 ThumbStick::poll()
{

#if defined(TARGET_OS_IPHONE) || defined(__ANDROID__)

    glm::vec2 point;    // the relative point from the centre of the button's background
    float distance = 0.0f; // distance of 'point'

    std::vector<gu::Finger> *fingers = gu::Input::getTouchFingers();

    // Check for new finger touches
    for (unsigned int i = 0; i < fingers->size(); i++)
    {
        if (fingers->at(i).type == SDL_FINGERDOWN)
        {
            float x = fingers->at(i).x;
            float y = fingers->at(i).y;

            glm::vec2 dv = glm::vec2(x, y) - button_bg_.getPosition(false);

            if (glm::length(dv) <= active_radius_ * button_bg_.getScaling().x)
            {
                point = glm::vec2(x, y);
                distance = 0.0f;

                last_snap_point_ = point;
                last_snap_distance_ = distance;

                snap_index_ = fingers->at(i).index;
                pivot_ = point;
            }
        }
    }

    // Check for snapped finger touch
    if (snap_index_ != -1)
    {
        bool snap_update = false;

        for (unsigned int i = 0; i < fingers->size(); i++)
        {
            if (snap_index_ == fingers->at(i).index)
            {
                if (fingers->at(i).type == SDL_FINGERMOTION)
                {
                    point = glm::vec2(fingers->at(i).x - pivot_.x, fingers->at(i).y - pivot_.y);
                    distance = std::sqrt(point.x * point.x + point.y * point.y);

                    last_snap_point_ = point;
                    last_snap_distance_ = distance;

                    snap_update = true;
                }
                else if (fingers->at(i).type == SDL_FINGERUP)
                {
                    snap_index_ = -1;
                    snap_update = true;
                }
            }
        }

        // In case the snapped was held still (finger was not lifted)
        if (!snap_update)
        {
            // Use previous data
            point = last_snap_point_;
            distance = last_snap_distance_;
        }
    }

    // Leash button graphic to mouse point
    leash(point, distance);
        
    // Only move if dragged a certain distance away
    // Move the player
    if (snap_index_ != -1 && distance > distance_threshold_ * button_bg_.getScaling().x)
        return glm::normalize(point);
    else
        return glm::vec2(0.0f, 0.0f);

#else

    // On desktop, default to WASD
    
    gu::Keys keys = gu::Input::getKeyboardState();

    glm::vec2 direction;

    if (keys[SDL_SCANCODE_W])
        direction.y -= 1.0f;
    if (keys[SDL_SCANCODE_S])
        direction.y += 1.0f;
    if (keys[SDL_SCANCODE_A])
        direction.x -= 1.0f;
    if (keys[SDL_SCANCODE_D])
        direction.x += 1.0f;

    return glm::normalize(direction);

#endif

}
    
void ThumbStick::reset()
{
    snap_index_ = -1;
}

void ThumbStick::leash(const glm::vec2 &point, float distance)
{
    glm::vec2 button_pos;
    
    if (snap_index_ != -1)
    {
        glm::vec2 button_pos = glm::length(point) > 0 ? glm::normalize(point) : glm::vec2(0.0f, 0.0f);
        if (distance > 60.0f * button_bg_.getScaling().x)
        {
            button_pos.x *= 60 * button_bg_.getScaling().x;
            button_pos.y *= 60 * button_bg_.getScaling().x;
        }
        else
        {
            button_pos.x *= distance;
            button_pos.y *= distance;
        }
        
        button_pos.x += button_bg_.getPosition().x;
        button_pos.y += button_bg_.getPosition().y;
        
        button_.setPosition(button_pos, false);
    }
    else
        button_.setPosition(button_bg_.getPosition(false), false);
}

// Setters

void ThumbStick::setActiveRadius(float r)
{
    active_radius_ = r;
}

void ThumbStick::setScalePercent(float percent, gu::Axis axis)
{
    button_.setScalePercent(percent, axis);
    button_bg_.setScalePercent(percent, axis);
}

void ThumbStick::setPositionPercent(const glm::vec2 &position_percent)
{
    button_.setPositionPercent(position_percent);
    button_bg_.setPositionPercent(position_percent);
}

void ThumbStick::setDistanceThreshold(float d)
{
    distance_threshold_ = d;
}

// Getters

float ThumbStick::getActiveRadius() const
{
    return active_radius_;
}

float ThumbStick::getScalePercent(gu::Axis *axis) const
{
    return button_bg_.getScalePercent(axis);
}

const glm::vec2& ThumbStick::getPositionPercent() const
{
    return button_bg_.getPositionPercent();
}

float ThumbStick::getDistanceThreshold() const
{
    return distance_threshold_;
}

}
