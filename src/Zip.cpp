#include "GUMI/Zip.h"

namespace gu
{

std::string Zip::zip_path_;
std::string Zip::zip_password_;
bool Zip::redirect_file_loading_ = false;

Zip::Zip() :
    zip_(NULL)
{
}

Zip::Zip(const std::string &filename) :
    zip_(NULL)
{
    loadFile(filename);
}

void Zip::loadFile(const std::string &filename)
{
    zip_ = unzOpen(zip_path_.c_str());
    
    if (zip_ == NULL)
    {
        SDL_Log("[GUMI] (Zip) Error: Could not open zip file");
    }
    unzLocateFile(zip_,filename.c_str(),0);
    
    unzGetCurrentFileInfo(zip_,&info_,NULL,0,NULL,0,NULL,0);
    
    //unzOpenCurrentFile(zip_); unencrypted file load
    if (unzOpenCurrentFilePassword(zip_, zip_password_.c_str()) != UNZ_OK)
    {
        SDL_Log("[GUMI] (Zip) Error: Decryption failure");
    }

    buffer_ = (char*)malloc(info_.uncompressed_size);

    if ((unsigned int)unzReadCurrentFile(zip_, buffer_, info_.uncompressed_size) != info_.uncompressed_size)
    {
        SDL_Log("[GUMI] (Zip) Error: Compression error");
    }

    size_ = info_.uncompressed_size;
    unzClose(zip_);
}

// Static

void Zip::setZipPath(const std::string &path)
{
    zip_path_ = path;
}

void Zip::setZipPassword(const std::string &password)
{
    zip_password_ = password;
}

void Zip::setRedirectFileLoading(bool b)
{
    redirect_file_loading_ = b;
}

bool Zip::getRedirectFileLoading()
{
    return redirect_file_loading_;
}

// Getters

char* Zip::getBuffer()
{
    return buffer_;
}

uLong Zip::getSize()
{
    return size_;
}

}
