#include "GUMI/FastOrderDraw.h"

namespace gu
{

FastOrderDraw::FastOrderDraw()
{
}

void FastOrderDraw::draw(const float interpolation, const float frame_time)
{
    glDepthMask(GL_FALSE);
    for (unsigned int i = 0; i < list_.size(); i++)
    {
        list_[i].drawable->updateAnimations(frame_time);
        list_[i].drawable->draw(interpolation);
    }
    glDepthMask(GL_TRUE);
}

bool compare(const gu::FastDrawElement &A, const gu::FastDrawElement &B)
{
    return A.distance > B.distance;
}

void FastOrderDraw::sort(const gu::Camera &camera)
{
    for (unsigned int i = 0; i < list_.size(); i++)
        list_[i].distance = glm::length(camera.getPosition() - list_[i].drawable->getPosition());

    std::sort(list_.begin(), list_.end(), compare);
}

void FastOrderDraw::add(gu::Drawable *drawable)
{
    gu::FastDrawElement temp;
    temp.drawable = drawable;
    list_.push_back(temp);
}

void FastOrderDraw::clear()
{
    list_.clear();
}

}
