#include "GUMI/GameSettings.h"

namespace gu
{

std::map<std::string, Category> GameSettings::categories_;

void GameSettings::loadFileSettings(const std::string &file_path)
{
    if (gu::Zip::getRedirectFileLoading())
    {
        loadZipSettings(file_path);
        return;
    }

#if defined(TARGET_OS_IPHONE) || defined(__ANDROID__)

    SDL_RWops *rw = SDL_RWFromFile(file_path.c_str(), "r");

    if (rw == NULL)
    {
        SDL_Log("[GUMI] (GameSettings) Error: Could not open game settings file: \"%s\"", file_path.c_str());
        return;
    }

    Sint64 size = rw->size(rw);

    char *buffer = new char[size];
    rw->read(rw, buffer, size, 1);

    std::string contents(buffer, size);

    delete[] buffer;
    SDL_FreeRW(rw);

    processLoad(contents);

#else

    std::ifstream file(file_path.c_str());

    std::string line;
    std::string contents;

    while (std::getline(file, line))
    {
        contents += line + "\n";
    }

    file.close();
    
    // Remove potential Windows file endings
    contents.erase(std::remove(contents.begin(), contents.end(), '\r'), contents.end()); 

    processLoad(contents);

#endif

}

void GameSettings::loadZipSettings(const std::string &file_path)
{

#if defined(TARGET_OS_IPHONE) || defined(__ANDROID__)

    SDL_RWops *rw = SDL_RWFromFile(file_path.c_str(), "r");

    if (rw == NULL)
    {
        SDL_Log("[GUMI] (GameSettings) Error: Could not open game settings file: \"%s\"", file_path.c_str());
        return;
    }

    Sint64 size = rw->size(rw);

    char *buffer = new char[size];
    rw->read(rw, buffer, size, 1);

    std::string contents(buffer, size);

    delete[] buffer;
    SDL_FreeRW(rw);

    processLoad(contents);

#else

    gu::Zip zip(file_path);
    std::string contents(zip.getBuffer(), zip.getSize());
    free(zip.getBuffer());

    // Remove potential Windows file endings
    contents.erase(std::remove(contents.begin(), contents.end(), '\r'), contents.end()); 

    processLoad(contents);

#endif

}

void GameSettings::clear()
{
    categories_.clear();
}

void GameSettings::addCategory(const std::string &category)
{
    categories_.insert(std::pair<std::string, Category>(category, Category()));
}

void GameSettings::addKey(const std::string &category, const std::string &key, int value)
{
    std::map<std::string,Category>::iterator it = categories_.find(category);

    if (it != categories_.end())
    {
        it->second.int_keys.insert(std::pair<std::string,int>(key, value));
    }
    else
    {
        SDL_Log("[GUMI] (GameSettings) Error: Could find category: \"%s\"", category.c_str());
    }
}

void GameSettings::addKey(const std::string &category, const std::string &key, float value)
{
    std::map<std::string,Category>::iterator it = categories_.find(category);

    if (it != categories_.end())
    {
        it->second.float_keys.insert(std::pair<std::string,float>(key, value));
    }
    else
    {
        SDL_Log("[GUMI] (GameSettings) Error: Could find category: \"%s\"", category.c_str());
    }
}

void GameSettings::addKey(const std::string &category, const std::string &key, const std::string &value)
{
    std::map<std::string,Category>::iterator it = categories_.find(category);

    if (it != categories_.end())
    {
        it->second.string_keys.insert(std::pair<std::string,std::string>(key, value));
    }
    else
    {
        SDL_Log("[GUMI] (GameSettings) Error: Could find category: \"%s\"", category.c_str());
    }
}

int GameSettings::getIntKey(const std::string &category, const std::string &key)
{
    std::map<std::string,Category>::iterator it1 = categories_.find(category);

    if (it1 != categories_.end())
    {
        std::map<std::string,int>::iterator it2 = it1->second.int_keys.find(key);

        if (it2 != it1->second.int_keys.end())
        {
            return it2->second;
        }
        else
        {
            SDL_Log("[GUMI] (GameSettings) Error: Could find int value for key: \"%s\"", key.c_str());
            return 0;
        }
    }
    else
    {
        SDL_Log("[GUMI] (GameSettings) Error: Could find category: \"%s\"", category.c_str());
        return 0;
    }
}

float GameSettings::getFloatKey(const std::string &category, const std::string &key)
{
    std::map<std::string,Category>::iterator it1 = categories_.find(category);

    if (it1 != categories_.end())
    {
        std::map<std::string,float>::iterator it2 = it1->second.float_keys.find(key);

        if (it2 != it1->second.float_keys.end())
        {
            return it2->second;
        }
        else
        {
            SDL_Log("[GUMI] (GameSettings) Error: Could find float value for key: \"%s\"", key.c_str());
            return 0.0f;
        }
    }
    else
    {
        SDL_Log("[GUMI] (GameSettings) Error: Could find category: \"%s\"", category.c_str());
        return 0.0f;
    }
}

std::string GameSettings::getStringKey(const std::string &category, const std::string &key)
{
    std::map<std::string,Category>::iterator it1 = categories_.find(category);

    if (it1 != categories_.end())
    {
        std::map<std::string,std::string>::iterator it2 = it1->second.string_keys.find(key);

        if (it2 != it1->second.string_keys.end())
        {
            return it2->second;
        }
        else
        {
            SDL_Log("[GUMI] (GameSettings) Error: Could find string value for key: \"%s\"", key.c_str());
            return "";
        }
    }
    else
    {
        SDL_Log("[GUMI] (GameSettings) Error: Could find category: \"%s\"", category.c_str());
        return "";
    }
}

void GameSettings::processLoad(const std::string &contents)
{
    std::vector<std::string> lines;

    size_t index1 = 0;
    size_t index2 = 0;

    while (true)
    {
        index2 = contents.find('\n', index1);
        std::string pair = contents.substr(index1, index2 - index1);

        if (pair.find(':') != std::string::npos || pair.find('{') != std::string::npos)
            lines.push_back(pair);

        if (index2 == std::string::npos)
            break;

        index1 = index2 + 1;
    }

    std::string category;

    for (unsigned int i = 0; i < lines.size(); i++)
    {
        // Ignore any pairs without a valid category
        if (category.empty() && lines[i].at(0) != '{')
            continue;

        // Ignore lines which are comments
        if (lines[i].at(0) == '#')
            continue;

        // New category
        if (lines[i].at(0) == '{')
        {
            category = lines[i].substr(1, lines[i].length() - 2);
            category.erase(std::remove_if(category.begin(), category.end(), isspace), category.end());
            addCategory(category.c_str());
        }
        // New Key:Value
        else
        {
            size_t delimiter = lines[i].find_first_of(':');
            std::string key = lines[i].substr(0, delimiter);
            std::string value = lines[i].substr(delimiter + 1);

            // Remove excess white space from Key:Value
            key.erase(std::remove_if(key.begin(), key.end(), isspace), key.end());

            size_t quote_start = value.find_first_of('\"');
            size_t quote_end = value.find_last_of('\"');

            if (quote_start == std::string::npos)
                value.erase(std::remove_if(value.begin(), value.end(), isspace), value.end());
            else
                value = value.substr(quote_start, quote_end - quote_start + 1);

            // Determine Value Type
            if (value[0] == '\"')
            {
                // Remove beginning and end quotes
                value.erase(0, 1);
                if (value[value.size() - 1] == '\"')
                    value.erase(value.size() - 1, 1);
                
                // String type
                addKey(category.c_str(), key.c_str(), value);
            }
            else if (value.find('.') != std::string::npos)
            {
                // Float type
                addKey(category.c_str(), key.c_str(), static_cast<float>(atof(value.c_str()))); 
            }
            else
            {
                // Int type
                addKey(category.c_str(), key.c_str(), atoi(value.c_str())); 
            }
        }
    }
}

}
