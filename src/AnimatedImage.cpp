#include "GUMI/AnimatedImage.h"

namespace gu
{

AnimatedImage::AnimatedImage()
{
}

AnimatedImage::AnimatedImage(const AnimatedImage &obj) :
    gu::Image(obj),
    animated_texture_(obj.animated_texture_),
    texture_infos_(obj.texture_infos_)
{
}

AnimatedImage& AnimatedImage::operator=(const AnimatedImage &obj)
{
    gu::Image::operator=(obj);

    animated_texture_ = obj.animated_texture_;
    texture_infos_ = obj.texture_infos_;

    return *this;
}

void AnimatedImage::draw(const float interpolation, bool pure_2D)
{
    gu::TextureInfo info = texture_infos_[animated_texture_.getCurrentTexture()];
    gu::Image::setTexture(animated_texture_.getCurrentTexture(), info.w, info.h, pure_2D);
    gu::Image::draw(interpolation);
}

void AnimatedImage::addTexture(GLuint texture, float time)
{
    animated_texture_.addTexture(texture, time);
    gu::TextureInfo *info = gu::TextureLoader::getTextureInfo(texture);
    texture_infos_.insert(std::pair<GLuint, gu::TextureInfo>(texture, *info));
}

void AnimatedImage::update(const float dt)
{
    animated_texture_.update(dt);
}

gu::AnimatedTexture* AnimatedImage::getAnimatedTexture()
{
    return &animated_texture_;
}

}
