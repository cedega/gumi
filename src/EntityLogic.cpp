#include "GUMI/EntityLogic.h"

namespace gu
{

float EntityLogic::default_gravity_ = -9.8f;
glm::vec3 EntityLogic::default_terminal_velocity_ = glm::vec3(100.0f, 100.0f, 100.0f);

EntityLogic::EntityLogic() :
    use_default_gravity_(true),
    ignore_gravity_(false),
    gravity_(getDefaultGravity()),
    terminal_velocity_(getDefaultTerminalVelocity())
{
}

EntityLogic::EntityLogic(const EntityLogic &obj) :
    position_(obj.position_),
    velocity_(obj.velocity_),
    acceleration_(obj.acceleration_),
    step_(obj.step_),
    use_default_gravity_(obj.use_default_gravity_),
    ignore_gravity_(obj.ignore_gravity_),
    gravity_(obj.gravity_),
    terminal_velocity_(obj.terminal_velocity_)
{
    forces_ = obj.forces_;
}

EntityLogic& EntityLogic::operator=(const EntityLogic &obj)
{
    position_ = obj.position_;
    velocity_ = obj.velocity_;
    acceleration_ = obj.acceleration_;
    step_ = obj.step_;

    use_default_gravity_ = obj.use_default_gravity_;
    ignore_gravity_ = obj.ignore_gravity_;
    gravity_ = obj.gravity_;
    terminal_velocity_ = obj.terminal_velocity_;

    forces_ = obj.forces_;
    
    return *this;
}

// Velocity is seperated into 2 variables: force and non-forced
void EntityLogic::move(const float dt, bool rerun)
{
    // Compute force velocities
    applyForces(dt, rerun);

	// Compute Gravity
	if (!ignore_gravity_ && !forceHasDisabledGravity())
		acceleration_.y = use_default_gravity_ ? default_gravity_ : gravity_;
	else
		acceleration_.y = 0;

    // Apply acceleration before position value calculation
    velocity_ += acceleration_*dt; 

    // Move
    step_ = velocity_*dt + force_velocity_*dt;
    position_ += step_;
    
	// Set terminal velocities
    if (gu::absval(velocity_.y) > terminal_velocity_.y)
    {
        if (velocity_.y < 0) velocity_.y = -terminal_velocity_.y;
        else                 velocity_.y = terminal_velocity_.y;
    }

    if (gu::absval(velocity_.x) > terminal_velocity_.x)
    {
        if (velocity_.y < 0) velocity_.x = -terminal_velocity_.x;
        else                 velocity_.x = terminal_velocity_.x;
    }
    
    if (gu::absval(velocity_.z) > terminal_velocity_.z)
    {
        if (velocity_.z < 0) velocity_.z = -terminal_velocity_.z;
        else                 velocity_.z = terminal_velocity_.z;
    }
}

// Force acceleration is continually added to the internal force velocity
// therefore, setting force variables to 0 every frame does not break acceleration build up
void EntityLogic::applyForces(float dt, bool rerun)
{
    // Set the force velocities to 0 -- prepare for adding
    force_velocity_ = glm::vec3(0.0f, 0.0f, 0.0f);

    // X-Forces
    for (int i = 0; i < static_cast<int>(forces_.size()); i++)
    {
        // If Force is active
        if (!rerun || forces_[i].rerun)
        {
            forces_[i].time -= static_cast<int>(dt*1000);

            // As long as the force is still active, then move
            if (forces_[i].time > 0)
            {
                force_velocity_ += forces_[i].velocity;
                forces_[i].velocity += forces_[i].acceleration*dt;
            }
            else
            {
                // remove force
                onForceEnd(i);
                forces_.erase(forces_.begin() + i);
                i--;
            }
        }
    }
}

gu::COLLISION_ID EntityLogic::collide(gu::AABB *hitbox, const gu::AABB &object)
{
    if (gu::intersects(*hitbox, object))
    {
        const float skin = 0.000001f;

        float x_intrusion_left = hitbox->centre.x + hitbox->radius.x - (object.centre.x - object.radius.x);
        float x_intrusion_right = object.centre.x + object.radius.x - (hitbox->centre.x - hitbox->radius.x);

        float y_intrusion_top = object.centre.y + object.radius.y - (hitbox->centre.y - hitbox->radius.y);
        float y_intrusion_bottom = hitbox->centre.y + hitbox->radius.y - (object.centre.y - object.radius.y);

        // Z is positive towards the camera
        float z_intrusion_top = hitbox->centre.z + hitbox->radius.z - (object.centre.z - object.radius.z);
        float z_intrusion_bottom = object.centre.z + object.radius.z - (hitbox->centre.z - hitbox->radius.z);

        float x_intrusion = std::min(x_intrusion_left, x_intrusion_right);
        float y_intrusion = std::min(y_intrusion_top, y_intrusion_bottom);
        float z_intrusion = std::min(z_intrusion_top, z_intrusion_bottom);

        // Displace X
        if (x_intrusion < y_intrusion && x_intrusion < z_intrusion)
        {
            // On left
            if (x_intrusion_left < x_intrusion_right)
            {
                hitbox->centre.x -= (x_intrusion_left + skin);
                return gu::COL_X_LEFT;
            }
            // On Right
            else
            {
                hitbox->centre.x += (x_intrusion_right + skin);
                return gu::COL_X_RIGHT;
            }
        }

        // Displace Y
        if (y_intrusion < x_intrusion && y_intrusion < z_intrusion)
        {
            // On Top
            if (y_intrusion_top < y_intrusion_bottom)
            {
                hitbox->centre.y += (y_intrusion_top + skin);
                setVelocity(glm::vec3(getVelocity().x, 0, getVelocity().z));
                onLand();
                return gu::COL_Y_TOP;
            }
            // On Bottom
            else
            {
                hitbox->centre.y -= (y_intrusion_bottom + skin);
                return gu::COL_Y_BOT;
            }
        }

        // Displace Z
        if (z_intrusion < y_intrusion && z_intrusion < x_intrusion)
        {
            // On Top
            if (z_intrusion_top < z_intrusion_bottom)
            {
                hitbox->centre.z -= (z_intrusion_top + skin);
                return gu::COL_Z_TOP;
            }
            // On Bottom
            else
            {
                hitbox->centre.z += (z_intrusion_bottom + skin);
                return gu::COL_Z_BOT;
            }
        }
    }

    return gu::COL_NONE;
}

bool EntityLogic::collide(const gu::YOBB &hitbox, const gu::YOBB &object, glm::vec3 *mtv, const gu::AABB *first_check, const gu::AABB *object_aabb)
{
    if (first_check != NULL)
    {
        if (!gu::intersects(*first_check, *object_aabb))
        {
            if (mtv != NULL)
                *mtv = glm::vec3(0.0f);
            
            return false;
        }
    }
    
    glm::vec3 temp_mtv;
    bool intersects = gu::intersects(hitbox, object, &temp_mtv);

    const float epsilon = 0.000000001f;

    // X and Z directions are 0, but Y mtv is negative (-- == +). Must be a landing on a surface.
    if (gu::absval(temp_mtv.x) < epsilon && gu::absval(temp_mtv.z) < epsilon && temp_mtv.y < 0.0f)
    {
        setVelocity(glm::vec3(getVelocity().x, 0.0f, getVelocity().z));
        onLand();
    }

    if (mtv != NULL)
        *mtv = temp_mtv;

    return intersects;
}

// Forces

int EntityLogic::addForce(const glm::vec3 &velocity, const glm::vec3 &acceleration, int time)
{
    forces_.push_back(gu::Force(velocity, acceleration, time));
    return forces_.size() - 1;
}

int EntityLogic::addForce(const gu::Force &force)
{
    forces_.push_back(force);
    return forces_.size() - 1;
}

void EntityLogic::removeForce(int force_id)
{
    if (force_id >= 0 && force_id < static_cast<int>(forces_.size()))
    {
        forces_.erase(forces_.begin() + force_id);
    }
}

void EntityLogic::removeAllForces()
{
    forces_.clear();
}

bool EntityLogic::forceExists(int force_id) const
{
    return force_id >= 0 && force_id < static_cast<int>(forces_.size());
}

bool EntityLogic::anyForceExists() const
{
    return !forces_.empty();
}

bool EntityLogic::forceHasDisabledGravity() const
{
    for (unsigned int i = 0; i < forces_.size(); i++)
    {
        if (forces_[i].disable_gravity)
            return true;
    }

    return false;
}

int EntityLogic::applyNoRerun(int force_id)
{
    if (force_id >= 0 && force_id < static_cast<int>(forces_.size()))
        forces_[force_id].rerun = false;

    return force_id;
}

int EntityLogic::applyNoGravity(int force_id)
{
    if (force_id >= 0 && force_id < static_cast<int>(forces_.size()))
        forces_[force_id].disable_gravity = true;

    return force_id;
}

// Triggers

void EntityLogic::onForceEnd(int force_id)
{

}

void EntityLogic::onLand()
{

}

// Static

void EntityLogic::setDefaultGravity(float gravity)
{
    default_gravity_ = gravity;
}

void EntityLogic::setDefaultTerminalVelocity(const glm::vec3 &terminal_velocity)
{
    default_terminal_velocity_ = terminal_velocity;
}

float EntityLogic::getDefaultGravity()
{
    return default_gravity_;
}

glm::vec3 EntityLogic::getDefaultTerminalVelocity()
{
    return default_terminal_velocity_;
}

// Setters

void EntityLogic::setX(float x)
{
    position_.x = x;
}

void EntityLogic::setY(float y)
{
    position_.y = y;
}

void EntityLogic::setZ(float z)
{
    position_.z = z;
}

void EntityLogic::setPosition(const glm::vec3 &position)
{
    position_ = position;
}

void EntityLogic::setVelocity(const glm::vec3 &velocity)
{
    velocity_ = velocity;
}

void EntityLogic::setAcceleration(const glm::vec3 &acceleration)
{
    acceleration_ = acceleration;
}

void EntityLogic::setIgnoreGravity(bool ignore)
{
	ignore_gravity_ = ignore;
}

void EntityLogic::setGravity(float gravity)
{
	gravity_ = gravity;
    use_default_gravity_ = false;
}

void EntityLogic::setTerminalVelocity(const glm::vec3 &terminal_velocity)
{
    terminal_velocity_ = terminal_velocity;
}

// Getters

float EntityLogic::getX() const
{
    return position_.x;
}

float EntityLogic::getY() const
{
    return position_.y;
}

float EntityLogic::getZ() const
{
    return position_.z;
}

const glm::vec3& EntityLogic::getPosition() const
{
    return position_;
}

const glm::vec3& EntityLogic::getVelocity() const
{
   return velocity_;
}

const glm::vec3& EntityLogic::getStep() const
{
    return step_;
}

const glm::vec3& EntityLogic::getAcceleration() const
{
   return acceleration_;
}

const glm::vec3& EntityLogic::getForceVelocity() const
{
    return force_velocity_;
}

bool EntityLogic::getIgnoreGravity() const
{
	return ignore_gravity_;
}

float EntityLogic::getGravity() const
{
    return gravity_;
}

const glm::vec3& EntityLogic::getTerminalVelocity() const
{
    return terminal_velocity_;
}

}
