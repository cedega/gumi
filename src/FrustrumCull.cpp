#include "GUMI/FrustrumCull.h"

namespace gu
{

FrustrumCull::FrustrumCull() :
    initialized_(false),
    near_(0.0f),
    far_(0.0f),
    near_width_(0.0f),
    near_height_(0.0f)
{
}

void FrustrumCull::init(float angle, float ratio, float near, float far)
{
    near_ = near;
    far_ = far;

    near_height_ = near_ * static_cast<float>(std::tan(glm::radians(angle) * 0.5f)); 
    near_width_ = near_height_ * ratio;

    initialized_ = true;
}

void FrustrumCull::set(const glm::vec3 &p, const glm::vec3 &l, const glm::vec3 &u)
{
    if (!initialized_)
        return;

    glm::vec3 Z = glm::normalize(p - l);
    glm::vec3 X = glm::normalize(glm::cross(u, Z));
    glm::vec3 Y = glm::cross(Z, X);

    glm::vec3 near_centre = p - (near_ * Z);
    glm::vec3 far_centre = p - (far_ * Z);

    // NEAR
    planes_[0].set(-Z, near_centre);
    // FAR
    planes_[1].set(Z, far_centre);

    glm::vec3 aux, normal;

    // TOP
    aux = glm::normalize((near_centre + (near_height_ * Y)) - p);
    normal = glm::cross(aux, X);
    planes_[2].set(normal, near_centre + (near_height_ * Y));

    // BOTTOM
    aux = glm::normalize((near_centre - (near_height_ * Y)) - p);
    normal = glm::cross(X, aux);
    planes_[3].set(normal, near_centre - (near_height_ * Y));

    // LEFT
    aux = glm::normalize((near_centre - (near_width_ * X)) - p);
    normal = glm::cross(aux, Y);
    planes_[4].set(normal, near_centre - (near_width_ * X));

    // RIGHT
    aux = glm::normalize((near_centre + (near_width_ * X)) - p);
    normal = glm::cross(Y, aux);
    planes_[5].set(normal, near_centre + (near_width_ * X));
}

bool FrustrumCull::visible(const gu::AABB &aabb) const
{
    if (!initialized_)
        return true;

    for (int i = 0; i < 6; i++)
    {
        if (planes_[i].distance(aabb.getPositiveVertex(planes_[i].getNormal())) < 0.0f)
            return false;
    }

    return true;
}

}
