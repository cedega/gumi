#include "GUMI/Text.h"

namespace gu
{

Text::Text() :
    font_(NULL),
    zip_rw_(NULL),
    pure_2d_(true),
    texture_loaded_(false),
    loaded_font_(false),
    size_(16),
    zip_font_loaded_(false)
{
}

Text::Text(const Text &obj) :
    gu::Image(obj),
    font_(obj.font_),
    zip_rw_(NULL),
    pure_2d_(obj.pure_2d_),
    texture_loaded_(false),
    loaded_font_(false),
    size_(obj.size_),
    zip_font_buffer_(NULL),
    zip_font_loaded_(false)
{
    setString(obj.string_);
}

Text::~Text()
{
    deconstruct();
}

Text& Text::operator=(const Text &obj)
{
    gu::Image::operator=(obj);

    deconstruct();

    font_ = obj.font_;
    zip_rw_ = NULL;
    pure_2d_ = obj.pure_2d_;
    texture_loaded_ = false;
    loaded_font_ = false;
    size_ = obj.size_;
    zip_font_buffer_ = NULL;
    zip_font_loaded_ = false;

    setString(obj.string_);

    return *this;
}

void Text::deconstruct()
{
    if (loaded_font_)
    {
        TTF_CloseFont(font_);
    }
    
    if (texture_loaded_)
    {
        glDeleteTextures(1, &texture_);
    }

    if (zip_font_loaded_)
    {
        free(zip_font_buffer_);
        SDL_RWclose(zip_rw_);
    }

    font_ = NULL;
    zip_rw_ = NULL;
    pure_2d_ = true;
    texture_loaded_ = false;
    zip_font_loaded_ = false;
    loaded_font_ = false;
}

void Text::loadZipFont(const std::string &file_path, gu::Shader *shader, int character_size, bool pure_2D)
{
    deconstruct();

    if (shader != NULL)
        bindShader(shader);
    
    gu::Zip zip(file_path);
    zip_font_buffer_ = zip.getBuffer();

    zip_rw_ = SDL_RWFromConstMem(zip.getBuffer(), zip.getSize());
    font_ = TTF_OpenFontRW(zip_rw_, 0, character_size);
    size_ = character_size;

    if (font_ == NULL)
        SDL_Log("[GUMI] (Text) Error: Could not load \"%s\"", file_path.c_str());
    
    pure_2d_ = pure_2D;
    setString(string_);

    zip_font_loaded_ = true;
    loaded_font_ = true;
}

void Text::loadFileFont(const std::string &file_path, gu::Shader *shader, int character_size, bool pure_2D)
{
    if (gu::Zip::getRedirectFileLoading())
    {
        loadZipFont(file_path, shader, character_size, pure_2D);
        return;
    }

    deconstruct();

    if (shader != NULL)
        bindShader(shader);
    
    font_ = TTF_OpenFont(file_path.c_str(), character_size);
    size_ = character_size;
    
    if (font_ == NULL)
        SDL_Log("[GUMI] (Text) Error: Could not load \"%s\"", file_path.c_str());
    
    pure_2d_ = pure_2D;
    setString(string_);
    
    zip_font_loaded_ = false;
    loaded_font_ = true;
}

// Setters
    
int po2(int x)
{
    float logbase2 = std::log(static_cast<float>(x)) / std::log(2.0f);
    return round(std::pow(2, std::ceil(logbase2)));
}

void Text::setFont(gu::Text *text)
{
    deconstruct();
    font_ = text->getFont();
    size_ = text->getCharacterSize();
    bindShader(text->getShader());
}

void Text::setString(const std::string &string)
{
    if (font_ == NULL)
        return;
    
    string_ = string;
    SDL_Color c;
    c.r = 255;
    c.g = 255;
    c.b = 255;
    c.a = 255;

    SDL_Surface *surface = TTF_RenderText_Blended(font_, string_.c_str(), c);

    if (surface == NULL)
        return;

    int old_w = surface->w;
    int old_h = surface->h;
    
    int w = po2(surface->w);
    int h = po2(surface->h);
    
    SDL_Surface *intermediate = SDL_CreateRGBSurface(0, w, h, 32, 0x00ff0000, 0x0000ff00, 0x000000ff, 0xff000000);

    SDL_BlitSurface(surface, NULL, intermediate, NULL);
    SDL_FreeSurface(surface);

    if (texture_loaded_)
        glDeleteTextures(1, &texture_);

    GLenum old_filter = gu::TextureLoader::getTextureFilter();
    gu::TextureLoader::setTextureFilter(GL_LINEAR);
    texture_ = gu::TextureLoader::loadSurface(intermediate); // automatically frees surface
    texture_loaded_ = true;
    gu::TextureLoader::setTextureFilter(old_filter);

    gu::Image::setTexture(texture_, w, h, pure_2d_);
    gu::Image::setTextureRect(glm::vec4(0, 0, old_w, old_h));
}

// Getters

int Text::getCharacterSize() const
{
    return size_;
}

const std::string& Text::getString() const
{
    return string_;
}

TTF_Font* Text::getFont()
{
    return font_;
}

}
