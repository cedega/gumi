SOURCES=compile/examples/helloworld/main.cpp
INCLUDE=include/
CFLAGS=-g -O3
FRAMEWORKS=

ifeq ($(OS),Windows_NT)
	MESSAGE="Operating System Detected: Windows"
	COMPILE=compile/windows/
	LIBS=-lgumi -lmingw32 -lSDL2main -lSDL2 -lSDL2_image -lSDL2_mixer -lSDL2_ttf -lSDL2_net -lassimp -lglew32 -lopengl32 -lminizip -lbz2 -lz -lm -Wall
	BINARY=binaries/windows/GUMI.exe
else ifeq ($(shell uname), Darwin)
	MESSAGE="Operating System Detected: Mac OSX"
	COMPILE=compile/mac/
	LIBS=-lgumi -framework SDL2 -framework SDL2_image -framework SDL2_mixer -framework SDL2_ttf -framework SDL2_net -lminizip -lz
	FRAMEWORKS="-Fcompile/mac/"
	BINARYbinaries/mac/GUMI
else
	MESSAGE="Operating System Detected: Linux"
	COMPILE=compile/linux/
	LIBS=-lgumi -lSDL2 -lSDL2_image -lSDL2_mixer -lSDL2_ttf -lassimp -lGLEW -lGLU -lGL -lminizip -lz -lm -Wall
	BINARY=binaries/linux/GUMI
endif

srcs = $(wildcard src/*.cpp)
objs = $(srcs:.cpp=.o)
deps = $(srcs:.cpp=.d)
LIBNAME=libgumi.a
OBJECTS=src/*.o

all: lib bin

bin:
	@echo $(MESSAGE)
	g++ $(CFLAGS) $(SOURCES) -I"$(INCLUDE)" -L"$(COMPILE)" $(FRAMEWORKS) $(LIBS) -o $(BINARY)

lib: $(objs)
	@echo $(MESSAGE)
	ar rcs $(COMPILE)$(LIBNAME) $(OBJECTS)

%.o: %.cpp
	g++ $(CFLAGS) -I"$(INCLUDE)" -L"$(COMPILE)" -MMD -MP -Wall -c $< -o $@

clean:
	rm -f src/*.o src/*.d

-include $(deps)
