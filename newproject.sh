#! /bin/bash

# ============================
# Creates a new GUMI project
# ============================
# Version 1.1

DIR=$(dirname $0)

if [[ "$DIR" != "." ]]; then
    echo "Please run the script from the GUMI root folder."
    exit 1
fi

if [[ -z "$1" ]]; then
    echo "Please supply a project name as the first argument."
    exit 1
fi

# Create Necessary Directories
mkdir -p ../$1/compile/android/assets
mkdir -p ../$1/compile/android/jni/assimp
mkdir -p ../$1/compile/android/jni/GUMI
mkdir -p ../$1/compile/android/jni/minizip
mkdir -p ../$1/compile/android/jni/SDL
mkdir -p ../$1/compile/android/jni/SDL_image
mkdir -p ../$1/compile/android/jni/SDL_mixer
mkdir -p ../$1/compile/android/jni/SDL_ttf
mkdir -p ../$1/compile/android/jni/src
mkdir -p ../$1/compile/ios/src
mkdir -p ../$1/src
mkdir -p ../$1/assets
mkdir -p ../$1/binaries/windows
mkdir -p ../$1/binaries/linux/lib64
mkdir -p ../$1/binaries/mac

# Sample main file
cp -r compile/examples/helloworld/main.cpp ../$1/src

# Android Compile
cp -r compile/android/jni/*.mk ../$1/compile/android/jni
cp -r compile/android/jni/src/*.mk ../$1/compile/android/jni/src

cp -r compile/android/res ../$1/compile/android
cp -r compile/android/src ../$1/compile/android
cp -r compile/android/*.xml ../$1/compile/android
cp -r compile/android/*.properties ../$1/compile/android
cp -r compile/android/*.sh ../$1/compile/android

rm ../$1/compile/android/install.sh
mv ../$1/compile/android/install-newproject.sh ../$1/compile/android/install.sh

# iOS Compile
cp -r compile/ios/*.* ../$1/compile/ios/
cp -r compile/ios/GUMI.xcodeproj ../$1/compile/ios/
cp -r compile/ios/minizip ../$1/compile/ios/

mv ../$1/compile/ios/install-newproject.sh ../$1/compile/ios/install.sh

# Linux Run Script
cp -r binaries/linux/gumi.sh ../$1/binaries/linux/
cp -r binaries/linux/lib64/*.so* ../$1/binaries/linux/lib64/

# Configs
cp .gitignore ../$1/
cp compile/Makefile-newproject ../$1/Makefile

# Auto-run installs
cd ../$1/compile/android/
chmod +x install.sh
./install.sh

cd ../ios/
chmod +x install.sh
./install.sh

echo "Project \"$1\" finished."
