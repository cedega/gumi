#include <GUMI/Engine.h>

class Engine : public gu::Engine
{
 public:
    Engine()
    {
        initWindow("GUMI", 800, 600, 0);
    }

    void events(const SDL_Event &event)
    {
        if (event.type == SDL_QUIT)
            gu::Engine::kill();
    }

    void logic(const float dt)
    {

    }

    void render(const float interpolation, const float frame_time)
    {

    }

 private:

};

int main(int argc, char *argv[])
{
    Engine gumi;
    gumi.runGameLoop();
    return EXIT_SUCCESS;
}
