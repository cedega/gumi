#! /bin/bash
adb shell stop
adb shell setprop log.redirect-stdio true
adb shell start

echo "Press Enter when the Android device has properly restarted."
read input

adb logcat -s "stdout","stderr"
