#! /bin/bash

# Check for correct run path
DIR=$(dirname $0)

if [[ "$DIR" != "." ]]; then
    echo "Please run the script from the 'android' folder."
    exit 1
fi

cd jni/

# Link GUMI include folder
rm -rf include
ln -s ../../../../gumi/include include

# Link GUMI android folder
rm -rf GUMI
ln -s ../../../../gumi/compile/android/jni/GUMI GUMI

# Link assimp android folder
rm -rf assimp
ln -s ../../../../gumi/compile/android/jni/assimp assimp

# Link minizip android folder
rm -rf minizip
ln -s ../../../../gumi/compile/android/jni/minizip minizip

# Link SDL android folder
rm -rf SDL
ln -s ../../../../gumi/compile/android/jni/SDL SDL

# Link SDL_image android folder
rm -rf SDL_image
ln -s ../../../../gumi/compile/android/jni/SDL_image SDL_image

# Link SDL android folder
rm -rf SDL_mixer
ln -s ../../../../gumi/compile/android/jni/SDL_mixer SDL_mixer

# Link SDL android folder
rm -rf SDL_ttf
ln -s ../../../../gumi/compile/android/jni/SDL_ttf SDL_ttf

# Link main folder
cd src
rm -rf src
ln -s ../../../../src src

# Link assets folder
cd ../../
rm -rf assets
ln -s ../../assets assets

echo "Android Project Installation complete."
