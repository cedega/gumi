#! /bin/bash

# Check for correct run path
DIR=$(dirname $0)

if [[ "$DIR" != "." ]]; then
    echo "Please run the script from the 'android' folder."
    exit 1
fi

cd jni/

# Link GUMI include folder
rm -f include
ln -s ../../../include/ include

# Link GUMI src folder
cd GUMI/
rm -f src
ln -s ../../../../src/ src

echo "Android Installation complete."
echo "Please copy the source files to the appropriate jni/ directories."
