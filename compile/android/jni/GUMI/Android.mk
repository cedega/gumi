LOCAL_PATH := $(call my-dir)

include $(CLEAR_VARS)

LOCAL_MODULE := GUMI
LOCAL_C_INCLUDES := $(LOCAL_PATH)/../include
LOCAL_CFLAGS += -O3
LOCAL_SRC_FILES := $(subst $(LOCAL_PATH)/,,$(wildcard $(LOCAL_PATH)/src/*.cpp))
LOCAL_SHARED_LIBRARIES := SDL2 SDL2_image SDL2_ttf SDL2_mixer assimp minizip libstlport
LOCAL_LDLIBS := -lGLESv2 -lEGL

include $(BUILD_SHARED_LIBRARY)
