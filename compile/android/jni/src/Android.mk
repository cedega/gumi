LOCAL_PATH := $(call my-dir)

include $(CLEAR_VARS)

LOCAL_MODULE := main

SDL_PATH := ../SDL
LOCAL_C_INCLUDES := $(LOCAL_PATH)/../include
LOCAL_CFLAGS += -Os
# Add your application source files here...
LOCAL_SRC_FILES := $(SDL_PATH)/src/main/android/SDL_android_main.c \
	$(subst $(LOCAL_PATH)/,, \
	$(wildcard $(LOCAL_PATH)/src/*.cpp))

LOCAL_SHARED_LIBRARIES := GUMI SDL2 SDL2_image SDL2_ttf SDL2_mixer assimp minizip
LOCAL_LDLIBS := -lGLESv1_CM -lGLESv2 -llog -lEGL

include $(BUILD_SHARED_LIBRARY)
