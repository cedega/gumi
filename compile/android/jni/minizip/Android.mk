LOCAL_PATH := $(call my-dir)

include $(CLEAR_VARS)

LOCAL_MODULE := minizip
LOCAL_CFLAGS += -Dz_crc_t='unsigned int' -DUSE_FILE32API
LOCAL_SRC_FILES := ioapi.c mztools.c unzip.c zip.c
LOCAL_LDLIBS := -lz

include $(BUILD_SHARED_LIBRARY)
