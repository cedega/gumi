#! /bin/bash

# Check for correct run path
DIR=$(dirname $0)

if [[ "$DIR" != "." ]]; then
    echo "Please run the script from the 'ios' folder."
    exit 1
fi

# Link main folder
rm -rf src
ln -s ../../src src

echo "iOS Project Installation complete."
