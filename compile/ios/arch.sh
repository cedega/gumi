#! /bin/bash

if [ "$1" == "sim" ] 
then
    rm SDL/lib/libSDL2*.a
    cp SDL/lib/simulator/*.a SDL/lib/
    echo "Set archiecture to: Simulator"

elif [ "$1" == "armv7" ] 
then
    rm SDL/lib/libSDL2*.a
    cp SDL/lib/armv7/*.a SDL/lib/
    echo "Set archiecture to: armv7"

else
    echo "Error: no architecture specified"
fi

