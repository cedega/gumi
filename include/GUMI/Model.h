#pragma once
#include "Drawable.h"
#include "AnimatedTextureList.h"
#include "Armature.h"
#include "IOSystem.h"
// #include <assimp/DefaultLogger.hpp>

namespace gu
{

//! 3D Model class

//!----------------------
//! Overview
//!----------------------
//!
//! gu::Model handles the loading and drawing of animated and non-animated 3D Models.
//!
//! While all entities or moving objects should be made with gu::Entity, gu::Model should be used for all static objects.
//!
//! Every gu::Model contains a vector of gu::AnimatedTextureList.
class Model : public gu::Drawable
{
 public:
    Model();
    //!
    //! Copy constructor.
    //!
    //! It will copy any position, scaling, rotations, and settings (including the bounding AABB calcuated by setGenerateAABB()),
    //! but will not actually copy the internal mesh.
    Model(const Model &obj);
    virtual ~Model();
    //!
    //! Assignment operator.
    //!
    //! It will copy any position, scaling, rotations, and settings (including the bounding AABB calcuated by setGenerateAABB()),
    //! but will not actually copy the internal mesh.
    Model& operator=(const Model &obj);

    //!
    //! Resets gu::Model internal data, and calls the deconstructor.
    void unload();

    //!
    //! Increments animations done by a gu::Armature.
    void updateAnimations(const float frame_time);
    //!
    //! Draws the model based on <b>interpolation</b> as the interpolation factor.
    //!
    //! If the object is static, <b>interpolation</b> can be left as 1.0f.
    void draw(const float interpolation=1.0f);

    //!
    //! Binds a gu::Shader to the gu::Model. The gu::Model will store <b>shader</b> as a reference, and use it every draw() call.
    //!
    //! This is a mandatory call unless a gu::Shader is passed as an argument to one of the load* functions.
    void bindShader(gu::Shader *shader);
    //!
    //! Binds a gu::Armature to the gu::Model. The gu::Model will store <b>armature</b> as a reference, and use it every draw() call.
    //!
    //! This is a mandatory call for skeletal animation. If no animation is to be used, this can be left alone.
    //!
    //! In order for the binding of an armature to be successful, this function must be called <b>before</b> loading a model via load*Model().
    void bindArmature(gu::Armature *armature);
    //!
    //! Manually load a texture into the vector of textures.
    //!
    //! This is almost always unneeded, because by default a gu::Model will automatically load textures.
    void loadTexture(GLuint texture);
    //!
    //! Loads the gu::Model from a file.
    //!
    //! If <b>shader</b> isn't NULL, then bindShader() will be automatically called on <b>shader</b>.
    //! If <b>auto_load_textures</b> is asserted, then textures will be automatically loaded from the same directory as the file.
    bool loadFileModel(const std::string &file_path, gu::Shader *shader=NULL, bool auto_load_textures=true);
    //!
    //! Loads the gu::Model from a zip file.
    //!
    //! If <b>shader</b> isn't NULL, then bindShader() will be automatically called on <b>shader</b>.
    //! If <b>auto_load_textures</b> is asserted, then textures will be automatically loaded from the same directory as the file.
    bool loadZipModel(const std::string &zip_path, gu::Shader *shader=NULL, bool auto_load_textures=true);

    //!
    //! Updates the last position of the gu::Model.
    //!
    //! Is automatically called by gu::Entity, and gu::Entity should be used for all moving gu::Model.
    //!
    //! However, in case that's not attractive, in order to properly interpolate the gu::Model, call this before gu::Model::setPosition().
    void updateLastPosition();
    
    // Static
    
    //!
    //! Sets the upwards orientation of all gu::Models.
    //!
    //! Because Blender exports with the Z-axis as up, this value is default true.
    //! However, if loading models from Maya, set this to false.
    static void setAllZUp(bool);
    static bool getAllZUp();

    // Setters
    
    //!
    //! Sets the position of the gu::Model.
    //!
    //! By asserting <b>snap</b>, the gu::Model will teleport to the given position and skip interpolation.
    //!
    //! <b>snap</b> should be asserted in situations like spawning a gu::Model or teleporting.
    void setPosition(const glm::vec3 &position, bool snap=false);
    void setScaling(const glm::vec3 &scaling);
    void setRotation(const glm::vec3 &rotation);
    //!
    //! Sets the colour modification of the gu::Model.
    void setColour(const glm::vec4 &colour);
    //!
    //! Manually set the last position for interpolation. This should be unneeded for most users, and is effectively updateLastPosition() but with more control.
    void setLastPosition(const glm::vec3 &last_position);
    //!
    //! Sets whether backface culling will be used for the gu::Model. If the gu::Model has single planes (for example, hair planes), then
    //! set this to false. By default, this is true.
    void setBackfaceCulling(bool);
    //!
    //! If this option is set before loading a gu::Model, then an expensive gu::AABB generation algorithm will be run to accurately determine the
    //! model's bounding box. This is incredibly useful for static models, and the result is stored in getModelAABB() / getModelYOBB(), which can then be tested via
    //! gu::Camera::visible() for culling.
    void setGenerateAABB(bool);
    //!
    //! Sets whether this single model has Z-axis as up. This overrides setAllZUp() if asserted.
    //! The default is true.
    void setZUp(bool);

    // Getters

    //!
    //! Returns the shader that is bound to the gu::Model.
    gu::Shader* getShader();
    //!
    //! Returns the armature that is bound to the gu::Model.
    gu::Armature* getArmature();
    //!
    //! Returns the file name without extension of the loaded model.
    const std::string& getFileName() const;
    //!
    //! Returns a vector of the names of all the textures used by the model.
    std::vector<std::string>* getTextureNames();
    const glm::vec3& getPosition() const;
    const glm::vec3& getScaling() const;
    const glm::vec3& getRotation() const;
    const glm::vec4& getColour() const;
    const glm::vec3& getLastPosition() const;
    bool getBackfaceCulling() const;
    //!
    //! Returns the new position given an interpolation. This can be used to determine the actual position of the gu::Model in gu::Engine::render().
    glm::vec3 getInterpolatedPosition(const float interpolation) const;
    //!
    //! Returns the bounding gu::AABB for the model. Only available if setGenerateAABB() was asserted.
    gu::AABB getModelAABB() const;
    //!
    //! Returns the bounding gu::YOBB for the model. Only available if setGenerateAABB() was asserted.
    //! This is simply just a rotation of the gu::AABB calculated in setGenerateAABB(), and is not guarenteed to be a minimal bounding box.
    gu::YOBB getModelYOBB() const;
    bool getGenerateAABB() const;
    //!
    //! Returns the list of gu::AnimatedTexture's given the name of the default texture.
    //!
    //! For example, passing "face.png" will retrieve a list of gu::AnimatedTexture's for "face.png". This allows us
    //! to insert more animations for "face.png", and use gu::AnimatedTextureList::change() to cycle between them.
    gu::AnimatedTextureList* getAnimatedTextureList(const std::string &first_texture_name);
    //!
    //! Returns whether this single model has Z-axis as up.
    bool getZUp() const;

 protected:
    gu::Shader *shader_;

 private:
    static bool z_up_;
    int local_z_up_;

    gu::Armature *armature_;

    std::string file_name_;
    bool model_loaded_;
    bool has_textures_;
    bool loaded_vao_;
    std::vector<gu::AnimatedTextureList> textures_;
    bool backface_culling_;

    std::vector<GLuint> vao_;
    std::vector<GLuint> vbo_;
    std::vector<GLuint> ebo_;
    std::vector<GLuint> ind_size_;
    std::vector<std::string> texture_names_;
    std::vector<GLuint> texture_numbers_;
    std::vector<GLuint> texcoord_locations_;
    std::vector<GLuint> bone_id_locations_;
    std::vector<GLuint> bone_weight_locations_;

    GLuint vPosition_;
    GLuint vTexCoord_;
    GLuint textureSample_;
    GLuint boneids_;
    GLuint weights_;

    glm::vec3 position_;
    glm::vec3 last_position_;
    glm::vec3 scaling_;
    glm::vec3 rotation_;
    glm::vec4 colour_;

    bool generate_aabb_;
    gu::AABB aabb_;

    void init();
    void deconstruct();
    bool loadObject(const std::string &object_path, const aiScene *scene);
    void autoLoadTextures(bool from_zip, const std::string &append="");
    void generateAABB(const aiScene *scene);
};

}
