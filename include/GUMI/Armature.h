#pragma once
#include "Animation.h"

namespace gu
{

enum AnimationLayer
{
    ANIM_BASE = 0,
    ANIM_OVERLOAD = 1
};

struct Bone
{
    std::string name;
    glm::mat4 offset;
    glm::mat4 transform;
    glm::mat4 world_transform;

    Bone *parent;
    std::vector<Bone*> children; 

    Bone(const std::string &n, Bone *p) : offset(1.0f), transform(1.0f)
    {
        name = n;
        parent = p;
    }
};

//! Bone structure of a gu::Model

//!----------------------
//! Overview
//!----------------------
//!
//! Handles the loading, updating and applying of a bone structure to a gu::Model.
//! Every gu::Model has a single gu::Armature as a component. When loading a model, if bones are detected,
//! then gu::Armature is utilized and used.
class Armature
{
 public:
    Armature(); 
    //!
    //! Copy constructor.
    //!
    //! Copies the internal bone structure. No animations or blending are copied because they're pointer-dependent.
    Armature(const Armature &obj);
    ~Armature();
    //!
    //! Assignment operator.
    //!
    //! Copies the internal bone structure. No animations or blending are copied because they're pointer-dependent.
    Armature& operator=(const Armature &obj);

    //!
    //! Loads an armature + necessary mesh to construct the bone structure used for animating.
    //!
    //! Use the copy constructor or assignment operator to perform an in-memory copy from an already-existing gu::Armature.
    bool loadFileArmature(const std::string &file_path);
    //!
    //! Loads an armature + necessary mesh to construct the bone structure used for animating from the zip filed defined by gu::Engine.
    //!
    //! Use the copy constructor or assignment operator to perform an in-memory copy from an already-existing gu::Armature.
    bool loadZipArmature(const std::string &zip_path);

    //!
    //! Deletes and reinitializes all internal gu::Armature data.
    void unload();

    //!
    //! Loads the supplied gu::Animation with a <b>blend_time</b>.
    //! By blending the animation, the gu::Model will interpolate its bones into
    //! the first frame of the animation over <b>blend_time</b> duration. If <b>blend_time</b> is 0, then
    //! no blending will occur.
    void loadAnimation(gu::Animation *anim, float blend_time=0.0f, gu::AnimationLayer index=ANIM_BASE);
    void removeOverloadedAnimation(float blend_time=0.0f);
    //!
    //! Returns whether the address on the <b>anim</b> argument matches the currently-loaded gu::Animation.
    bool isAnimating(const gu::Animation *anim, gu::AnimationLayer index=ANIM_BASE) const;

    // Getters

    //!
    //! Returns the current gu::Animation which is loaded.
    //! NULL is returned if there's no gu::Animation loaded.
    gu::Animation* getLoadedAnimation(gu::AnimationLayer index=ANIM_BASE);

    void update(const float ft);
    const std::vector<glm::mat4>* getTransforms() const;
    const std::vector<Bone*> *getBones() const;
    const std::vector<glm::ivec4>* getBoneIDs(unsigned int mesh_id) const;
    const std::vector<glm::vec4>* getBoneWeights(unsigned int mesh_id) const;

 private:
    Bone *root_;
    std::vector<Bone*> bones_;
    std::vector<glm::ivec4> *bone_ids_;
    std::vector<glm::vec4> *bone_weights_;
    std::vector<glm::mat4> transforms_;

    unsigned int number_of_meshes_;

    gu::Animation *loaded_animations_[2];
    unsigned int standard_channel_size_;
    bool error_displayed_;

    bool blending_[2];
    float blend_duration_[2];
    float blend_progress_[2];

    std::vector<aiQuaternion> last_interpolated_rotation_;
    std::vector<aiVector3D> last_interpolated_position_;
    std::vector<aiVector3D> last_interpolated_scaling_;

    std::vector<aiQuaternion> last_blend_interpolated_rotation_;
    std::vector<aiVector3D> last_blend_interpolated_position_;
    std::vector<aiVector3D> last_blend_interpolated_scaling_;

    std::vector<aiQuaternion> storage_interpolated_rotation_;
    std::vector<aiVector3D> storage_interpolated_position_;
    std::vector<aiVector3D> storage_interpolated_scaling_;

    std::vector<int> remove_blend_channels_;
    float remove_blend_duration_;
    float remove_blend_progress_;
    std::vector<aiQuaternion> remove_blend_rotation_;
    std::vector<aiVector3D> remove_blend_position_;
    std::vector<aiVector3D> remove_blend_scaling_;
    gu::Animation *remove_blend_animation_;

    //! Loads the bones from a scene.
    bool loadBones(const aiScene *scene);
    Bone* loadBoneTree(const aiNode *node, Bone *parent);
    void recursiveUpdate(Bone *bone);
    void calculateBoneWorldTransforms(Bone *bone);
    void assignVertices(const aiScene *scene);

    void init();
    void deconstruct();
    void processAnimation(gu::AnimationLayer index, const float ft);
    Bone* copyBoneTree(Bone *root, Bone *copy_parent);
};

}
