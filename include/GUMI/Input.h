#pragma once
#include <glm/gtc/matrix_transform.hpp>
#include <SDL2/SDL_log.h>
#include <SDL2/SDL_mouse.h>
#include <SDL2/SDL_keyboard.h>
#include <SDL2/SDL_gamecontroller.h>
#include <SDL2/SDL_events.h>
#include <vector>
#include <iostream>
#include <string>
#include <map>

namespace gu
{

typedef const Uint8* Keys;

struct Finger
{
    Uint32 type;
    int index;
    float x;
    float y;
    float dx;
    float dy;
};
    
class Engine;

//! Abstracted SDL input handling

//!----------------------
//! Overview
//!----------------------
//!
//! gu::Input removes a lot of the boilerplate code for SDL input handling, as well as streamlines functionality for inputting on mobile devices.
//!
//! Use gu::Input as frequently as possible over traditional SDL input handling.
class Input
{
public:
    friend class Engine;
    //!
    //! Returns the position of the mouse.
    static glm::vec2 getMousePosition();
    //!
    //! Returns whether the left mouse button is down or not.
    static bool getLeftMouseDown();
    //!
    //! Returns whether the right mouse button is down or not.
    static bool getRightMouseDown();
    //!
    //! Returns whether the middle mouse button is down or not.
    static bool getMiddleMouseDown();
    //!
    //! Returns all the touch locations on the screen. Currently experimental, and is used for mobile devices.
    static std::vector<gu::Finger>* getTouchFingers();
    //!
    //! Returns all the keys held. Can be querried as a bool via gu::Keys[SDL_SCANCODE_**KEY**].
    static gu::Keys getKeyboardState();
    //!
    //! Returns all controllers connected.
    static std::vector<SDL_GameController*>* getControllers();
    //!
    //! Returns whether a button on the controller is being held.
    static bool getControllerButton(SDL_GameController *pad, SDL_GameControllerButton button);
    //!
    //! Returns the normalized vector of the left analog stick.
    static glm::vec2 getControllerLeftAxis(SDL_GameController *pad);
    //!
    //! Returns the normalized vector of the right analog stick.
    static glm::vec2 getControllerRightAxis(SDL_GameController *pad);
    
private:
    Input() {};
    Input(const Input&);
    void operator=(const Input&);
    
    static glm::vec2 render_ratio_;
    static std::vector<gu::Finger> touch_fingers_;
    
    static void setRenderRatio(const glm::vec2 &ratio);

    // Controllers
    static std::vector<SDL_GameController*> controllers_;
    static std::map<int, SDL_GameController*> controller_mapping_;
    static void initControllers();
    static void addController(int id);
    static void removeController(int id);
};
    
}
