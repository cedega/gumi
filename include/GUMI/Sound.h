#pragma once
#include <SDL2/SDL_log.h>
#include <SDL2/SDL_mixer.h>
#include "Zip.h"

namespace gu
{

//! Sound Playing Class

//!----------------------
//! Overview
//!----------------------
//!
//! Plays sounds from zip or an on-disk file.
class Sound
{
 public:
    Sound();
    ~Sound();

    //!
    //! Loads a sound from the zip file specified by gu::Engine at <b>file_path</b>.
    void loadZipWAV(const std::string &file_path);
    //!
    //! Loads a regular sound from the disk at path <b>file_path</b>.
    void loadFileWAV(const std::string &file_path);
    
    //!
    //! Plays the current sound 1 time, on the first available channel.
    void play();
    //!
    //! Stops the current channel in which the sound effect is assigned to.
    void stop();
    
    // Setters
    void setVolume(int volume);
    
    // Getters
    int getVolume() const;
    
 private:
    DISALLOW_COPY_AND_ASSIGN(Sound);

    Mix_Chunk *chunk_;
    SDL_RWops *zip_rw_;

    char *zip_sound_buffer_;
    bool zip_used_;
    
    int channel_;
    int volume_;

    void deconstruct();
};

}
