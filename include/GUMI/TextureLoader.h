#pragma once
#include <vector>

#ifdef __APPLE__
    #include <OpenGLES/ES2/gl.h>
    #include <OpenGLES/ES2/glext.h>
#elif defined(__ANDROID__)
    #include <GLES2/gl2.h> 
    #include <GLES2/gl2ext.h>
#else
    #include <GL/glew.h>
#endif

#include "SDL2/SDL_image.h"
#include "Zip.h"

namespace gu
{

struct TextureInfo
{
    GLuint id;
    std::string name;
    int w;
    int h;
    int count;
};

//! Singleton OpenGL Texture Loading class

//!----------------------
//! Overview
//!----------------------
//!
//! gu::TextureLoader is responsible for loading RGBA images from on-disk or the zip file.
//!
//! When a texture is loaded, the result is cached and returned when the same texture name is loaded.
//!
//! For example, if we load "test.png", every consecutive load of "test.png" will simply return the cached GLuint of the first load.
//!
//! Caching, however, is only available when loading from a file.
class TextureLoader
{
 public:
    //!
    //! Loads a texture from on-disk.
    //!
    //! <b>cache</b> in enabled by default. If it's disabled, then it's the users's responsibility to properly free the returned texture. 
    //!
    //! If a texture exists in cache, this function will increment its reference count by 1.
    static GLuint loadFileTexture(const std::string &file_path, bool cache=true);
    //!
    //! Loads a texture from the zip file.
    //!
    //! <b>cache</b> in enabled by default. If it's disabled, then it's the users's responsibility to properly free the returned texture. 
    //!
    //! If a texture exists in cache, this function will increment its reference count by 1.
    static GLuint loadZipTexture(const std::string &zip_path, bool cache=true);
    //!
    //! Loads a texture from a SDL_Surface.
    //!
    //! The texture will be cached if and only if a <b>file_path</b> is supplied and the file name hasn't already been cached.
    //! If the <b>file_path</b> is not supplied, then it's the user's responsibility to free the texture.
    //! If the <b>file_path</b> already exists as a cached texture, 0 will be returned and no new texture will be created.
    //!
    //! If a texture exists in cache, this function will return 0.
    static GLuint loadSurface(SDL_Surface *surface, const std::string &file_path="");
    //!
    //! Loads a texture from an array of data, given a <b>width</b> and <b>height</b>.
    //!
    //! It's the user's responsibility to free the returned texture.
    static GLuint loadPixels(void *pixels, int width, int height);
    //!
    //! Delete <b>texture_id</b> and remove it from cache.
    static void deleteTexture(GLuint texture_id);
    //!
    //! Delete the texture with the name <b>texture_name</b> and remove it from cache.
    static void deleteTexture(const std::string &texture_name);
    //!
    //! Delete every texture in the cache.
    static void deleteAllTextures();
    //!
    //! Retrieve the texture's information corresponding to the id <b>texture_id</b> from cache.
    static gu::TextureInfo* getTextureInfo(GLuint texture_id);
    //!
    //! Retrieve the texture's information corresponding to the name <b>texture_name</b> from cache.
    static gu::TextureInfo* getTextureInfo(const std::string &texture_name);
    //!
    //! Sets the filter which to load textures with.
    //!
    //! For example, GL_NEAREST or GL_LINEAR. By default, GL_NEAREST is used.
    static void setTextureFilter(GLenum filter);
    //!
    //! Returns the filter which is currently being used.
    static GLenum getTextureFilter();
    //!
    //! Returns the number of textures that are being cached.
    static unsigned int getTextureCount();

 private:
    TextureLoader() {};
    TextureLoader(const TextureLoader&);
    void operator=(const TextureLoader&);

    static std::vector<gu::TextureInfo> textures_;
    static GLenum filter_;
};

}
