#pragma once
#include "Tools.h"
#include "Copy.h"

namespace gu
{
    struct Force
    {
        bool rerun;
        glm::vec3 velocity;
        glm::vec3 acceleration;
        int time;
        bool disable_gravity;

        Force()
        {
            rerun = true;
            time = 0;
            disable_gravity = false;
        }

        Force(const glm::vec3 &vel, const glm::vec3 &acc, int t)
        {
            rerun = true;
            velocity = vel;
            acceleration = acc;
            time = t;
            disable_gravity = false;
        }
    };

    enum COLLISION_ID
    {
        COL_NONE,
        COL_X_LEFT,
        COL_X_RIGHT,
        COL_Y_TOP,
        COL_Y_BOT,
        COL_Z_TOP,
        COL_Z_BOT
    };
};

namespace gu
{

//! Physics-driven EntityLogic

//!----------------------
//! Overview
//!----------------------
//!
//! Handles collision and integration.
//!
//! Is inherited by gu::Entity.
class EntityLogic
{
 public:
    EntityLogic();
    EntityLogic(const EntityLogic &obj);
    EntityLogic& operator=(const EntityLogic &obj);

    //!
    //! Integrates the EntityLogic's movement and updates the position.
    //!
    //! <b>rerun</b> dictates if the integration is to be done at the "catch-up" phase of a network game.
    //! if the game is not being played over a network, leave <b>rerun</b> as false.
    //!
    //! <b>NOTE: As of the current version, network play is not supported. "rerun" will be useless, so leave it alone.</b>
    void move(const float dt, bool rerun=false);

    //!
    //! Collides two AABB hitboxes. The first argument is the entity's own hitbox, and the
    //! second argument is the object to collide against. <b>hitbox</b>'s centre will be changed
    //! to reflect the new position.
    //!
    //! If using gu::Entity, this method is already implemented as gu::Entity::collide().
    gu::COLLISION_ID collide(gu::AABB *hitbox, const gu::AABB &object);
    //!
    //! Collides two YOBB hitboxes in a similar manner to the AABB collision.
    //!
    //! <b>mtv</b> is the vector to be subtraced from <b>hitbox</b>'s position to resolve the collision.
    //!
    //! <b>first_check</b> is a pointer to a gu::AABB which will be checked first before performing SAT.
    //! Note that passing a non-NULL value means that the next argument, <b>object_aabb</b> is required as well.
    //!
    //! If using gu::Entity, this method is already implemented as gu::Entity::collide().
    bool collide(const gu::YOBB &hitbox, const gu::YOBB &object, glm::vec3 *mtv, const gu::AABB *first_check=NULL, const gu::AABB *object_aabb=NULL);
     
    // Forces
    
    //!
    //! Creates a new force. <b>velocity</b> is applied for <b>time</b> amount of milliseconds.
    int addForce(const glm::vec3 &velocity, const glm::vec3 &acceleration, int time);
    //!
    //! Creates a new force by taking an acutal force construct.
    int addForce(const gu::Force &force);
    //!
    //! Removes the force at index <b>force_id</b>.
    void removeForce(int force_id);
    //!
    //! Removes all forces.
    void removeAllForces();
    //!
    //! Returns whether a force at <b>force_id</b> exists.
    bool forceExists(int force_id) const;
    //!
    //! Returns whether at least one force is present.
    bool anyForceExists() const;
    //!
    //! Returns whether a force has triggered gravity to temporarily no longer apply.
    bool forceHasDisabledGravity() const;
    //!
    //! Takes an index of a force and disables the act of using it in the "catch-up" step in network games.
    //! Returns the same <b>force_id</b> passed.
    //!
    //! <b>NOTE: As of the current version, network play is not supported. "rerun" will be useless, so leave it alone.</b>
    int applyNoRerun(int force_id);
    //!
    //! Takes an index of a force and disables all gravity affecting the EntityLogic for the force's duration.
    //! Returns the same <b>force_id</b> passed.
    int applyNoGravity(int force_id);

    // Triggers
    
    //!
    //! Executes every time an force ends.
    //! Passes a <b>force_id</b>, which is the index of the force.
    virtual void onForceEnd(int force_id);
    //!
    //! Executes every time the EntityLogic hits the ground.
    virtual void onLand();

    // Static
    
    //!
    //! Sets the default gravity used for all entities.
    static void setDefaultGravity(float); 
    //!
    //! Sets the default terminal velocity used for all entities.
    static void setDefaultTerminalVelocity(const glm::vec3 &terminal_velocity);
    //!
    //! Returns the default gravity used for all entities.
    static float getDefaultGravity();
    //!
    //! Returns the default terminal velocity used for all entities.
    static glm::vec3 getDefaultTerminalVelocity();

    // Setters
    
    void setX(float);
    void setY(float);
    void setZ(float);
    void setPosition(const glm::vec3 &position);
    void setVelocity(const glm::vec3 &velocity);
    void setAcceleration(const glm::vec3 &acceleration);
    //!
    //! Sets whether gravity applies to this EntityLogic.
    void setIgnoreGravity(bool);
    void setGravity(float);
    void setTerminalVelocity(const glm::vec3 &terminal_velocity);

    // Getters

    float getX() const;
    float getY() const;
    float getZ() const;
    const glm::vec3& getPosition() const; 
    const glm::vec3& getVelocity() const;
    //!
    //! Returns the amount of movement done over a single move() call.
    const glm::vec3& getStep() const;
    const glm::vec3& getAcceleration() const;
    //!
    //! Returns the total combined force velocity applied to the EntityLogic.
    const glm::vec3& getForceVelocity() const;
    bool getIgnoreGravity() const;
    float getGravity() const;
    const glm::vec3& getTerminalVelocity() const;

 private:
    static float default_gravity_;
    static glm::vec3 default_terminal_velocity_;

    glm::vec3 position_;
    glm::vec3 velocity_;
    glm::vec3 acceleration_;
    glm::vec3 force_velocity_;
    glm::vec3 step_;

    bool use_default_gravity_;
    bool ignore_gravity_;
    float gravity_;
    glm::vec3 terminal_velocity_;

    std::vector<gu::Force> forces_;
    void applyForces(float dt, bool rerun=false);
};

}
