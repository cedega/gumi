#pragma once
#include <SDL2/SDL_rwops.h>
#include <assimp/scene.h>
#include <assimp/Importer.hpp>
#include <assimp/postprocess.h>
#include <glm/gtc/matrix_transform.hpp>
#include <iostream>
#include <vector>
#include "Zip.h"

namespace gu
{

struct Channel
{
    bool valid;
    std::string name;
    std::vector<aiVectorKey> positions;
    std::vector<aiQuatKey> rotations;
    std::vector<aiVectorKey> scalings;
};

//! Animating a gu::Armature 

//!----------------------
//! Overview
//!----------------------
//!
//! A gu::Animation handles the animating of a gu::Armature.
//!
//! This component only loads animations from model files (DAE/FBX), and will not redundantly reload mesh.
//!
//! A new gu::Animation must be created per unique animation. Therefore, even if a gu::Model is reused 100 times, 100
//! unique gu::Animation must created.
//!
//! gu::Animation is one of the few GUMI classes which are copy and assignable. Therefore, to reduce redundant loading,
//! simply use an assignment operator (=).
//!
//! After loading an animation, users can simply pass this object to gu::Model via gu::Model::loadAnimation() to use it.
class Animation
{
 public:
    Animation();
    Animation(const Animation &obj);
    Animation& operator=(const Animation &obj);

    //!
    //! Progress animations forward by <b>ft</b>. This is called by gu::Model as a part of its update() function.
    void tick(const float ft);
    //!
    //! If called, will only load the bone specified. Can be called multiple times to load more bones.
    //! Must be called before load*Animation. If left uncalled, every bone will load on default.
    void addBoneFilter(const std::string &bone_name);
    //!
    //! Load the animations from a given model file.
    bool loadFileAnimation(const std::string &file_path);
    //!
    //! Load the animations from a given model file via the zip file used by gu::Engine.
    bool loadZipAnimation(const std::string &zip_path);
    //!
    //! Load the animations from a given aiScene.
    bool loadScene(const aiScene *scene);

    //!
    //! Retrieve the <b>frame</b>, <b>last frame</b>, and <b>factor</b> based on the current tick.
    //! This is called and used by gu::Model automatically.
    void sample(unsigned int *frame, unsigned int *last_frame, float *factor);
    //!
    //! Restarts the current animation from frame 1, time 0.
    void restart();
    //!
    //! Returns whether the animation has stopped. Animations can only be stopped if looping is false.
    bool stopped() const;

    // Setters
    void setLooping(bool);
    void setTime(float);
    //!
    //! Sets the speed which the animation will be played at. For example, passing 2.0 will play the animation twice as fast.
    void setPlaybackSpeedFactor(float);

    // Getters
    const std::vector<Channel>& getChannels() const;
    bool getLooping() const;
    float getTime() const;
    float getDuration() const;
    float getPlaybackSpeedFactor() const;

 private:
    std::vector<Channel> channels_;
    std::vector<std::string> bone_filter_;
    float time_;
    float ticks_per_second_;
    float duration_;
    float playback_speed_factor_;
    bool looping_;
    bool stopped_;

    void sampleDAE(unsigned int *frame, unsigned int *last_frame, float *factor);
};

}
