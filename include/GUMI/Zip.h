#pragma once
#include <SDL2/SDL_log.h>
#include <iostream>
#include <string>
#include "minizip/unzip.h"
#include "Copy.h"

namespace gu
{

//! Zip File Opening & Data Loading

//!----------------------
//! Overview
//!----------------------
//!
//! gu::Zip extracts information from the specified Zip File, and loads it into memory for other GUMI classes to interpret.
//!
//! Due to the nature of sound files, however, the gu::Zip buffer must be free'd before gu::Zip goes out of scope or gets deleted.
//!
//! As a result, call free(getBuffer()) before deleting or scoping gu::Zip.
class Zip
{
 public:
    Zip();
    explicit Zip(const std::string &file_path);
    
    //!
    //! Loads a file at <b>file_path</b> from the zip.
    //! Refer to the data via getBuffer() and getSize().
    void loadFile(const std::string &file_path);

    // Static
    
    //!
    //! Sets the location of the zip file on disk to use.
    static void setZipPath(const std::string &file_path);
    //!
    //! Sets the password used to encrypt the zip file.
    //! Note that AES isn't support, and only ZipCrypto is.
    static void setZipPassword(const std::string &password);
    //!
    //! Will redirect all load*File functions from GUMI to use the zip version instead.
    //!
    //! This is meant to be used as a convenience to quickly alter between a debug and release state.
    static void setRedirectFileLoading(bool);
    //!
    //! Returns the value set by setRedirectFileLoading(). By default, this is false.
    static bool getRedirectFileLoading();

    // Getters
    
    //!
    //! Returns a pointer refering to the data loaded in memory.
    char* getBuffer();
    //!
    //! Returns the size of the data loaded in memory.
    uLong getSize();
    
 private:
    DISALLOW_COPY_AND_ASSIGN(Zip);

    std::string zip_name_;
    unzFile zip_;
    unz_file_info info_;
    char *buffer_;
    uLong size_;

    static std::string zip_path_;
    static std::string zip_password_;
    static bool redirect_file_loading_;
};

}
