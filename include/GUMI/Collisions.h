#pragma once
#include <algorithm>
#include <GUMI/DrawRect.h>
#include "Entity.h"

namespace gu
{

class Collisions
{
 public:
    Collisions();
    void draw();
    void loadFileCollisions(const std::string &file_path);
    void loadZipCollisions(const std::string &file_path);
    bool collide(gu::Entity *entity) const;

    void add(const gu::AABB &aabb);
    void clear();

    // Getters
    const std::vector<gu::AABB>& getAABBs() const;

 private:
    DISALLOW_COPY_AND_ASSIGN(Collisions);
    std::vector<gu::AABB> aabbs_;
    
    void processAABBs(const std::string &contents);
};

}
