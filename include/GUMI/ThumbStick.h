#include "Image.h"
#include "Input.h"

namespace gu
{

class ThumbStick
{
  public:
    ThumbStick();
    
    void loadTextures(GLuint front_texture_id, GLuint back_texture_id, gu::Shader *front_shader, gu::Shader *back_shader=NULL);
    
    void draw();
    glm::vec2 poll();
    void reset();
    
    // Setters
    void setActiveRadius(float);
    void setScalePercent(float percent, gu::Axis axis);
    void setPositionPercent(const glm::vec2 &position_percent);
    void setDistanceThreshold(float);

    // Getters
    float getActiveRadius() const;
    float getScalePercent(gu::Axis *axis=NULL) const;
    const glm::vec2& getPositionPercent() const;
    float getDistanceThreshold() const;
    
  private:
    DISALLOW_COPY_AND_ASSIGN(ThumbStick);
    
    gu::Image button_;
    gu::Image button_bg_;
    glm::ivec2 render_size_;
    
    float active_radius_;
    float distance_threshold_;

    int snap_index_;
    glm::vec2 pivot_;

    glm::vec2 last_snap_point_;
    float last_snap_distance_;

    void leash(const glm::vec2 &point, float distance);
};

}
