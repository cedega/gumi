#pragma once

#if (__APPLE__)
    #include <OpenGLES/ES2/gl.h>
    #include <OpenGLES/ES2/glext.h>
#elif defined(__ANDROID__)
    #include <GLES2/gl2.h> 
    #include <GLES2/gl2ext.h>
#else
    #include <GL/glew.h>
#endif

#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <map>
#include "Zip.h"
#include "Tools.h"

namespace gu
{

class Engine;

//! GLSL Shader class

//!----------------------
//! Overview
//!----------------------
//!
//! gu::Shader is the main class for loading, using, and modifying GLSL shaders.
//!
//! gu::Shader can compile GLSL source code, with proper error reporting, and can then use the modify() functions to modify GLSL uniforms.
//!
//! By default, gu::Shader creates a bunch of static default shaders to use.
//! Therefore, most shader use will be calling defaults like gu::Shader::SHADER_UI.
class Shader
{
 friend class Engine;

 public:
    Shader();
    ~Shader();

    //!
    //! Compiles vertex and fragment shaders as character arrays. This allows for shaders defined in the source code to be compiled.
    //! Returns true if the operation was successful.
    //!
    //! To allow the writing of universal shaders, <b>version</b> must be supplied. This will append "#version <b>version</b>" to the beginning of both the
    //! vertex and fragment shader. As a reminder, with OpenGL ES 2.0, passing "100" will be valid.
    bool compile(std::string vertex, std::string fragment, const std::string &version, const std::string &vertex_file_name="", const std::string &fragment_file_name="");

    //!
    //! Compiles a vertex and fragment shader from a file.
    //! Returns true if the operation was successful.
    //!
    //! See compile() for an explanation of <b>version</b>.
    bool loadFileShader(const std::string &vertex_file, const std::string &fragment_file, const std::string &version);
    //!
    //! Compiles a vertex and fragment shader from the zip file set by gu::Engine.
    //! Returns true if the operation was successful.
    //!
    //! See compile() for an explanation of <b>version</b>.
    bool loadZipShader(const std::string &zip_vertex_file, const std::string &zip_fragment_file, const std::string &version);
    //!
    //! Binds the shader. This is a wrapper around glUseProgram(), but will not rebind shaders if the same shader is already bound for a performance boost.
    void bind() const;
    //!
    //! Sets the uniform with the name <b>u</b> in the GLSL program to the value of the second argument.
    //! If <b>cache</b> is asserted, then the location of the uniform <b>u</b> will be stored for subsequent fast access.
    void modify(const std::string &u, int i, bool cache=true);
    //!
    //! Sets the uniform with the name <b>u</b> in the GLSL program to the value of the second argument.
    //! If <b>cache</b> is asserted, then the location of the uniform <b>u</b> will be stored for subsequent fast access.
    void modify(const std::string &u, float f, bool cache=true);
    //!
    //! Sets the uniform with the name <b>u</b> in the GLSL program to the value of the second argument.
    //! If <b>cache</b> is asserted, then the location of the uniform <b>u</b> will be stored for subsequent fast access.
    void modify(const std::string &u, const glm::vec2 &v, bool cache=true);
    //!
    //! Sets the uniform with the name <b>u</b> in the GLSL program to the value of the second argument.
    //! If <b>cache</b> is asserted, then the location of the uniform <b>u</b> will be stored for subsequent fast access.
    void modify(const std::string &u, const glm::vec3 &v, bool cache=true);
    //!
    //! Sets the uniform with the name <b>u</b> in the GLSL program to the value of the second argument.
    //! If <b>cache</b> is asserted, then the location of the uniform <b>u</b> will be stored for subsequent fast access.
    void modify(const std::string &u, const glm::vec4 &v, bool cache=true);
    //!
    //! Sets the uniform with the name <b>u</b> in the GLSL program to the value of the second argument.
    //! If <b>cache</b> is asserted, then the location of the uniform <b>u</b> will be stored for subsequent fast access.
    void modify(const std::string &u, const glm::mat4 &m, bool cahce=true); 
    //!
    //! Sets the uniform with the name <b>u</b> in the GLSL program to the value of the second argument.
    //! If <b>cache</b> is asserted, then the location of the uniform <b>u</b> will be stored for subsequent fast access.
    void modify(const std::string &u, const std::vector<glm::mat4> *m, bool cache=true);

    // Static
    
    //!
    //! Sets the major OpenGL version. This is automatically set by gu::Engine, and shouldn't be touched. If greater than two, VAOs will be used.
    static void setOpenGLVersion(int major);
    //!
    //! See: setOpenGLVersion()
    static int getOpenGLVersion();

    //!
    //! The default shader for a non-animated 3D Model.
    static Shader *SHADER_3D;
    //!
    //! The default shader for an animated 3D Model.
    static Shader *SHADER_3D_RIG;
    //!
    //! The default shader for a 2D Image in 3D space.
    static Shader *SHADER_2D;
    //!
    //! The default shader for a 2D Billboard Image in 3D space.
    static Shader *SHADER_2D_BILLBOARD;
    //!
    //! The default shader for a Particle System.
    static Shader *SHADER_PS;
    //!
    //! The defualt shader for a Particle System which uses textures.
    static Shader *SHADER_PS_TEXTURE;
    //!
    //! The default shader for a 2D Image in 2D space.
    static Shader *SHADER_UI;

    // Getters
    GLuint getProgram() const;
    GLuint getAttribLocation(const std::string &str) const;
    GLuint getUniformLocation(const std::string &str) const;

 private:
    DISALLOW_COPY_AND_ASSIGN(Shader);
    
    static int opengl_major_;
    static GLuint bound_shader_;

    bool loaded_;
    GLuint vertex_shader_;
    GLuint fragment_shader_;
    GLuint program_;

    std::map<std::string, GLuint> locations_;

    void deconstruct();
    bool compileShaders(const char *vertex, const char *fragment, const std::string &vertex_file_name, const std::string &fragment_file_name);

    static void initDefaultShaders();
    static void deleteDefaultShaders();
};

}
