#pragma once
#include "SDL2/SDL_ttf.h"
#include "Image.h"

namespace gu
{

//! Text drawing class

//!----------------------
//! Overview
//!----------------------
//!
//! A class which displays fonts using gu::Image and SDL_ttf.
//!
//! The class itself is just an extended gu::Image with methods for text.
class Text : public gu::Image
{
 public:
    Text();
    //!
    //! Copy constructor.
    //!
    //! It will reference the internal font of <b>obj</b> and call setString().
    Text(const Text &obj);
    ~Text();
    //!
    //! Assignment operator.
    //!
    //! It will reference the internal font of <b>obj</b> and call setString().
    Text& operator=(const Text &obj);

    //!
    //! Loads a font from the zip file defined in gu::Engine at the path <b>file_path</b>.
    void loadZipFont(const std::string &file_path, gu::Shader *shader=NULL, int character_size=16, bool pure_2D=true);
    //!
    //! Loads a font regularly from an on-disk file at path <b>file_path</b>.
    void loadFileFont(const std::string &file_path, gu::Shader *shader=NULL, int character_size=16, bool pure_2D=true);
    
    // Setters

    //!
    //! Sets the internal font to that of another gu::Text instead of using one of the load*Font functions.
    void setFont(gu::Text *text);
    //!
    //! Sets the string.  This is an expensive operation, which has to create a new surface and texture.
    //!
    //! If the internal string and the string argument are equal, this function will return early as an optimization technique.
    void setString(const std::string &string);
            
    // Getters
    
    //!
    //! Returns the character size of the font.
    int getCharacterSize() const;
    //!
    //! Returns the current string.
    const std::string& getString() const;
    //!
    //! Returns the internal font used by gu::Text.
    TTF_Font* getFont();
    
 private:
    TTF_Font *font_;
    SDL_RWops *zip_rw_;
    bool pure_2d_;
    bool texture_loaded_;
    bool loaded_font_;
    GLuint texture_;

    int size_;
    std::string string_;
    
    // Keeps track of zip memory data to free up later
    char* zip_font_buffer_;
    bool zip_font_loaded_;

    void deconstruct();
};

}
