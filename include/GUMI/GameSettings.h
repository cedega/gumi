#pragma once
#include <map>
#include <fstream>
#include <algorithm>
#include <vector>
#include <SDL2/SDL_rwops.h>
#include "Zip.h"

namespace gu
{

struct Category
{
    std::map<std::string, int> int_keys;
    std::map<std::string, float> float_keys;
    std::map<std::string, std::string> string_keys;
};

class GameSettings
{
 public:
    static void loadFileSettings(const std::string &file_path); 
    static void loadZipSettings(const std::string &file_path); 
    static void clear();

    static void addCategory(const std::string &category);
    static void addKey(const std::string &category, const std::string &key, int value);
    static void addKey(const std::string &category, const std::string &key, float value);
    static void addKey(const std::string &category, const std::string &key, const std::string &value);

    static int getIntKey(const std::string &category, const std::string &key);
    static float getFloatKey(const std::string &cateory, const std::string &key);
    static std::string getStringKey(const std::string &category, const std::string &key);

 private:
    GameSettings() {};
    GameSettings(const GameSettings&);
    void operator=(const GameSettings&);

    static std::map<std::string, Category> categories_;

    static void processLoad(const std::string &contents);
};

}
