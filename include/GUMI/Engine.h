#pragma once

#ifdef __APPLE__
    #include <OpenGLES/ES2/gl.h>
    #include <OpenGLES/ES2/glext.h>
#elif defined(__ANDROID__)
    #include <GLES2/gl2.h> 
    #include <GLES2/gl2ext.h>
#else
    #include <GL/glew.h>
#endif

#include "SDL2/SDL.h"
#include "SDL2/SDL_mixer.h"
#include "SDL2/SDL_ttf.h"

#if !defined(TARGET_OS_IPHONE) && !defined(__ANDROID__)
#include "SDL2/SDL_net.h"
#endif

#include "DrawRect.h"
#include "Image.h"
#include "Input.h"

namespace gu
{

//! The main entry point for GUMI applications 

//!----------------------
//! Overview
//!----------------------
//!
//! gu::Engine is the class which governs window, events, and gameloop creation.
//!
//! This class must be inherited to be used as events(), logic(), and render() are all pure virtual.
class Engine
{
 public:
    //!
    //! Creates the engine with no zip file specified.
    Engine();
    //!
    //! Creates the engine using the zip file specified at <b>zip_path</b> with ZipCrypto password <b>zip_password</b>.
    Engine(const std::string &zip_path, const std::string &zip_password);
    ~Engine();
    
    //!
    //! Creates a window with a <b>window_title</b> at size <b>render_width</b>X<b>render_height</b>.
    //!
    //! <b>msaa</b> is a the anti-aliasing factor. No check is done on the msaa factor, so if it exceeds the
    //! capability of the graphics card, the application will crash. This will be fixed in the future.
    //!
    //! This function also creates the OpenGL context, and initializes any static objects.
    void initWindow(const std::string &window_title, int render_width, int render_height, int msaa=0);
    
    //!
    //! Virtual callback for the SDL events loop. Certain events (especially regarding mobile events) are automatically reacted to by gu::Engine.
    //!
    //! However, any event that isn't handled internally is called through this function. Therefore, this function may run many times for every event.
    //!
    //! This function occurs strictly before logic() and render().
    virtual void events(const SDL_Event &event) = 0;
    //!
    //! Virtual callback for semi-fixed timestep logic. By default, <b>dt</b> is 1/60, but can be modified using setPhysicsDt().
    //!
    //! All movement, collision, and state updating should occur in this function. On slower computers, this function may run multiple times before a render() is called.
    virtual void logic(const float dt) = 0;
    //!
    //! Virtual callback for rendering. <b>interpolation</b> is the interpolation factor for the given frame.
    //!
    //! The interpolation factor is a value between 0 and 1, and dictates roughly how much "percent" the program is until the next logic() call.
    //! By passing <b>interpolation</b> to objects which take it as a draw parameter, linear interpolation is applied to objects to smooth them out.
    //!
    //! The last argument is <b>frame_time</b>, which is the time difference between this render() and the last.
    virtual void render(const float interpolation, const float frame_time) = 0;

    //!
    //! Starts the gameloop. Must be called after initializing any data. The gameloop can only be killed using kill().
    void runGameLoop();
    //!
    //! Kills the gameloop, and shuts down the GUMI application.
    void kill();
    //!
    //! Resets the internal accumulator. Only useful for certain networking applications.
    void resetAccumulator();
    //!
    //! Disables logic() and render().
    //!
    //! This is automatically asserted when mobile applications go into the background to avoid being terminated.
    //!
    //! When disabled, the game loop will run with a 100 ms delay to reduce CPU usage.
    void disable();
    //!
    //! Enables logic() and render().
    //!
    //! This is automatically asserted when mobile applications resume from the background.
    void enable();

    // Setters

    //!
    //! Enables fullscreen. Currently it's advised to only fullscreen applications with identical window to monitor sizes.
    //!
    //! For example, if playing on a 1080p monitor, make sure initWindow() was initialized with 1920x1080 as a size and then call this function.
    void setFullScreen(bool);
    //!
    //! Manually set the internal accumulator. Only useful for certain networking applications.
    void setLogicAccumulator(float f);
    //!
    //! Sets the random seed. This is a wrapper around srand(), but saves the seed to it can be distributed to other applications, say, in a network.
    void setSeed(unsigned int seed);
    //!
    //! Sets the semi-fixed timestep rate. By default, this is 1/60. For some applications, this can even be as low as 1/20.
    //! The bigger the dt, the less frequently logic will be run and hence the faster GUMI applications will run.
    void setPhysicsDt(float dt);
    //!
    //! Limits the frames per second rate of the game to <b>fps</b>.
    void setFPSLimit(int fps);
    //!
    //! Enables or disables vertical sync. If using setFPSLimit(), this should probably be disabled.
    //!
    //! Will return true if vertical sync was successfully enabled, and false if it was not.
    bool setVerticalSync(bool);
    //!
    //! Sets the title of the window made by initWindow().
    void setWindowTitle(const std::string &title);
    //!
    //! Sets the window icon based on an image in the zip file.
    void setWindowIcon(const std::string &path_in_zip);
    //!
    //! Sets the clear colour of the window. This is a wrapper around glClearColor().
    void setClearColour(float r, float g, float b, float a=1.0f);

    // Getters
    bool getFullScreen() const;
    //!
    //! Returns the time between the last and current render().
    float getFrameTime() const;
    float getLogicAccumulator() const;
    unsigned int getSeed() const;
    float getPhysicsDt() const;
    int getFPSLimit() const;
    bool getVerticalSync() const;
    //!
    //! Returns the rendering size of the window. This can be different from getWindowSize, especially regarding high-DPI displays.
    const glm::ivec2& getRenderSize() const;
    //!
    //! Returns the size of the actual window.
    glm::ivec2 getWindowSize() const;
    //!
    //! Returns the ratio of the actual window.
    glm::vec2 getWindowRatio() const;
    //!
    //! Returns the FPS of the gameloop.
    unsigned int getFPS() const;
    //!
    //! Returns the pointer to the SDL_Window which was made using initWindow().
    SDL_Window* getWindow();
    //!
    //! Returns a projection matrix which is orthogonal and the same size as the window.
    //!
    //! This is meant for any purely 2D drawing.
    //!
    //! gu::Shader::SHADER_UI automatically uses this.
    const glm::mat4& getPerspective2D() const;
    //!
    //! Returns the aspect ratio of the window created by initWindow().
    float getAspectRatio() const;
    
 private:
    DISALLOW_COPY_AND_ASSIGN(Engine);

    // Engine Data
    bool alive_;
    float frame_time_;
    float accumulator_;
    bool reset_accumulator_;
    float dt_;
    int fps_limit_;
    bool vsync_;
    unsigned int seed_;
    bool fullscreen_;
    glm::mat4 perspective_2d_;
    float aspect_ratio_;
    bool enabled_;
    unsigned int frame_count_;

    // Window & Events
    SDL_Window *window_;
    SDL_GLContext context_;

    // Window Data
    std::string window_title_;
    glm::ivec2 render_size_;
    
    void init();
    void initOpenGL();
    void delay(Uint32 new_time);
};

}
