#pragma once
#include "AnimatedTexture.h"

namespace gu
{

//! A list of gu::AnimatedTexture

//!----------------------
//! Overview
//!----------------------
//!
//! Creates a list of gu::AnimatedTexture for a gu::Model.
//!
//! For example, one may want a happy, sad, and neutral texture for the character's face.
//! A single gu::AnimatedTextureList exists for the "face" texture, but it will hold 4 different gu::AnimatedTexture:
//!
//! - default (a single animation consisting of 1 frame -- the original texture)
//! - happy
//! - sad
//! - neutral
//!
//! Therefore, a gu::AnimatedTextureList exists for each texture of a single model, and governs a set of
//! gu::AnimatedTexture.
class AnimatedTextureList
{
 public:
    AnimatedTextureList();
    explicit AnimatedTextureList(const gu::AnimatedTexture &at);

    //!
    //! Adds a gu::AnimatedTexture object with an assigned <b>animation_name</b>. This name can then be
    //! referenced in change().
    void add(const std::string &animation_name, const gu::AnimatedTexture &at);
    //!
    //! Changes the currently-displaying animation to the first gu::AnimatedTexture with the name <b>animation_name</b>.
    void change(const std::string &animation_name, bool reset=true);
    //!
    //! Changes the currently-displaying animation to whichever index <b>index</b> points to.
    void change(int index, bool reset=true);

    // Getters
    gu::AnimatedTexture* getCurrentAnimatedTexture();
    std::vector<gu::AnimatedTexture>* getAnimatedTextures();

 private:
    std::vector<gu::AnimatedTexture> animated_textures_;
    std::vector<std::string> animated_texture_names_;
    int selected_index_;
};

}
