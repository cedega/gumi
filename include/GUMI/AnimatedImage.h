#pragma once
#include "Image.h"
#include "AnimatedTexture.h"
#include <map>

namespace gu
{

//! Animating a series of gu::Image 

//!----------------------
//! Overview
//!----------------------
//!
//! Handles the animation of many independent images.
//! For example, animating between 1.png, 2.png, and 3.png with a timing of 3 seconds each.
//!
//! Acts just like a gu::Image, but many textures must be added through addTexture().
class AnimatedImage : public gu::Image
{
 public:
    AnimatedImage();
    AnimatedImage(const AnimatedImage &obj);
    AnimatedImage& operator=(const AnimatedImage &obj);

    void draw(const float interpolation=1.0f, bool pure_2D=true);
    //!
    //! Adds the texture to a list of textures to be animated, with a period of <b>time</b>.
    void addTexture(GLuint texture, float time);
    void update(const float dt);

    gu::AnimatedTexture* getAnimatedTexture();

 private:
    gu::AnimatedTexture animated_texture_;
    std::map<GLuint, gu::TextureInfo> texture_infos_;
};

}
