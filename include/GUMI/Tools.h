#pragma once
#include <SDL2/SDL_log.h>
#include <SDL2/SDL_rect.h>
#include <SDL2/SDL_mouse.h>
#include <glm/gtc/matrix_transform.hpp>
#include <sstream>
#include <fstream>
#include <cmath>
#include <cstdlib>
#include <ctime>
#include <iostream>
#include <string>
#include <vector>
#include <cfloat>

#if __APPLE__
    #include <OpenGLES/ES2/gl.h>
    #include <OpenGLES/ES2/glext.h>
#elif defined(__ANDROID__)
    #include <GLES2/gl2.h> 
    #include <GLES2/gl2ext.h>
#else
    #include <GL/glew.h>
#endif

namespace gu
{

class Engine;

#define PI 3.14159265f

enum Axis
{
    AXIS_X,
    AXIS_Y,
    AXIS_Z,
    AXIS_XY,
    AXIS_XZ,
    AXIS_YZ,
    AXIS_XYZ
};

//! Axis-Aligned Bounding Box 

//!----------------------
//! Overview
//!----------------------
//!
//! An Axis-Aligned Bounding Box (AABB) is a fast but restrictive 3D rectangle used for intersections.
//!
//! The <b>centre<\b> member is the centre of the AABB, while the <b>radius</b> is the magnitude (width/height) of the box from the centre point in a single direction.
//!
//! For example, a box with radius.x = 2 units will be 4 units wide.
//!
//! Because AABBs are axis-aligned, they cannot be rotated.
struct AABB
{
    glm::vec3 centre;
    glm::vec3 radius;

    AABB()
    {

    }

    AABB(const glm::vec3 &c, const glm::vec3 &r)
    {
        centre = c;
        radius = r;
    }

    //!
    //! Used by the SAT algorithm.
    glm::vec3 getPositiveVertex(const glm::vec3 &normal) const
    {
        glm::vec3 temp(centre.x - radius.x, centre.y - radius.y, centre.z - radius.z);

        if (normal.x >= 0.0f)
            temp.x = centre.x + radius.x;
        if (normal.y >= 0.0f)
            temp.y = centre.y + radius.y;
        if (normal.z >= 0.0f)
            temp.z = centre.z + radius.z;

        return temp;
    }
};

//! Y-Oriented Bounding Box

//!----------------------
//! Overview
//!----------------------
//!
//! A Y-Oriented Bounding Box (YOBB) is a AABB that can be rotated along the Y-axis. Because only one axis is rotated, performing a SAT collision on
//! the structure is reasonably fast.
//!
//! Aside from the <b>rotation</b> member, its structure is identical to gu::AABB.
struct YOBB
{
    glm::vec3 centre;
    glm::vec3 radius;
    float rotation;
    
    YOBB()
    {
        rotation = 0.0f;
    }
    
    YOBB(const glm::vec3 &c, const glm::vec3 &r, float rot)
    {
        centre = c;
        radius = r;
        rotation = rot;
    }
};

// Functions

//!
//! A newly created gu::Engine will attach itself to the toolset via this function. Do not call unless you really know what you're doing.
void attachEngine(gu::Engine *engine);

//!
//! Converts a boolean into a string.
std::string toString(bool boolean);
//!
//! Converts a character into a string.
std::string toString(char character);
//!
//! Converts a float into a string.
std::string toString(float number);
//!
//! Converts an int into a string.
std::string toString(int number);
//!
//! Converts a double into a string.
std::string toString(double number);
//!
//! Converts a long into a string.
std::string toString(long number);
//!
//! Converts a glm::vec2 to a string.
std::string toString(const glm::vec2 &v);
//!
//! Converts a glm::vec3 to a string.
std::string toString(const glm::vec3 &v);
//!
//! Converts a glm::vec4 to a string.
std::string toString(const glm::vec4 &v);
//!
//! Checks for the existance of a file on storage
bool fileExists(const std::string &path);
//!
//! Float absolute value function
float absval(float number);
//!
//! Converts a whole-number from a decimal based on rounding.
int round(float num);
//!
//! Return a random number between and including min to max
int random(int min, int max);
//!
//! Returns a random 3 decimal number between and including min to max
float random(float min, float max, int precision=1000);
//!
//! Returns the set of corners from the AABB object.
//! <b>output</b> should be filled with 8 vertices.
void corners(const gu::AABB &aabb, std::vector<glm::vec3> *output);
//!
//! Returns the set of corners from the YOBB object.
//! <b>output</b> should be filled with 8 vertices.
void corners(const gu::YOBB &yobb, std::vector<glm::vec3> *output);
//!
//! Returns the 2D set of corners to compute the normal edges from a YOBB object.
//! <b>output</b> should be an array size 4.
void corners2D(const gu::YOBB &yobb, glm::vec3 output[]);
//!
//! Returns 3 normalized XZ normals from a series of corners.
//! (top-left), (bottom-left), (bottom-right)
//!
//! Note: is meant to be used with the output of gu::corners2D on a YOBB.
//! <b>output</b> should be an array size 2.
void normalsXZ(glm::vec3 corners[], glm::vec2 output[]);
//!
//! Returns the min and max points of a projected shape onto an axis.
//! <b>corners</b> should be an array size 4.
void projectXZ(glm::vec3 corners[], const glm::vec2 &axis, float *min, float *max);
//!
//! Returns whether two sets of values are overlapping
bool overlaps(float min_A, float max_A, float min_B, float max_B, float *overlap);
//!
//! Returns whether A is completely contained in B.
bool contains(const gu::AABB &A, const gu::AABB &B);
//!
//! Returns whether A is completely contained in B using only the X and Z axis.
bool containsXZ(const gu::AABB &A, const gu::AABB &B);
//!
//! Intersection between 2 AABB.
bool intersects(const gu::AABB &A, const gu::AABB &B);
//!
//! Intersection between 2 YOBB.
bool intersects(const gu::YOBB &A, const gu::YOBB &B, glm::vec3 *mtv=NULL);
//!
//! Intersection between ray and AABB
//!
//! Please normalize the ray direction.
bool intersects(const glm::vec3 &ray_position, glm::vec3 ray_direction, const gu::AABB &aabb, float *distance);
//!
//! Intersection between 2 lines.  The last argument is the output intersection point.
bool intersects(const glm::vec2 &A1, const glm::vec2 &A2, const glm::vec2 &B1, const glm::vec2 &B2, glm::vec2 *intersection);
//!
//! Prints OpenGL errors.
bool printOpenGLErrors(std::string output_prefix="", bool output_on_no_error=false);
//!
//! Gets OpenGL errors.
std::string getOpenGLErrors(const std::string &output_prefix="", bool output_on_no_error=false);
//!
//! Conversion to AABB.
//!
//! This operation is expensive, as every corner of the gu::YOBB needs to be computed using trigonometric functions before the gu::AABB can be made.
gu::AABB toAABB(const gu::YOBB &yobb);
//!
//! Conversion to YOBB.
//!
//! This operation is incredibly cheap, as it it involes just a per-member copy.
gu::YOBB toYOBB(const gu::AABB &aabb);
//!
//! Returns the render size of the currently-running gu::Engine. This is simply a shortcut and calls the same method as gu::Engine::getRenderSize().
const glm::ivec2& getRenderSize();

}
