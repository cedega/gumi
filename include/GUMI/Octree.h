#pragma once
#include "Copy.h"
#include "DrawRect.h"

namespace gu
{

enum QUADTREE_TYPE
{
    QUADTREE_NONE,
    QUADTREE_XY,
    QUADTREE_XZ,
    QUADTREE_YZ
};

class Octree
{
 public:
    Octree();
    ~Octree();
    void draw();
    void init(const gu::AABB &size, const std::vector<gu::YOBB> &objects, int depth=0, gu::Octree *parent=NULL);
    void update(const std::vector<gu::YOBB> &objects);

    void convertToQuadTree(QUADTREE_TYPE);

    static int comparisons;

 private:
    DISALLOW_COPY_AND_ASSIGN(Octree);
    gu::AABB size_;
    std::vector<gu::AABB> objects_;
    Octree *parent_;
    Octree *children_[8];
    gu::QUADTREE_TYPE type_;

    void deconstruct();
    void make4(const gu::AABB &size, const std::vector<gu::AABB> &objects, int depth, gu::Octree *parent);
    void make8(const gu::AABB &size, const std::vector<gu::AABB> &objects, int depth, gu::Octree *parent);
};

}
