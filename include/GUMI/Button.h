#pragma once
#include "Image.h"
#include "Input.h"

namespace gu
{

class Button : public gu::Image
{
 public:
    Button();
    //!
    //! Copy constructor.
    //!
    //! Will copy all data.
    Button(const Button &obj);
    ~Button();
    //!
    //! Assignment operator.
    //!
    //! Will copy all data.
    Button& operator=(const Button &obj);

    void loadPressedTexture(GLuint texture_id);
    bool poll();

    // Setters
    void setActiveRadius(float);
    void setActiveArea(const glm::vec4 &rect);
    void setActivateOnRelease(bool);
    void setForcePressedTexture(int);
    void setIgnoreDistance(float);
    void setActivateOnContact(bool);

    // Getters
    float getActiveRadius() const;
    const glm::vec4& getActiveArea() const;
    bool getActivateOnRelease() const;
    int getForcePressedTexture() const;
    float getIgnoreDistance() const;
    bool getActivateOnContact() const;

 private:
    int held_;
    glm::vec2 start_held_;

    bool use_circle_;
    float active_radius_;
    glm::vec4 active_area_;

    bool activate_on_release_;
    bool activate_on_contact_;
    bool has_pressed_texture_;
    int force_pressed_texture_;
    GLuint texture_;
    GLuint pressed_texture_;
    float ignore_distance_;

#if !defined(TARGET_OS_IPHONE) && !defined(__ANDROID__)

    bool last_mouse_down_;
    glm::vec2 start_press_;

#endif

    bool processPress(const glm::vec2 &press_position);
    bool processRelease(const glm::vec2 &release_position);

};

}
