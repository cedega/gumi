#pragma once
#include "Image.h"

namespace gu
{

class PostProcess
{
 public:
    PostProcess();
    ~PostProcess();

    void draw();
    void drawDepth();

    void init(const glm::vec2 &resolution, gu::Shader *shader, gu::Shader *depth_shader, const glm::vec3 &blank_colour);
    void begin();
    void end();

    // Getters
    GLuint getTexture() const;
    GLuint getDepthTexture() const;

 private:
    DISALLOW_COPY_AND_ASSIGN(PostProcess);

    gu::Image blank_;
    gu::Shader blank_shader_;

    gu::Image image_;
    gu::Image depth_image_;

    bool initialized_;
    GLuint frame_buffer_;
    GLuint depth_buffer_;
    GLuint texture_;
    GLuint depth_texture_;
};

}
