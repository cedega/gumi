#pragma once
#include "Model.h"
#include "EntityLogic.h"

namespace gu
{

struct EntityInput
{
    glm::vec2 move;
    bool jump;

    EntityInput()
    {
        jump = false;
    }
};

//! Core Entity class

//!----------------------
//! Overview
//!----------------------
//!
//! gu::Entity is a combination of a gu::Model and gu::EntityLogic. Note that only gu::EntityLogic is inherited,
//! and the gu::Model is a component. This is to easily allow the reuse of a gu::Model among many gu::Entity.
//!
//! This class is the base for any interactable object in a GUMI project. Roughly, the call procedure should work like:
//!
//! In gu::Engine::logic():
//!     - input()
//!     - move()
//!     - collide() against all objects
//!     - update()
//!
//! In gu::Engine::render()
//!     - updateAnimations(frame_time);
//!     - if (camera.visible(getCullbox())) draw(interpolation)
class Entity : public gu::EntityLogic, public gu::Drawable
{
 public:
    friend class Collisions;
    Entity();
    //!
    //! Copy constructor.
    //!
    //! It will copy all internal data and set a reference to the gu::Model of <b>obj</b>.
    Entity(const Entity &obj);
    ~Entity();
    //!
    //! Assignment operator.
    //!
    //! It will copy all internal data and set a reference to the gu::Model of <b>obj</b>.
    Entity& operator=(const Entity &obj);

    //!
    //! Updates the animations inside the gu::Entity's armature.
    //!
    //! Therefore for all gu::Entity which share a gu::Model, this is a required call even if no armature is present.
    void updateAnimations(const float frame_time);
    //!
    //! Draws the gu::Model assigned.
    void draw(const float interpolation);
    //!
    //! Binds the shader to the internal gu::Model.
    void bindShader(gu::Shader *shader);
    //!
    //! Binds the gu::Model to the gu::Entity. This is a mandatory call.
    //!
    //! The <b>shader</b> argument exists as a shader override, but the default NULL will simply use whichever shader was bound to the gu::Model previously.
    //!
    //! In almost all cases, leave <b>shader</b> as NULL.
    void bindModel(gu::Model *model, gu::Shader *shader=NULL);
    //!
    //! Sets the input direction for the gu::Entity. Think of this as the direction of an analog stick.
    //! Using this direction supplied by <b>move</b> and the speed / jump of setSpeed() and setJump(), the gu::Entity will
    //! move appropriately at the move() step.
    void input(const glm::vec2 &move, bool jump);
    //!
    //! Moves the gu::Entity based on the previous input(). Note that no collision detection is performed at this stage.
    //! Therefore, after calling this function, collide the gu::Entity against world objects using collide().
    //!
    //! In addition to moving, the gu::Entity will also rotate along the Y-axis to face the input direction. This is done based on the value of getTurnSpeed().
    bool move(const float dt);
    //!
    //! Collides the gu::Entity's hitbox against a gu::AABB object. If a collision is made, the gu::Entity's hitbox will be updated to a non-colliding position.
    bool collide(const gu::AABB &object);
    //!
    //! Collides the gu::Entity's hitbox against a gu::YOBB object. If a collision is made, the gu::Entity's hitbox will be updated to a non-colliding position.
    bool collide(const gu::YOBB &object);
    //!
    //! Updates the attached gu::Model's position to that of gu::EntityLogic::getPosition().
    //!
    //! The cullbox is also updated at this call to the same position.
    void update(const float dt);

    //!
    //! Loads the given animation into the armature of the bound gu::Model.
    //! 
    //! <b>blend_time</b> is the amount of time to interpolate between the gu::Model's current armature and the
    //! armature of the first frame of <b>animation</b>. If <b>blend_time</b> is 0, then no blending will occur.
    void loadAnimation(gu::Animation *animation, float blend_time=0.0f, gu::AnimationLayer index=ANIM_BASE);
    //!
    //! Removes the overloaded animation that's applied to the gu::Entity's gu::Model.
    void removeOverloadedAnimation(float blend_time=0.0f);

    // Virtual

    //!
    //! A virtual callback for when a gu::Entity hits the ground.
    virtual void onLand();

    // Setters
    
    //!
    //! Sets the internal gu::Armature. This is a mandatory call if skeletal animation will be used.
    //!
    //! This is an expensive operation, because the entire <b>armature</b> is deep-copied into the gu::Entity.
    //!
    //! After this call, loadAnimation() must be called.
    void setArmature(const gu::Armature &armature);
    //!
    //! Sets the position of the gu::Entity.
    //!
    //! By asserting <b>snap</b>, the gu::Entity will teleport to the given position and skip interpolation.
    //!
    //! <b>snap</b> should be asserted in situations like spawning a gu::Entity or teleporting.
    void setPosition(const glm::vec3 &position, bool snap=false);
    //!
    //! Sets the rotation of the gu::Entity. This should be used over gu::Model::setRotation().
    void setRotation(const glm::vec3 &rotation);
    //!
    //! Sets the scaling of the gu::Entity. This should be used over gu::Model::setScaling().
    void setScaling(const glm::vec3 &scaling);
    //!
    //! Sets the colour mod of the gu::Entity. This should be used over gu::Model::setColour().
    void setColour(const glm::vec4 &colour);
    //!
    //! Sets the radius of the gu::Entity's hitbox. The hitbox is used for all environment collisions and hit detection.
    void setHitboxRadius(const glm::vec3 &radius);
    //!
    //! Sets the offset of the gu::Entity's hitbox. Because the hitbox is always centered around gu::EntityLogic::getPosition(), this function
    //! offsets that value to position the hitbox more accurately around the gu::Model.
    void setHitboxOffset(const glm::vec3 &offset);
    //!
    //! Sets the rotation of the gu::Entity's hitbox. The argument is in degrees.
    void setHitboxRotation(float);
    //!
    //! Sets the radius of the gu::Entity's cullbox. The cullbox is used for frustrum culling, and should be querried via gu::Camera::visible().
    //! If gu::Camera::visible() returns false on the cullbox, then it's safe to skip gu::Entity::draw() to save on OpenGL performance.
    //!
    //! Therefore, it is necessary to make sure that the cullbox's radius is set large enough to fully encompass the entire model and all animations it may perform.
    void setCullboxRadius(const glm::vec3 &radius);
    //!
    //! Like setHitboxOffset(), the cullbox offset will offset the box from gu::EntityLogic::getPosition().
    void setCullboxOffset(const glm::vec3 &offset);
    //!
    //! If this setting is asserted, then the hitbox will rotate in the direction of the gu::Entity. For most squareish objects, this should be
    //! disabled because it doesn't look as nice as a static gu::YOBB.
    void setRotateHitbox(bool);
    //!
    //! Sets the movement speed of the gu::Entity.
    void setSpeed(float);
    //!
    //! Sets the jump height of the gu::Entity.
    void setJump(float);
    //!
    //! Sets the turn speed of the gu::Entity. The lower the value, the faster the turn speed. Do not set below 1.0f (1.0f = instant).
    void setTurnSpeed(float);
    //!
    //! Determines whether turn rotation will be applied at the move() function.
    void applyTurnSpeed(bool);

    // Getters
   
    //!
    //! Returns the currently bound gu::Shader.
    gu::Shader* getShader();
    //!
    //! Returns the internal gu::Armature.
    gu::Armature* getArmature();
    //!
    //! Returns the position of the gu::Entity.
    const glm::vec3& getPosition() const;
    //!
    //! Returns the rotation of the gu::Entity. This should be used over gu::Model::getRotation().
    const glm::vec3& getRotation() const;
    //!
    //! Returns the scaling of the gu::Entity. This should be used over gu::Model::getScaling().
    const glm::vec3& getScaling() const;
    //!
    //! Returns the colour mod of the gu::Entity. This should be used over gu::Model::getColour().
    const glm::vec4& getColour() const;
    const EntityInput& getEntityInput() const;
    //!
    //! Returns the hitbox as a whole which can be tested using gu::intersects().
    const gu::YOBB& getHitbox() const;
    const glm::vec3& getHitboxOffset() const;
    float getHitboxRotation() const;
    //!
    //! Returns the cullbox as a whole which can be texted using gu::Camera::visible().
    const gu::AABB& getCullbox() const;
    const glm::vec3& getCullboxOffset() const;
    bool getRotateHitbox() const;
    float getSpeed() const;
    float getJump() const;
    //!
    //! Returns the gu::Model() which is bound to the gu::Entity. This function can be used to call methods from gu::Model.
    gu::Model* getModel();
    float getTurnSpeed() const;
    //!
    //! Returns the animation loaded from the internal model
    gu::Animation* getLoadedAnimation(gu::AnimationLayer index=ANIM_BASE);
    //!
    //! Returns whether the model is animating the passed animation.
    bool isAnimating(const gu::Animation *animation, gu::AnimationLayer index=ANIM_BASE) const;

 protected:
    //!
    //! This function will set the entity's hitbox to the location of gu::EntityLogic::getPosition().
    //!
    //! This is especially useful for gu::Entity which don't wish to use the traditional move() method, but wish
    //! to update the hitbox.
    void updateHitbox();

 private:
    gu::Model *model_;
    gu::Armature armature_;

    glm::vec3 position_;
    glm::vec3 last_position_;
    glm::vec3 rotation_;
    glm::vec3 scaling_;
    glm::vec4 colour_;

    EntityInput input_;
    float turn_speed_;
    float speed_;
    float jump_;
    bool jumping_;
    bool apply_turn_speed_;
    bool loaded_armature_;

    gu::YOBB hitbox_;
    glm::vec3 hitbox_offset_;
    gu::AABB cullbox_;
    glm::vec3 cullbox_offset_;
    bool rotate_hitbox_;
};

}

