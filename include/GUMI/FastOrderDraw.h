#pragma once
#include "Drawable.h"
#include "Camera.h"
#include <algorithm>

namespace gu
{

struct FastDrawElement
{
    gu::Drawable *drawable;
    float distance;
};

//! Ordered drawing based on centre-point distances

//!----------------------
//! Overview
//!----------------------
//!
//! Due to the problems with transparent objects and the z-buffer, gu::FastDrawElement was created to handle the sorting of transparent objects in a scene
//! to draw them correctly.
//!
//! In detail, because transparent objects are see-through, geometry must be visible behind it. However, by default, the z-buffer makes sure that all
//! geometry behind an object is hidden, or not drawn. Therefore, if we were to create a giant invisible texture covering the front of the camera,
//! no geometry behind it will be visible because the z-buffer doesn't care about opacity -- it only thinks that a giant quad is covering the camera.
//!
//! To get around this, the common procedure is to:
//!     - draw all opaque geometry
//!     - sort all transparent gemoetry
//!     - disable the Z-buffer
//!     - draw all transparent geometry from farthest to closest.
//!
//! The gu::FastOrderDraw object streamlines the last 3 steps. However, the sort() function will only sort based on centre points, which may have visual problems.
//! However, using this with 2D billboards will be accurate, and it will be incredibly fast compared to sorting individual vertices.
class FastOrderDraw
{
 public:
    FastOrderDraw();

    //!
    //! Draws all added objects. Must call sort() before to have the intended effect.
    //!
    //! When drawing gu::Model or gu::Entity, updateAnimations() will be called automatically.
    void draw(const float interpolation=1.0f, const float frame_time=0.0f);
    //!
    //! Sorts all added objects from farthest to closest, based on the supplied gu::Camera's position.
    void sort(const gu::Camera &camera);
    //!
    //! Add an transparent drawable object to the list of objects to draw.
    void add(gu::Drawable *drawable);
    //!
    //! Clears all added drawables from the internal list.
    void clear();

 private:
    DISALLOW_COPY_AND_ASSIGN(FastOrderDraw);

    std::vector<gu::FastDrawElement> list_;
};

}
