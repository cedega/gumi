#pragma once
#include "Image.h"

namespace gu
{

//! Extends gu::Image to add spritesheet support.

//!----------------------
//! Overview
//!----------------------
//!
//! gu::SpriteSheet supersedes gu::Image and adds spritesheeting functionality.
//!
//! Spritesheets are divided up into "frames" or "sprite boxes".
//!
//! Every sprite in the spritesheet is suppose to fit in the specified frame.
//! This means that, if the biggest sprite is 100x200, your frame will have to be at least that big for ALL sprites.
//! 
//! By making this decision, the need for per-frame displacements is gone, artists will be in charge of assembling their frames correctly.
//!
//! Frames have 3 main pieces of data:
//!   1) width
//!   2) height
//!   3) spacing
//!
//! width and height are the dimensions of a frame (ex: width = 100, height = 200), and spacing is the value between frames.
//! This value exists so any blending that occurs will not spill into neighbouring frames.
class SpriteSheet : public gu::Image
{
 public:
    SpriteSheet();
    //!
    //! Creates a spritesheet with values for the width, height, and spacing.
    SpriteSheet(int sprite_box_width, int sprite_box_height, int sprite_spacing);
    SpriteSheet(const SpriteSheet &obj);
    SpriteSheet& operator=(const SpriteSheet &obj);
    
    //!
    //! Selects a sprite on the specified row & column to display.
    //! Note that rows and columns start at 1, not 0.
    void setSprite(int row, int column);
    
    // Setters

    //!
    //! Sets 3 main pieces of data of a spritesheet. Refer to the Overview for an explanation.
    void setSpriteSheet(int sprite_box_width, int sprite_box_height, int sprite_spacing);

    // Getters
    
    //!
    //! Returns the row of the currently selected sprite.
    int getRow() const;
    //!
    //! Returns the column of the currently selected sprite. 
    int getColumn() const;
    //!
    //! Returns the width of each "frame" or "sprite box."
    int getSpriteBoxWidth() const;
    //!
    //! Returns the height of each "frame" or "sprite box."
    int getSpriteBoxHeight() const;
    //!
    //! Returns the spacing between each "frame" or "sprite box."
    int getSpriteSpacing() const;
    //!
    //! Returns how many sprites can fit on a single row, based on the texture size and "sprite box" width.
    int getSpritesPerRow() const;
    //!
    //! Returns a rectangle containing the X,Y coordinate of the sprite within the spritesheet, as well as the width and height of the "sprite box."
    glm::vec4 getBounds(int row, int column) const;
    
 private:
    int sprite_spacing_;
    int sprite_box_width_;
    int sprite_box_height_;
    int row_;
    int column_;

    inline int addColumnSpacing(int c) const;
    inline int addRowSpacing(int r) const;
};

}
