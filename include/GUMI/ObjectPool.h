#pragma once
#include <cstddef>
#include <iostream>

namespace gu
{

template<typename T> 
struct PoolObject
{
    bool active;
    T object;
    PoolObject *next_free_object;
};

template<typename T, unsigned int N> 
class ObjectPool
{
 public:
    ObjectPool()
    {
        size_ = N; 

        for (unsigned int i = 0; i < N - 1; i++)
        {
            array_[i].active = false;
            array_[i].next_free_object = &array_[i + 1];
        }

        array_[N - 1].active = false;
        array_[N - 1].next_free_object = NULL;

        free_object_ = &array_[0];
        last_created_object_ = NULL;
    }

    ObjectPool(const ObjectPool &obj)
    {
        size_ = N;

        for (unsigned int i = 0; i < size_; i++)
        {
            array_[i].active = obj.array_[i].active;
            array_[i].object = obj.array_[i].object;

            if (obj.array_[i].next_free_object != NULL)
            {
                int index = obj.array_[i].next_free_object - &obj.array_[0];
                array_[i].next_free_object = &array_[index];
            }
            else
            {
                array_[i].next_free_object = NULL;
            }
        }

        if (obj.free_object_ != NULL)
        {
            int index = obj.free_object_ - &obj.array_[0];
            free_object_ = &array_[index];
        }
        else
            free_object_ = NULL;

        if (obj.last_created_object_ != NULL)
        {
            int index = obj.last_created_object_ - &obj.array_[0];
            last_created_object_ = &array_[index];
        }
        else
            last_created_object_ = NULL;
    }

    ObjectPool& operator=(const ObjectPool &obj)
    {
        size_ = N;

        for (unsigned int i = 0; i < size_; i++)
        {
            array_[i].active = obj.array_[i].active;
            array_[i].object = obj.array_[i].object;

            if (obj.array_[i].next_free_object != NULL)
            {
                int index = obj.array_[i].next_free_object - &obj.array_[0];
                array_[i].next_free_object = &array_[index];
            }
            else
            {
                array_[i].next_free_object = NULL;
            }
        }

        if (obj.free_object_ != NULL)
        {
            int index = obj.free_object_ - &obj.array_[0];
            free_object_ = &array_[index];
        }
        else
            free_object_ = NULL;

        if (obj.last_created_object_ != NULL)
        {
            int index = obj.last_created_object_ - &obj.array_[0];
            last_created_object_ = &array_[index];
        }
        else
            last_created_object_ = NULL;

        return *this;
    }

    bool create(const T &object)
    {
        if (free_object_ == NULL)
            return false;

        free_object_->object = object;
        free_object_->active = true;

        last_created_object_ = free_object_;
        free_object_ = free_object_->next_free_object;

        return true;
    }

    bool erase(unsigned int index)
    {
        if (!array_[index].active)
            return false;

        array_[index].active = false;
        array_[index].next_free_object = free_object_;
        free_object_ = &array_[index];

        return true;
    }

    void clear()
    {
        for (unsigned int i = 0; i < size_ - 1; i++)
        {
            array_[i].active = false;
            array_[i].next_free_object = &array_[i + 1];
        }

        array_[size_ - 1].active = false;
        array_[size_ - 1].next_free_object = NULL;

        free_object_ = &array_[0];
        last_created_object_ = NULL;
    }

    gu::PoolObject<T>* getObjects()
    {
        return array_;
    }

    T& at(unsigned int index)
    {
        return array_[index].object;
    }

    T& back()
    {
        return last_created_object_->object;
    }

    T& operator[](unsigned int index)
    {
        return array_[index].object;
    }

    unsigned int size() const
    {
        return size_;
    }

    bool active(unsigned int index) const
    {
        return array_[index].active;
    }

 private:
    PoolObject<T> array_[N];
    PoolObject<T> *free_object_;
    unsigned int size_;
    PoolObject<T> *last_created_object_;
};

}
