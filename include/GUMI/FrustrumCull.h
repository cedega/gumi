#pragma once
#include <GUMI/Copy.h>
#include <GUMI/Tools.h>

namespace gu
{

class Plane
{
 public:
    void set(const glm::vec3 &n, const glm::vec3 &point)
    {
        normal_ = glm::normalize(n);
        d_ = -glm::dot(point, normal_);
    }

    float distance(const glm::vec3 &point) const
    {
        return glm::dot(normal_, point) + d_;
    }

    const glm::vec3& getNormal() const
    {
        return normal_;
    }

 private:
    glm::vec3 normal_;
    float d_;
};

class FrustrumCull
{
 public:
    FrustrumCull();

    void init(float angle, float ratio, float near, float far);
    void set(const glm::vec3 &p, const glm::vec3 &l, const glm::vec3 &u);
    bool visible(const gu::AABB &aabb) const;

 private:
    DISALLOW_COPY_AND_ASSIGN(FrustrumCull);
    gu::Plane planes_[6];
    bool initialized_;
    float near_;
    float far_;
    float near_width_;
    float near_height_;
};

}
