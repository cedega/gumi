#pragma once
#include "TextureLoader.h"

namespace gu
{

//! Animated textures for gu::Model

//!----------------------
//! Overview
//!----------------------
//!
//! Animates the texture of a gu::Model.
//! Only governs a single set of animations. Typically with a 3D Model, many animated textures may exist,
//! but this is instead handled by gu::AnimatedTextureList.
class AnimatedTexture
{
 public:
    AnimatedTexture();
    AnimatedTexture(GLuint texture, float time);
    AnimatedTexture(const gu::AnimatedTexture &obj);
    ~AnimatedTexture();
    AnimatedTexture& operator=(const gu::AnimatedTexture &obj);

    //!
    //! Adds a texture to the list of textures to be animated.
    void addTexture(GLuint texture, float time);
    void update(const float dt);
    void reset();

    // Setters
    void setLooping(bool);
    void setFrame(unsigned int);

    // Getters
    bool getLooping() const;
    bool getStopped() const;
    unsigned int getFrame() const;
    GLuint getCurrentTexture() const;
    const std::vector<GLuint>* getTextures() const;
    const std::vector<float>* getTimings() const;

 private:
    std::vector<GLuint> textures_;
    std::vector<float> timings_;
    unsigned int frame_;
    float tick_;
    bool looping_;
    bool stopped_;
};

}
