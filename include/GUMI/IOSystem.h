#pragma once
#include <assimp/IOSystem.hpp>
#include <assimp/IOStream.hpp>
#include <SDL2/SDL_rwops.h>

namespace gu
{

class IOStream : public Assimp::IOStream
{
 public:
    IOStream(SDL_RWops *rw);
    ~IOStream();

    size_t FileSize() const;
    void Flush();
    size_t Read(void *pvBuffer, size_t pSize, size_t pCount);
    aiReturn Seek(size_t pOffset, aiOrigin pOrigin);
    size_t Tell() const;
    size_t Write(const void *pvBuffer, size_t pSize, size_t pCount);

 private:
    SDL_RWops *rw_;
};

class IOSystem : public Assimp::IOSystem
{
 public:
    IOSystem();

    void Close(Assimp::IOStream *pFile);
    bool Exists(const char *pFile) const;
    char getOsSeparator() const;
    Assimp::IOStream* Open(const char *pFile, const char *pMode="rb");
};

}
