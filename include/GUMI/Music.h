#pragma once
#include <SDL2/SDL_mixer.h>
#include "Zip.h"

namespace gu
{

//! Music Playing Class

//!----------------------
//! Overview
//!----------------------
//!
//! Loads music from a zip file or an on-disk file.
class Music
{
 public:
    Music();
    ~Music();

    //!
    //! Loads a music file from within the zip file specified in gu::Engine.
    void loadZipMusic(const std::string &file_path);
    //!
    //! Loads a regular music file from the disk.
    void loadFileMusic(const std::string &file_path);
    
    //!
    //! Plays the music, which will loop automatically.
    void play();
    //!
    //! Stops all music, from all instances of gu::Music.
    static void stop();
    //!
    //! Stops all music after fading it out for the passed milliseconds.
    static void fadeOut(int ms);
    //!
    //! Plays the music, but fades in the volume for the passed milliseconds.
    void fadeIn(int ms);
    
    // Setters
    void setVolume(int volume);
    
    // Getters
    int getVolume() const;
    
 private:
    DISALLOW_COPY_AND_ASSIGN(Music);

    Mix_Music *music_;
    SDL_RWops *zip_rw_;
    char *zip_music_buffer_;
    bool zip_used_;
    
    int volume_;

    void deconstruct();
};

}
