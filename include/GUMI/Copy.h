#pragma once
#define DISALLOW_COPY_AND_ASSIGN(TypeName) \
    TypeName(const TypeName&);             \
    void operator=(const TypeName&)
#define DISALLOW_COPY(TypeName) \
    void operator=(const TypeName&)
