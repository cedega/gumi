#pragma once
#if defined(__ANDROID__)
#include <EGL/egl.h>
#endif
#include "TextureLoader.h"
#include "Shader.h"

namespace gu
{

//! Abstract class for drawable objects

//!----------------------
//! Overview
//!----------------------
//!
//! All of gu::Model, gu::Image, gu::Entity, and gu::ParticleSystem inherit gu::Drawable.
//!
//! This allows for all 3 to be grouped and to exist in the same container / vector.
//!
//! As well, this class initializes Android VAO functionality.
class Drawable
{
 public:
    //!
    //! Optional overload used by gu::Model and gu::Entity.
    virtual void updateAnimations(const float frame_time) {}
    virtual void draw(const float interpolation=1.0f) = 0;
    virtual void bindShader(gu::Shader* shader) = 0;

    virtual const glm::vec3& getPosition() const = 0;

    virtual gu::Shader* getShader() = 0;

    #if defined(__ANDROID__)
    static void initAndroid();
    #endif

    #if defined(TARGET_OS_IPHONE) || defined(__ANDROID__)
    static bool hasOES();
    #endif

 protected:
    #if defined(__ANDROID__)
    static PFNGLGENVERTEXARRAYSOESPROC glGenVertexArraysOES;
    static PFNGLBINDVERTEXARRAYOESPROC glBindVertexArrayOES;
    static PFNGLDELETEVERTEXARRAYSOESPROC glDeleteVertexArraysOES;
    static PFNGLISVERTEXARRAYOESPROC glIsVertexArrayOES;
    #endif
 private:
    #if defined(TARGET_OS_IPHONE) || defined(__ANDROID__)
    static bool has_oes_support_;
    #endif
};

}
