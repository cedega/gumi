#pragma once
#include "Drawable.h"

namespace gu
{

struct Particle
{
    float life_tick;
    float life_time;
    glm::vec3 position;
    glm::vec3 velocity;
    glm::vec3 acceleration;
    glm::vec3 colour;
    float rotation;
    float rvelocity;
    
    Particle()
    {
        life_tick = 0.0f;
        life_time = 0.0f;
        rotation = 0.0f;
        rvelocity = 0.0f;
    }
};

//! Particle System class

//!----------------------
//! Overview
//!----------------------
//!
//! gu::ParticleSystem handles the simulating of particle effects. By using a programmable function pointer, users can specify and create
//! a wide vareity of effects.
//!
//! To properly program effects, one must be very familiar with the gu::Particle struct. Basically:
//!
//!     - life_time = the time a particle is alive for (in seconds)
//!     - position = the starting position of the particle
//!     - velocity = the velocity of the particle
//!     - acceleration = the acceleration of the particle
//!     - colour = the colour of the particle
//!
//! When creating particle effects, all 5 of these attributes must be set to some value.
//! This will guarentee that particle effects will properly reset.
//!
//! Users may use the two example functions: gu::particleSquare() and gu::particleSphere as 
//! a reference for customizing particle effects.
class ParticleSystem : public gu::Drawable
{
 public:
    ParticleSystem();
    //!
    //! Copy constructor.
    //!
    //! It will perform a deep-copy of all particles and call create() if <b>obj</b> was initialized.
    ParticleSystem(const ParticleSystem &obj);
    ~ParticleSystem();
    //!
    //! Assignment operator.
    //!
    //! It will perform a deep-copy of all particles and call create() if <b>obj</b> was initialized.
    ParticleSystem& operator=(const ParticleSystem &obj);
    
    //!
    //! Draw the particle system, using an interpolation factor of <b>interpolation</b>
    //!
    //! Note that only the emitter's location is interpolated. Invididual particles are <b>NOT</b> interpolated.
    //!
    //! To reduce visual artifacts on some drivers, it's <b>heavily</b> recommended to draw <b>after</b> all depth-tested objects, especially if drawing with blending.
    void draw(const float interpolation=1.0f);
    //!
    //! Updates all the positions and states of the particles in the gu::ParticleSystem.
    void update(const float dt);
 
    //!
    //! Binds a gu::Shader to the gu::ParticleSystem. The gu::ParticleSystem will store <b>shader</b> as a reference, and use it every draw() call.
    //!
    //! This is a mandatory call unless a gu::Shader is passed as an argument to the create() function.
    //!
    //! <b>use_texture</b> is a boolean which determines whether textures will be used for the particles rather than flat colours.
    void bindShader(gu::Shader *shader);
    //!
    //! Loads a texture into the Particle System. This call must be done at least one <b>before</b> calling create().
    void loadTexture(GLuint texture);
    //!
    //! Initializes a particle system with <b>particles</b> amount of particles, and set them using the function <b>particleSet</b>.
    //!
    //! The <b>data</b> argument will be passed to whichever function <b>particleSet</b> corresponds to.
    //!
    //! <b>particleSet</b> must be a supplied function that initializes the particle system. <b>particleSet</b> will then be saved and run
    //! every time reset() is called.
    //!
    //! If <b>shader</b> is not NULL, then bindShader() will automatically be called.
    void create(unsigned int particles, void (*particleSet)(gu::Particle *particle, int index, void *data), void *data, gu::Shader *shader=NULL);
    //!
    //! Plays the particle system from the beginning of the <b>particleSet</b> function.
    void play();
    //!
    //! Updates the last position to be used in interpolation. This must be called before setPosition() for interpolation to work correctly.
    void updateLastPosition();
    //!
    //! Unload the particle system.
    void unload();
    
    // Setters

    //!
    //! Sets the position of the particle emitter.
    //!
    //! By asserting <b>snap</b>, the gu::ParticleSystem will teleport to the given position and skip interpolation.
    //!
    //! <b>snap</b> should be asserted in situations like spawning a gu::ParticleSystem or teleporting.
    void setPosition(const glm::vec3 &position, bool snap=false);
    //!
    //! Sets the size of every particle. For example, a value of 2.0 will make particles twice the size of a pixel.
    void setPointSize(float);
    //!
    //! Sets whether additive blending will be used.
    void setAdditiveBlending(bool);
    //!
    //! Sets whether the system will loop continually.
    void setContinuous(bool);
    //!
    //! Sets the data pointer initialized by create().
    void setDataPointer(void *data);
    
    // Getters

    //!
    //! Returns the currently bound gu::Shader.
    gu::Shader* getShader();
    //!
    //! Returns the location of the particle emitter.
    const glm::vec3& getPosition() const;
    //!
    //! Returns the size of every particle.
    float getPointSize() const;
    //!
    //! Returns whether additive blending will be used.
    bool getAdditiveBlending() const;
    //!
    //! Returns whether the system will loop continually.
    bool getContinuous() const;
    //!
    //! Returns the data pointer initialized by create().
    void* getDataPointer();
    //!
    //! Returns the number of active particles (being drawn).
    GLuint getActiveParticles() const;

 
 private:
    std::vector<gu::Particle> particles_;
    std::vector<GLfloat> particle_data_;
    gu::Shader *shader_;
    bool initialized_;
    bool loaded_vao_;
    GLuint vao_;
    GLuint vbo_;
    GLuint vPosition_;
    GLuint vColour_;
    GLuint vRotation_;
    float point_size_;
    bool use_texture_;
    GLuint num_particles_;
    GLuint texture_;

    bool active_;
    bool additive_blending_;
    bool continuous_;
    
    glm::vec3 emitter_position_;
    glm::vec3 last_emitter_position_;
    
    int updateParticles(const float dt);
    void (*particleSet_)(gu::Particle *particles, int index, void *data);
    void *data_;

    void deconstruct();
};

// ------------------------
// Examples
// ------------------------
void particleSquare(gu::Particle *particle, int index, void *data);
void particleSphere(gu::Particle *particle, int index, void *data);
// ------------------------
// Examples End
// ------------------------

}
