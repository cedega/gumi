#pragma once
#include "Drawable.h"

namespace gu
{

//! 2D Image class 

//!----------------------
//! Overview
//!----------------------
//!
//! gu::Image is an all-purpose image class capable of rendering flat 2D images (like UI) as well as 2D images in a 3D scene.
//!
//! This functionality can be chosen via the <b>pure_2D</b> arguments on image loading functions.
class Image : public gu::Drawable
{
 public:
    friend class Engine;
    Image();
    //!
    //! Copy constructor.
    //!
    //! If the <b>obj</b> argument has called loadTexture(), then the constructed gu::Image will also
    //! call loadTexture() and increment the count of the texture.
    Image(const Image &obj);
    virtual ~Image();
    //!
    //! Assignment operator.
    //!
    //! If the <b>obj</b> argument has called loadTexture(), then the constructed gu::Image will also
    //! call loadTexture() and increment the count of the texture.
    Image& operator=(const Image &obj);

    //!
    //! Draws the gu::Image. Unless the gu::Image is moving, <b>interpolation</b> can be left as 1.0f.
    virtual void draw(const float interpolation=1.0f);
    //!
    //! Binds a gu::Shader to the gu::Image. The gu::Image will store <b>shader</b> as a reference, and use it every draw() call.
    //!
    //! This is a mandatory call unless a gu::Shader is passed as an argument to one of the load* functions.
    void bindShader(gu::Shader *shader);
    //!
    //! Loads a flat colour with a given width and height.
    //!
    //! If <b>shader</b> isn't NULL, then bindShader() will be automatically called on <b>shader</b>.
    //! If <b>pure_2D</b> is asserted, then the gu::Image will be drawn like a UI element. If it's false, then the gu::Image will be drawn like
    //! geometry in a 3D scene.
    void loadColour(GLfloat w, GLfloat h, const glm::vec4 &colour, gu::Shader *shader=NULL, bool pure_2D=true);
    //!
    //! Loads a texture from the gu::TextureLoader.
    //!
    //! If <b>shader</b> isn't NULL, then bindShader() will be automatically called on <b>shader</b>.
    //! If <b>pure_2D</b> is asserted, then the gu::Image will be drawn like a UI element. If it's false, then the gu::Image will be drawn like
    //! geometry in a 3D scene.
    //!
    //! Note that this function will decrement the reference count of any previously loaded texture, and in some cases destroy the texture if
    //! it's the last valid reference to it.
    void loadTexture(GLuint texture, gu::Shader *shader=NULL, bool pure_2D=true);
    //!
    //! Updates the last internal position. Used in interpolation, and only needs to be called if draw() is taking a non-1.0f interpolation argument.
    //!
    //! Should be called directly before updating the position via setPosition().
    void updateLastPosition();
    //!
    //! Decides whether alpha blending will be used for the image or not. By default, this is true.
    void enableAlphaBlending(bool);

    // Setters
 
    //!
    //! Sets the 2D centre position of the gu::Image. However, if <b>top_left_origin</b> is asserted, then
    //! the position will be set relative to the top-left, as common in 2D graphic APIs.
    //!
    //! By asserting <b>snap</b>, the gu::Image will teleport to the given position and skip interpolation.
    //!
    //! <b>snap</b> should be asserted in situations like spawning a gu::Image or teleporting.
    void setPosition(const glm::vec2 &position, bool top_left_origin, bool snap=false);
    //!
    //! Sets the 3D centre position of the gu::Image. Should be used if the gu::Image was not loaded with <b>pure_2D</b> asserted.
    //!
    //! By asserting <b>snap</b>, the gu::Image will teleport to the given position and skip interpolation.
    //!
    //! <b>snap</b> should be asserted in situations like spawning a gu::Image or teleporting.
    void setPosition(const glm::vec3 &position, bool snap=false);
    //!
    //! Sets the last position used by interpolation. This is effectively updateLastPosition(), but with more control.
    void setLastPosition(const glm::vec3 &last_position);
    //!
    //! Sets the centre of the image to the percent of the screen specified in <b>position_percent</b>.
    //!
    //! This is very useful for creating scalable UI.
    void setPositionPercent(const glm::vec2 &position_percent);
    //!
    //! Scales the image to a percent of screen space. To preserve aspect ratio, <b>axis</b> can be passed to determine which direction to uniformly scale the image.
    //!
    //! For example, if AXIS_X is passed, then the image will scale uniformly to <b>percent</b>% of the screen's width.
    //!
    //! The only valid gu::Axis passes are: gu::AXIS_X, gu::AXIS_Y, and gu::AXIS_XY. Any other value will be ignored.
    void setScalePercent(float percent, gu::Axis axis);
    //!
    //! Sets the scaling of the gu::Image around its centre.
    void setScaling(const glm::vec2 &scaling);
    //!
    //! Sets the rotation of the gu::Image around its centre.
    void setRotation(const glm::vec3 &rotation);
    //!
    //! Sets the rectangle area of the image to draw.
    void setTextureRect(const glm::vec4 &rect);
    //!
    //! Sets the colour modifier for the gu::Image.
    void setColour(const glm::vec4 &colour);
    //!
    //! Sets the texture of the gu::Image.
    //!
    //! Note that use of this function will <b>not</b> decrement the reference count of the texture <b>texture_id</b>,
    //!
    //! Therefore, by using this function, it's the user's responsibility to manage the lifetime of any textures passed in as an argument.
    //!
    //! This function should <b>not</b> be used with gu::TextureLoader functions. Use loadTexture() instead.
    //!
    //! Note that this function will decrement the reference count of any previously loaded texture, and in some cases destroy the texture if
    //! it's the last valid reference to it.
    void setTexture(GLuint texture_id, bool pure_2D=true);
    //!
    //! Same as the other setTexture(), but with customizable width and height arguments.
    void setTexture(GLuint texture_id, int width, int height, bool pure_2D=true);

    // Getters

    //!
    //! Returns the currently bound gu::Shader. Can be NULL if no shader is bound.
    gu::Shader* getShader();
    //!
    //! Returns the 2D centre position of the gu::Image. However, if <b>top_left_origin</b> is asserted, then
    //! the returned position will be relative to the top-left.
    glm::vec2 getPosition(bool top_left_origin) const;
    //!
    //! Returns the 3D position of the gu::Image.
    const glm::vec3& getPosition() const;
    //!
    //! Returns the last position used by interpolation.
    const glm::vec3& getLastPosition() const;
    //!
    //! Returns the percent of the screen where the centre of the image lies.
    //!
    //! Note that this function is <b>not</b> dynamic, and will only
    //! return the last value passed to setPositionPercent().
    const glm::vec2& getPositionPercent() const;
    //!
    //! Returns the percent of the image's occupancy of screen space.
    //!
    //! Note that this function is <b>not</b> dynamic, and will only
    //! return the last value passed to setSizePercent().
    //!
    //! The corresponding axis can be returned via using the first argument.
    float getScalePercent(gu::Axis *axis=NULL) const;
    //!
    //! Returns the scaling of the gu::Image around its centre.
    const glm::vec2& getScaling() const;
    //!
    //! Returns the rotation of the gu::Image around its centre.
    const glm::vec3& getRotation() const;
    //!
    //! Returns the width and height dimensions of the gu::Image.
    //!
    //! Note that this value is not affected by scaling or rotating, as is simply the dimensions of displayed the texture.
    //!
    //! This value is modified by setTextureRect().
    const glm::vec2& getDimensions() const;
    //!
    //! Returns the original width and height dimensions of gu::Image.
    //!
    //! This could be different from getDimensions() if setTextureRect() was called.
    const glm::vec2& getOriginalDimensions() const;
    //!
    //! Returns the colour modifier of the gu::Image.
    const glm::vec4& getColour() const;
    //!
    //! Returns the last-used setTextureRect() value. If setTextureRect() hasn't been called, the dimensions of the texture will be returned.
    glm::vec4 getTextureRect() const;
    //!
    //! Returns the bounding gu::AABB around the image for gu::Camera::visible().
    gu::AABB getImageAABB() const;
    //!
    //! Returns the bounding gu::AABB around the image assuming the image is a billboard image.
    //!
    //! The argument will most likely be gu::Camera::getView().
    gu::AABB getBillboardAABB(const glm::mat4 &view) const;
    //!
    //! Returns the dimensions as a rectangle represented as a glm::vec4.
    glm::vec4 getAsRectangle() const;
    //!
    //! Returns the currently loaded texture.
    //!
    //! Note that this function will <b>not</b> increment the texture's reference count, so be weary about using it as an argument to loadTexture().
    GLuint getTexture() const;

 private:
    static glm::ivec2 render_size_;

    gu::Shader *shader_;

    glm::vec3 position_;
    glm::vec3 last_position_;
    glm::vec2 scaling_;
    glm::vec3 rotation_;
    glm::vec2 original_dimensions_;
    glm::vec2 dimensions_;
    glm::vec4 colour_;
    glm::vec4 texture_rect_;

    glm::vec2 position_percent_;
    float scale_percent_;
    gu::Axis scale_percent_axis_;

    bool pure_2d_;
    bool loaded_vao_;
    bool loaded_vbo_;
    bool enable_alpha_blending_;
    int vbo_change_count_;
    bool owns_texture_;

    GLuint texture_;
    GLuint vao_;
    GLuint vbo_;
    GLuint ebo_;

    GLuint vPosition_;
    GLuint vTexCoord_;
    GLuint textureSample_;

    void initVBO(GLfloat width, GLfloat height, bool pure_2D);
    void applyDynamicScaling();

    static void init(const glm::ivec2 &render_size);
    void deconstruct();
};

}
