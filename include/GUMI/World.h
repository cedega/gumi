#pragma once
#include "Model.h"
#include "Camera.h"

namespace gu
{

class World
{
 public:
    World();
    ~World();

    void draw(const gu::Camera &camera);
    void loadFileWorld(const std::string &file_path, gu::Shader *shader);
    void loadZipWorld(const std::string &file_path, gu::Shader *shader);

    // Getters
    std::vector<gu::Model*>* getModels();

 private:
    DISALLOW_COPY_AND_ASSIGN(World);
    std::vector<gu::Model*> models_;

    void processWorld(const std::string &contents, const std::string &path, gu::Shader *shader, bool from_zip);
};

}
