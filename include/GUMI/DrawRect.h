#pragma once
#include "Shader.h"
#include "Tools.h"

namespace gu
{

class Engine;

//! Singleton debug drawing of gu::AABB and gu::YOBB

//!----------------------
//! Overview
//!----------------------
//!
//! Draws gu::AABB and gu::YOBB data. The draw is not interpolated, so results may look jittery.
//!
//! This class is strictly for debug purposes, and its functionality is disabled for iOS and Android platforms.
//!
//! gu::Camera objects automatically update this object. Therefore, it will use the last perspective / view assigned by a gu::Camera.
class DrawRect
{
 friend class Engine;

 public:
    //!
    //! Draws a gu::AABB object with a certain outline width.
    static void draw(const gu::AABB &aabb, float outline_width=2.0f);
    //!
    //! Draws a gu::YOBB object with a certain outline width.
    static void draw(const gu::YOBB &yobb, float outline_width=2.0f);
    //!
    //! Draws a 2D rectangle with a certain outline width.
    static void draw(const glm::vec4 &rect, float depth=0.0f, float outline_width=2.0f);
    //!
    //! Draws a ray as a 2D rectangle
    //!
    //! Please normalize the ray direction.
    static void draw(const glm::vec3 &ray_position, const glm::vec3 &ray_direction, float distance, const glm::vec4 &colour, float outline_width=2.0f);

    // Setters
    static void setPerspective(const glm::mat4 &perspective);
    static void setView(const glm::mat4 &view);

 private:
    DrawRect() {};
    DrawRect(const DrawRect&);
    void operator=(const DrawRect&);

    static bool loaded_vao_;
    static bool loaded_vbo_;

    static gu::Shader shader_;
    static GLuint vao_;
    static GLuint vbo_;
    static GLuint ebo_;

    static GLuint vPosition_;

    static void init();
    static void destroy();
};

}
