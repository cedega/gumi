#pragma once
#include "DrawRect.h"
#include "FrustrumCull.h"

namespace gu
{

//! Core Camera Functionality

//!----------------------
//! Overview
//!----------------------
//!
//! Handles perpsective, view distance, and any look-at functionality.
//!
//! Also supports FPS-mode viewing, and position following.
class Camera
{
 public:
    Camera(); 

    //!
    //! Creates an FPS-mode view. The first argument can be retrieved from gu::Engine::getWindow(), and the second and third argument
    //! must be the size of the window.
    void initFPSCamera(SDL_Window *window, int screen_x, int screen_y);
    //!
    //! Moves the camera to the target position using interpolation. Only call this once per logic loop to preserve interpolation data.
    //! 
    //! Note that getPositionOffset() will be automatically added to the arugment.
    void moveTo(const glm::vec3 &target);
    //! Instantly moves the camera to the target position. getPositionOffset() will be applied.
    void snapTo(const glm::vec3 &target);

    //!
    //! Set the perspective of a given shader to the gu::Camera's.
    //!
    //! Remeber to set a perspective beforehand!
    void applyPerspective(gu::Shader *shader);
    //!
    //! Sets the view of a given shader to the gu::Camera's.
    void applyView(gu::Shader *shader);
    //!
    //! Updates the view based on the interpolation factor. This should be called every gu::Engine::render() before anything
    //! is drawn.
    //!
    //! This function will update all default shaders in gu::Shader, as well as the view for gu::DrawRect.
    void updateView(const float interpolation);
    //!
    //! Moves the FPS view. If initFPSCamera() is not called, this function does nothing.
    //! <b>speed</b> is the movement speed of the camera, and <b>sensitivity</b> is the mouse sensitivity.
    //! This should be called every gu::Entity::logic().
    void updateFPSView(bool up_key, bool down_key, bool left_key, bool right_key, float speed, float sensitivity, const float dt);

    //!
    //! Returns whether a gu::AABB is visible to the view frustrum. By using this on a bounding box of a gu::Model
    //! or gu::Entity (Cullbox()), one can determine if the object is visible to the camera. If it's not visible,
    //! then it's safe to skip the draw call to save resources.
    bool visible(const gu::AABB &aabb) const;

    // Setters
 
    //!
    //! Sets the projection of the camera to be perspective given the 4 arguments.
    //!
    //! <b>angle</b> is the FOV angle.
    //! <b>ratio</b> is the aspect ratio of the window.
    //! <b>near</b> is the distance from the camera to the near plane.
    //! <b>far</b> is the distance from the camera to the far plane.
    //!
    //! This function will update all default shaders in gu::Shader, as well as the perspective for gu::DrawRect.
    void setPerspective(float angle, float ratio, float near, float far);
    //!
    //! Set the view manually. Most users would probably use updateView() instead.
    void setView(const glm::mat4 &view);
    //!
    //! Sets the position of the camera. If <b>hard_set</b> is asserted, the camera will not interpolate
    //! to the new position (it will snap to it).
    void setPosition(const glm::vec3 &position, bool hard_set=false);
    //!
    //! Sets the point which the camera is facing towards. If <b>hard_set</b> is asserted, the camera will not
    //! interpolate to the new point (it will snap to it).
    void setLookAt(const glm::vec3 &point, bool hard_set=false);
    //!
    //! Sets an offset for the camera. When used with moveTo(), the camera will remain this distance away from the point constantly.
    //! This is useful for following an object, because the offset can be the distance away and the moveTo() can simply be the position of the object itself.
    void setPositionOffset(const glm::vec3 &offset);
    //!
    //! Similar to setPositionOffset(), will be looking at an offset away from the point from moveTo().
    void setLookAtOffset(const glm::vec3 &offset);
    //!
    //! Sets how delayed the camera is at following a point with moveTo(). The lower the value, the quicker the camera.
    //! Don't set a value below 1.0f (1.0f = instant).
    void setLerpSpeed(float);

    // Getters
    const glm::mat4& getPerspective() const;
    const glm::mat4& getView() const;
    const glm::vec3& getPosition() const;
    const glm::vec3& getLookAt() const;
    const glm::vec3& getPositionOffset() const;
    const glm::vec3& getLookAtOffset() const;
    float getLerpSpeed() const;

 private:
    DISALLOW_COPY_AND_ASSIGN(Camera);

    gu::FrustrumCull fcull_;

    bool fps_camera_;
    SDL_Window *window_;
    int screen_x_;
    int screen_y_;
    float horizontal_angle_;
    float vertical_angle_;

    float lerp_speed_;

    glm::vec3 last_position_;
    glm::vec3 position_;
    glm::vec3 position_offset_;

    glm::vec3 last_look_at_;
    glm::vec3 look_at_;
    glm::vec3 look_at_offset_;

    glm::mat4 perspective_;
    glm::mat4 view_;
};

}
