bl_info = {
    "name":         "GUMI World Export",
    "author":       "cedega",
    "blender":      (2,7,3),
    "version":      (0,1,3),
    "location":     "File > Import-Export",
    "description":  "Export GUMI .gworld file",
    "category":     "Import-Export"
}

import bpy
from bpy_extras.io_utils import ExportHelper

class WorldExporter(bpy.types.Operator, ExportHelper):
    bl_idname       = "gumi.gworld";
    bl_label        = "Save";
    bl_options      = {'PRESET'};
    
    filename_ext    = ".gworld";

    def execute(self, context):
        cnts = "";
        num = 0;

        f = open(self.filepath, 'w', encoding='utf-8')
    
        for i in range(len(bpy.data.objects)):
            if bpy.data.objects[i].type == "MESH":
                name = ''.join([i for i in bpy.data.objects[i].name if not i.isdigit()]);
                name = name.replace(".","");
                if name != "col":
                    cnts += bpy.data.objects[i].name + ".obj\n";
                    num += 1;

        f.write(str(num) + "\n");
        f.write(cnts);
        f.write("END");
        f.close();
        return {'FINISHED'};

def menu_func(self, context):
    self.layout.operator(WorldExporter.bl_idname, text="GUMI World Format (.gworld)");

def register():
    bpy.utils.register_module(__name__);
    bpy.types.INFO_MT_file_export.append(menu_func);
    
def unregister():
    bpy.utils.unregister_module(__name__);
    bpy.types.INFO_MT_file_export.remove(menu_func);

if __name__ == "__main__":
    register()
