bl_info = {
    "name":         "GUMI AABB Collision Export",
    "author":       "cedega",
    "blender":      (2,7,3),
    "version":      (0,1,1),
    "location":     "File > Import-Export",
    "description":  "Export GUMI .gcol file",
    "category":     "Import-Export"
}
        
import bpy
from bpy_extras.io_utils import ExportHelper

class WorldExporter(bpy.types.Operator, ExportHelper):
    bl_idname       = "gumi.gcol";
    bl_label        = "Save";
    bl_options      = {'PRESET'};
    
    filename_ext    = ".gcol";
    
    def execute(self, context):
        f = open(self.filepath, 'w', encoding='utf-8')
    
        for i in range(len(bpy.data.objects)):
            if bpy.data.objects[i].type == "MESH":
                name = ''.join([i for i in bpy.data.objects[i].name if not i.isdigit()])
                name = name.replace(".","")
                if name == "col":
                    f.write("@" + bpy.data.objects[i].name + "\n")
                    f.write("{0:.10f}".format(bpy.data.objects[i].location.x) + "\n")
                    f.write("{0:.10f}".format(bpy.data.objects[i].location.z) + "\n")
                    f.write("{0:.10f}".format(-1.0 * bpy.data.objects[i].location.y) + "\n")
                    
                    f.write("{0:.10f}".format(bpy.data.objects[i].dimensions.x / 2.0) + "\n")
                    f.write("{0:.10f}".format(bpy.data.objects[i].dimensions.z / 2.0) + "\n")
                    f.write("{0:.10f}".format(bpy.data.objects[i].dimensions.y / 2.0) + "\n")
        
        f.write("END")
        f.close()
        return {'FINISHED'};

def menu_func(self, context):
    self.layout.operator(WorldExporter.bl_idname, text="GUMI AABB Collision Format (.gcol)");

def register():
    bpy.utils.register_module(__name__);
    bpy.types.INFO_MT_file_export.append(menu_func);
    
def unregister():
    bpy.utils.unregister_module(__name__);
    bpy.types.INFO_MT_file_export.remove(menu_func);

if __name__ == "__main__":
    register()
