bl_info = {
    "name":         "GUMI Tools",
    "author":       "cedega",
    "blender":      (2,7,3),
    "version":      (0,0,1),
    "location":     "",
    "description":  "A set of tools for working in the GUMI game engine.",
    "category":     ""
}

import bpy, bmesh

class GUMIPanel(bpy.types.Panel):
    """GUMI Toolbar"""
    bl_label = "GUMI Toolbar"
    bl_space_type = 'VIEW_3D'
    bl_region_type = 'TOOLS'

    def draw(self, context):
        layout = self.layout

        row = layout.row()
        split = layout.split()
        col = split.column(align=True)

        col.operator("gumi.split4", text="Splitx4", icon='MESH_PLANE')
        col.operator("gumi.split6", text="Splitx6", icon='MESH_PLANE')
        col.operator("gumi.split8", text="Splitx8", icon='MESH_PLANE')
        col.operator("gumi.split10", text="Splitx10", icon='MESH_PLANE')

def split(times, C):
    # Original code by CoDEmanX @ stackexchange.com

    bpy.ops.object.mode_set(mode='EDIT')
    bm = bmesh.from_edit_mesh(C.object.data)

    scl_x = C.object.scale.x;
    scl_y = C.object.scale.y;

    tick_x = C.object.dimensions.x * 1.1;
    tick_y = C.object.dimensions.y * 1.1;

    RNG = int(times/2);

    edges = [];

    for i in range(-RNG, RNG):
        ret = bmesh.ops.bisect_plane(bm, geom=bm.verts[:]+bm.edges[:]+bm.faces[:], plane_co=(((tick_x/times)*i/scl_x),0,0), plane_no=(1,0,0))
        bmesh.ops.split_edges(bm, edges=[e for e in ret['geom_cut'] if isinstance(e, bmesh.types.BMEdge)])

    for i in range(-RNG, RNG):
        ret = bmesh.ops.bisect_plane(bm, geom=bm.verts[:]+bm.edges[:]+bm.faces[:], plane_co=(0,((tick_y/times)*i/scl_y),0), plane_no=(0,1,0))
        bmesh.ops.split_edges(bm, edges=[e for e in ret['geom_cut'] if isinstance(e, bmesh.types.BMEdge)])

    bmesh.update_edit_mesh(C.object.data)

    bpy.ops.mesh.separate(type='LOOSE')
    bpy.ops.object.mode_set(mode='OBJECT')

class SplitOperator4(bpy.types.Operator):
    """Evenly splits objects into ~16 multiple smaller objects"""
    bl_idname = "gumi.split4"
    bl_label = "Splitx4"
    bl_space_type = 'VIEW_3D'
    bl_region_type = 'TOOLS'

    @classmethod
    def poll(cls, context):
        return context.active_object is not None

    def execute(self, context):
        split(4, context);
        return {'FINISHED'}

class SplitOperator6(bpy.types.Operator):
    """Evenly splits objects into ~36 multiple smaller objects"""
    bl_idname = "gumi.split6"
    bl_label = "Splitx6"
    bl_space_type = 'VIEW_3D'
    bl_region_type = 'TOOLS'

    @classmethod
    def poll(cls, context):
        return context.active_object is not None

    def execute(self, context):
        split(6, context);
        return {'FINISHED'}

class SplitOperator8(bpy.types.Operator):
    """Evenly splits objects into ~64 multiple smaller objects"""
    bl_idname = "gumi.split8"
    bl_label = "Splitx8"
    bl_space_type = 'VIEW_3D'
    bl_region_type = 'TOOLS'

    @classmethod
    def poll(cls, context):
        return context.active_object is not None

    def execute(self, context):
        split(8, context);
        return {'FINISHED'}

class SplitOperator10(bpy.types.Operator):
    """Evenly splits objects into ~100 multiple smaller objects"""
    bl_idname = "gumi.split10"
    bl_label = "Splitx10"
    bl_space_type = 'VIEW_3D'
    bl_region_type = 'TOOLS'

    @classmethod
    def poll(cls, context):
        return context.active_object is not None

    def execute(self, context):
        split(10, context);
        return {'FINISHED'}

def register():
    bpy.utils.register_class(SplitOperator4)
    bpy.utils.register_class(SplitOperator6)
    bpy.utils.register_class(SplitOperator8)
    bpy.utils.register_class(SplitOperator10)
    bpy.utils.register_class(GUMIPanel)

def unregister():
    bpy.utils.unregister_class(SplitOperator4)
    bpy.utils.unregister_class(SplitOperator6)
    bpy.utils.unregister_class(SplitOperator8)
    bpy.utils.unregister_class(SplitOperator10)
    bpy.utils.unregister_class(GUMIPanel)

if __name__ == "__main__":
    register()
