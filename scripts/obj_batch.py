bl_info = {
    "name":         "Batch OBJ Export",
    "author":       "cedega",
    "blender":      (2,7,3),
    "version":      (0,0,1),
    "location":     "File > Import-Export",
    "description":  "Export GUMI all objects as OBJ files.",
    "category":     "Import-Export"
}
        
import bpy, ntpath
from bpy_extras.io_utils import ExportHelper

class BatchOBJExporter(bpy.types.Operator, ExportHelper):
    bl_idname       = "gumi.batchobj";
    bl_label        = "Save Batch";
    bl_options      = {'PRESET'};

    filename_ext    = "";
    
    def execute(self, context):
        head, tail = ntpath.split(self.filepath);

        slash = "\\";
        if head.rfind("\\") == -1:
            slash = "/";

        bpy.ops.object.select_all(action='DESELECT');

        for i in range(len(bpy.data.objects)):
            if bpy.data.objects[i].type == "MESH":
                name = ''.join([i for i in bpy.data.objects[i].name if not i.isdigit()]);
                name = name.replace(".","");
                if name != "col":
                    bpy.data.objects[i].select = True; 
                    bpy.ops.export_scene.obj(filepath=str(head + slash + bpy.data.objects[i].name + '.obj'), use_selection=True, use_uvs=True, use_materials=True, use_triangles=True)
                    bpy.data.objects[i].select = False;

        return {'FINISHED'};

def menu_func(self, context):
    self.layout.operator(BatchOBJExporter.bl_idname, text="Batch OBJ Export (*.obj)");

def register():
    bpy.utils.register_module(__name__);
    bpy.types.INFO_MT_file_export.append(menu_func);
    
def unregister():
    bpy.utils.unregister_module(__name__);
    bpy.types.INFO_MT_file_export.remove(menu_func);

if __name__ == "__main__":
    register()
